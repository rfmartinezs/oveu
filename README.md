# Oveu 

Plataforma para la administracion de egresados universitarios, desarrollada con python/django.

Caracteristicas:

  - Administracion de informacion de egresados
  - Creacion y gestion de encuestas.
  - Administracion de eventos
  - Correos masivos apartir de eventos generados por los diferentes procesos
  - publicacion de noticias
  - funcionalidad de oferta laboral
  - generacion de informes predefinidos


### Version
1.0.0

### Herramientas

se utilizaron herramientas opensource:

* [Django] - Framework web en python!
* [SublimeText] - text editor
* [MySql] - Base de datos relacional
* [Twitter Bootstrap] - Framework Frontend


### Instalacion

Instalar rabbitmq:


```sh
$ sudo apt-get install rabbitmq-server
```

```sh
$ git clone 

```
Inicializar el worker de celery

```sh
$ celery -A oveu worker -l info - Corre solo el broker
$ celery -A oveu worker -l info -B - Corre el broker y el beat, para las tareas periodicas

```




License
----

GPL




[Django]:http://djangoproject.com/
[SublimeText]:http://www.sublimetext.com
[MySql]:http://www.mysql.com/
[Twitter Bootstrap]:http://getbootstrap.com