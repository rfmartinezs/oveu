
"""
Django settings for oveu project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/

"""
from __future__ import absolute_import

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

RUTA = os.path.dirname(os.path.realpath(__file__))

BROKER_URL = 'amqp://guest:guest@localhost:5672//'

#: Only add pickle to this list if your broker is secured
#: from unwanted access (see userguide/security.html)
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

# otra forma de ejecutar tareas periodicas con celery


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '9*i69t52(y!b6tl3lvnu*q#denv(lnt_o@27zm@cwpyz=eo92#'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


#nombre de dominio actual
DOM_NAME = 'localhost:8000'

TEMPLATE_DIRS = [os.path.join(RUTA,'templates')]

MEDIA_ROOT = os.path.normpath(os.path.join(os.path.dirname(__file__), 'media/'))
    
MEDIA_URL = '/media/'

TINYMCE_JS_URL = os.path.join(RUTA, 'static/js/tiny_mce.js')

#FileField : PDF, DOCX , DOC , ODF
CONTENT_TYPES = ['pdf','msword','vnd.openxmlformats-officedocument.wordprocessingml.document','vnd.oasis.opendocument.text']
# 2.5MB - 2621440
# 5MB - 5242880
# 10MB - 10485760
# 20MB - 20971520
# 50MB - 5242880
# 100MB 104857600
# 250MB - 214958080
# 500MB - 429916160
MAX_UPLOAD_SIZE = "5242880"

TINYMCE_SPELLCHECKER = True
TINYMCE_COMPRESSOR = True

TINYMCE_DEFAULT_CONFIG = {
    'plugins': "table,spellchecker,paste,searchreplace",
    'theme': "advanced",
    'cleanup_on_startup': True,
    'custom_undo_redo_levels': 10,
}
# Application definition

EMAIL_BACKEND = 'django_mailgun.MailgunBackend'
MAILGUN_ACCESS_KEY = 'key-9mfpl01wmc4d-18f8bw992ph-j3twli1'
MAILGUN_SERVER_NAME = 'sandbox9653.mailgun.org'
CORREO_COORDINADOR = 'extensionyegresados@unisinucartagena.edu.co'
#LISTAS DE EMAIL
MAILGUN_LIST_EMPRESA = "empresas@sandbox9653.mailgun.org"
MAILGUN_LIST_EGRESADO = "egresados@sandbox9653.mailgun.org"
MAILGUN_API_KEY = 'pubkey-9s1ju5068f-8c6-cbnsbcaetkyfj05y5'
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'bootstrapform',
    #'dajaxice',
    #'dajax',
    'apps.usuarios',
    'apps.eventos',
    'apps.informes',
    'apps.blog',
    'apps.encuesta',
    'apps.oferta',
    #'apps.correo',
    'south',
    'bootstrap3',
    'cities_light',
    'bootstrap_toolkit',
    #'crispy_forms',
    #'fancy_formsets',
    'autocomplete_light',
    'djangoformsetjs',
    'widget_tweaks',
    'wkhtmltopdf',
    'tinymce',
    'django_mailgun',
    'tekextensions',
    #'mailgun_validation',
    'djcelery',
    'kombu.transport.django',
    'django_filters',

)




BOOTSTRAP3 = {
    'jquery_url': '//code.jquery.com/jquery.min.js',
    'base_url': '//netdna.bootstrapcdn.com/bootstrap/3.0.3/',
    'css_url': None,
    'theme_url': None,
    'javascript_url': None,
    'horizontal_label_class': 'col-md-4',
    'horizontal_field_class': 'col-md-6',
}

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'oveu.urls'

WSGI_APPLICATION = 'oveu.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases




DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', 
        'NAME': 'oveu',
        'USER': 'root',
        'PASSWORD': '9002052541',
        'HOST': '',   # Or an IP Address that your DB is hosted on
        'PORT': '',
        #'OPTIONS': {
         #"init_command": "SET foreign_key_checks = 0;",
        #},
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'es-CO'

TIME_ZONE = 'America/Bogota'

CELERY_TIMEZONE = 'America/Bogota'
CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend'
CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = ''

STATICFILES_DIRS = (
    os.path.join(RUTA, 'static'),
    )

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request',
    'apps.usuarios.context_processors.ejemplo',
    'apps.usuarios.context_processors.menu',
    #'tekextensions.context_processors.admin_media_prefix',

    )
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    
)

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
        },
    },
    'loggers': {
        'django.request': {
            'handlers':['console'],
            'propagate': True,
            'level':'DEBUG',
        },
        'cities_light': {
            'handlers':['console'],
            'propagate': True,
            'level':'DEBUG',
        },
    }
}

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

WKHTMLTOPDF_CMD = '/usr/local/bin/wkhtmltopdf'

WKHTMLTOPDF_CMD_OPTIONS = {
   'quiet':True,
   'encoding': 'utf8',
   }

SESSION_EXPIRE_AT_BROWSER_CLOSE = True