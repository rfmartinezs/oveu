from django.conf.urls import patterns, include, url
from apps.usuarios.views import *
from apps.blog.views import *
from apps.eventos.views import *
from apps.oferta.views import *

import settings
#import autocomplete_light
from django.contrib import admin
autocomplete_light.autodiscover()
admin.autodiscover()


urlpatterns = patterns('',
    # Examples:
    url(r'^confirm/(?P<key>\w+)/','apps.usuarios.views.confimar'),
    url(r'^dashboard/egresado/reenviarcodigo/(?P<id_usuario>\d+)/$', 'apps.usuarios.views.reenviarCodigo', name='reenviarCodigo'),
    url(r'^autocomplete/', include('autocomplete_light.urls')),
    url(r'^add/(?P<model_name>\w+)/?$', 'tekextensions.views.add_new_model'),
    url(r'^configurar$','apps.usuarios.views.instalar'),
    url(r'^configurar/programas/$','apps.usuarios.views.ProgramaConfFormView' , name='agregarProgramas'),
    url(r'^dashboard/programas/eliminar/(?P<programa_id>\d+)/$','apps.usuarios.views.eliminarPrograma' , name='eliminarPrograma'),
    # url(r'^dashboard/', include('apps.usuarios.urls', namespace='dashurl')),
    #url(r'^agregaregresado', 'apps.usuarios.views.addEgresado', name='Egre'),
    url(r'^dashboard/encuesta', include('apps.encuesta.urls', namespace='encuesta')),
    url(r'^dashboard/informes', include('apps.informes.urls', namespace='informes')),
    url(r'^addE', EgresadoCreateView.as_view()),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',{'document_root':settings.MEDIA_ROOT}),
    url(r'^panel$', 'apps.usuarios.views.panel', name='panel'),
    url(r'^login$', 'apps.usuarios.views.iniciar_sesion', name='login'),
    url(r'^logout$', 'apps.usuarios.views.logout', name='logout'),
    url(r'^dashboard/egresados$', 'apps.usuarios.views.listaEgresados', name='listaEgresados'),
    url(r'^dashboard/$', 'apps.usuarios.views.dashboard', name='dashboard'),
    url(r'^dashboard/egresadosDesactualizados$', 'apps.usuarios.views.listaEgresadosDesactualizados', name='listaEgresadosDesactualizados'),
    url(r'^dashboard/usuarios/$', 'apps.usuarios.views.usuarios', name='usuarios'),
    #urls para empresas
    url(r'^dashboard/empresas/$', 'apps.usuarios.views.listaEmpresas', name='listaEmpresas'),
    url(r'^dashboard/empresa/(?P<empresa_id>\d+)/$', 'apps.usuarios.views.toggleEmpresa', name='toggleEmpresa'),
    url(r'^dashboard/addEmpresa/$', 'apps.usuarios.views.addEmpresa', name='addEmpresa'),
    #fin urls empresas
    #urls para eventos
    url(r'^dashboard/eventos', include('apps.eventos.urls',namespace='eventos')), 
    #url(r'^dashboard/eventos/$', 'apps.eventos.views.listaEventos', name='listaEventos'),
    #url(r'^dashboard/evento/add$', 'apps.eventos.views.addEvento', name='addEvento'),
    #url(r'^dashboard/evento/edit/(?P<evento_id>\d+)/$', 'apps.eventos.views.editEvento', name='editEvento'),
    #url(r'^dashboard/evento/detail/(?P<evento_id>\d+)/$', 'apps.eventos.views.detalleEvento', name='detailEvento'),
    #fin urls eventos
    url(r'^dashboard/addCoordinador$', 'apps.usuarios.views.addCoordinador', name='addCoordinador'),
    url(r'^dashboard/actualizarCoordinador$', CoordinadorUpdateView.as_view(), name='actualizarCoordinador'),
    
    url(r'^dashboard/egresado/addEgresado/$', EgresadoCreateView.as_view(), name='addEgresado'),
    url(r'^dashboard/egresado/edit/(?P<pk>\d+)/$', EgresadoUpdateView.as_view(), name='editEgresado'),
    url(r'^dashboard/egresado/eliminar/(?P<egresado_id>\d+)/$', 'apps.usuarios.views.eliminarEgresado', name='eliminarEgresado'),
    url(r'^admin/', include(admin.site.urls)),
    #urls de blogs
    url(r'^', include('apps.blog.urls')),
    #dashboard portal web
    url(r'^dashboard/portal/addEntrada/$', 'apps.blog.views.addEntrada', name='addEntrada'),
    url(r'^dashboard/portal/editEntrada/(?P<entradas_id>\d+)/$', 'apps.blog.views.editEntrada', name='editEntrada'),
    url(r'^dashboard/portal/delEntrada/(?P<entradas_id>\d+)/$', 'apps.blog.views.delEntrada', name='delEntrada'),
    url(r'^dashboard/portal/addCategoria/$', 'apps.blog.views.addCategoria', name='addCategoria'),
    url(r'^dashboard/portal/listaCategoria/$', 'apps.blog.views.listaCategoria', name='listaCategoria'),
    url(r'^dashboard/portal/listaCategoria/edit/(?P<categorias_id>\d+)/$', 'apps.blog.views.editCategoria', name='editCategoria'),
    url(r'^dashboard/portal/listaCategoria/del/(?P<categorias_id>\d+)/$', 'apps.blog.views.delCategoria', name='delCategoria'),
    url(r'^dashboard/portal/listaEntrada/$', 'apps.blog.views.listaEntrada', name='listaEntrada'),

    #urls de oferta laboral
    url(r'^dashboard/oferta', include('apps.oferta.urls')),
    #urls de correo
    url(r'^dashboard/correo', include('apps.correo.urls', namespace='correo')),  
    #urls de tinycme 
    url(r'^tinymce/', include('tinymce.urls')),
    #url programas
    url(r'^dashboard/programas/addPrograma$', ProgramaFormView.as_view(), name='addPrograma'),
    url(r'^dashboard/programas/list/$', ProgramaListView.as_view(), name='listaProgramas'),
    url(r'^dashboard/programas/edit/(?P<pk>\d+)/$', 'apps.usuarios.views.ProgramaUpdate', name='updatePrograma'),
    #fin url
    url(r'^dashboard/helper/busquedaAjax/', 'apps.usuarios.views.busquedaAjax', name='busquedaAjax'),
    url(r'^dashboard/egresado/detail/(?P<egresado_id>\d+)/$', 'apps.usuarios.views.detailEgresado', name='detailEgresado'),
)
