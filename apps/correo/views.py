from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from apps.usuarios.decorators import loginRequired
from django.core.mail import send_mail, BadHeaderError, EmailMessage
from django.http import HttpResponseRedirect, HttpResponse


class CorreoFormView(View):
	#form_class = ProgramaForm
	template_name = 'correo/correo.html'
	#visible_fields = ['nombre']

	@method_decorator(loginRequired)
	def dispatch(self, request, *args, **kwargs):
	    return super(CorreoFormView, self).dispatch(request, *args, **kwargs)
	
	def get(self, request, *args, **kwargs):
		return render(request, self.template_name)

	def post(self, request, *args, **kwargs):
		asunto = request.POST['asunto']
		mensaje = request.POST['mensaje']
		correo = request.POST['correo']

		if asunto and mensaje:
			try:
				msg = EmailMessage(asunto,mensaje,'rafaelmartinezs@live.com', [correo])
				msg.content_subtype = "html"  # Main content is now text/html
				msg.send()
				#send_mail(asunto, mensaje, 'rafaelmartinezs@live.com', [correo])
			except BadHeaderError:
				return HttpResponse('Invalid header found.')
			return render(request, self.template_name, {'info':'Se envio correctamente!'})
		else:
			# In reality we'd use a form class
			# to get proper validation errors.
			return render(request, self.template_name, {'info':'todos los campos son obligatorios!'})
