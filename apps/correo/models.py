from django.db import models

class Correo(models.Model):


	asunto = models.CharField(max_length=120)
	mensaje = models.TextField()
	fecha_creacion = models.DateTimeField(auto_now=True,auto_now_add=True)
	de = models.EmailField()
	
	

	def __unicode__(self):
		return self.asunto


class Destinatario(models.Model):


	to = models.EmailField()
	correo = models.ForeignKey(Correo)

	def __unicode__(self):
		return self.to