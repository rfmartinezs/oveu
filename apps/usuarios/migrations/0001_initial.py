# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Usuario'
        db.create_table(u'usuarios_usuario', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('codigo', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('password_2', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('fecha_creacion', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, auto_now_add=True, blank=True)),
            ('ultimo_ingreso', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2015, 2, 7, 0, 0))),
            ('estado', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('codigo_confirmacion', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'usuarios', ['Usuario'])

        # Adding model 'Empresa'
        db.create_table(u'usuarios_empresa', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('estado', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('usuario', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['usuarios.Usuario'], unique=True)),
        ))
        db.send_create_signal(u'usuarios', ['Empresa'])

        # Adding model 'Administrador'
        db.create_table(u'usuarios_administrador', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('apellidos', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('cargo', self.gf('django.db.models.fields.CharField')(max_length=70)),
            ('fecha_creacion', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, auto_now_add=True, blank=True)),
            ('foto', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('usuario', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['usuarios.Usuario'], unique=True)),
            ('direccion', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('telefono', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
        ))
        db.send_create_signal(u'usuarios', ['Administrador'])

        # Adding model 'Programa'
        db.create_table(u'usuarios_programa', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'usuarios', ['Programa'])

        # Adding model 'Egresado'
        db.create_table(u'usuarios_egresado', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('p_nombre', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('s_nombre', self.gf('django.db.models.fields.CharField')(max_length=45, null=True, blank=True)),
            ('p_apellido', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('s_apellido', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('programa', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Programa'])),
            ('fecha_grado', self.gf('django.db.models.fields.DateField')()),
            ('estado_laboral', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('ultima_modificacion', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2015, 2, 7, 0, 0))),
            ('imagen', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('usuario', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['usuarios.Usuario'], unique=True)),
            ('fecha_cumple', self.gf('django.db.models.fields.DateField')()),
            ('cedula', self.gf('django.db.models.fields.BigIntegerField')()),
            ('estado', self.gf('django.db.models.fields.CharField')(default='vivo', max_length=10)),
            ('carnet', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'usuarios', ['Egresado'])

        # Adding model 'Direccion'
        db.create_table(u'usuarios_direccion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('direccion', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('codigo_postal', self.gf('django.db.models.fields.IntegerField')()),
            ('ciudad', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cities_light.City'])),
            ('egresado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Egresado'])),
        ))
        db.send_create_signal(u'usuarios', ['Direccion'])

        # Adding model 'DireccionActual'
        db.create_table(u'usuarios_direccionactual', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('direccion', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('codigo_postal', self.gf('django.db.models.fields.IntegerField')()),
            ('ciudad', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cities_light.City'])),
            ('egresado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Egresado'])),
        ))
        db.send_create_signal(u'usuarios', ['DireccionActual'])

        # Adding model 'Telefono'
        db.create_table(u'usuarios_telefono', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('numero', self.gf('django.db.models.fields.BigIntegerField')()),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('usuario', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Egresado'])),
        ))
        db.send_create_signal(u'usuarios', ['Telefono'])

        # Adding model 'EstudiosRealizados'
        db.create_table(u'usuarios_estudiosrealizados', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('ano_inicio', self.gf('django.db.models.fields.CharField')(max_length=6)),
            ('ano_fin', self.gf('django.db.models.fields.CharField')(max_length=6)),
            ('egresado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Egresado'])),
            ('centro_estudio', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('ubicacion', self.gf('django.db.models.fields.CharField')(max_length=70)),
        ))
        db.send_create_signal(u'usuarios', ['EstudiosRealizados'])

        # Adding model 'ExperienciaLaboral'
        db.create_table(u'usuarios_experiencialaboral', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha_ingreso', self.gf('django.db.models.fields.DateField')()),
            ('fecha_salida', self.gf('django.db.models.fields.DateField')()),
            ('tipo_contrato', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('empresa', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('cargo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('descripcion', self.gf('django.db.models.fields.TextField')()),
            ('egresado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Egresado'])),
        ))
        db.send_create_signal(u'usuarios', ['ExperienciaLaboral'])

        # Adding model 'Premio'
        db.create_table(u'usuarios_premio', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('premio', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('descripcion', self.gf('django.db.models.fields.TextField')()),
            ('egresado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Egresado'])),
        ))
        db.send_create_signal(u'usuarios', ['Premio'])

        # Adding model 'Idioma'
        db.create_table(u'usuarios_idioma', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('idioma', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('nivel', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('egresado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Egresado'])),
        ))
        db.send_create_signal(u'usuarios', ['Idioma'])

        # Adding model 'Asociacion'
        db.create_table(u'usuarios_asociacion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('fecha_ingreso', self.gf('django.db.models.fields.DateField')()),
            ('egresado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Egresado'])),
        ))
        db.send_create_signal(u'usuarios', ['Asociacion'])


    def backwards(self, orm):
        # Deleting model 'Usuario'
        db.delete_table(u'usuarios_usuario')

        # Deleting model 'Empresa'
        db.delete_table(u'usuarios_empresa')

        # Deleting model 'Administrador'
        db.delete_table(u'usuarios_administrador')

        # Deleting model 'Programa'
        db.delete_table(u'usuarios_programa')

        # Deleting model 'Egresado'
        db.delete_table(u'usuarios_egresado')

        # Deleting model 'Direccion'
        db.delete_table(u'usuarios_direccion')

        # Deleting model 'DireccionActual'
        db.delete_table(u'usuarios_direccionactual')

        # Deleting model 'Telefono'
        db.delete_table(u'usuarios_telefono')

        # Deleting model 'EstudiosRealizados'
        db.delete_table(u'usuarios_estudiosrealizados')

        # Deleting model 'ExperienciaLaboral'
        db.delete_table(u'usuarios_experiencialaboral')

        # Deleting model 'Premio'
        db.delete_table(u'usuarios_premio')

        # Deleting model 'Idioma'
        db.delete_table(u'usuarios_idioma')

        # Deleting model 'Asociacion'
        db.delete_table(u'usuarios_asociacion')


    models = {
        u'cities_light.city': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('region', 'name'),)", 'object_name': 'City'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'feature_code': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '5', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '5', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'db_index': 'True'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'population': ('django.db.models.fields.BigIntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Region']", 'null': 'True', 'blank': 'True'}),
            'search_names': ('cities_light.models.ToSearchTextField', [], {'default': "''", 'max_length': '4000', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"})
        },
        u'cities_light.country': {
            'Meta': {'ordering': "['name']", 'object_name': 'Country'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'code2': ('django.db.models.fields.CharField', [], {'max_length': '2', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'code3': ('django.db.models.fields.CharField', [], {'max_length': '3', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'continent': ('django.db.models.fields.CharField', [], {'max_length': '2', 'db_index': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"}),
            'tld': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '5', 'blank': 'True'})
        },
        u'cities_light.region': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('country', 'name'),)", 'object_name': 'Region'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'geoname_code': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'db_index': 'True'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"})
        },
        u'usuarios.administrador': {
            'Meta': {'object_name': 'Administrador'},
            'apellidos': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'telefono': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['usuarios.Usuario']", 'unique': 'True'})
        },
        u'usuarios.asociacion': {
            'Meta': {'object_name': 'Asociacion'},
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'usuarios.direccion': {
            'Meta': {'object_name': 'Direccion'},
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.City']"}),
            'codigo_postal': ('django.db.models.fields.IntegerField', [], {}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'usuarios.direccionactual': {
            'Meta': {'object_name': 'DireccionActual'},
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.City']"}),
            'codigo_postal': ('django.db.models.fields.IntegerField', [], {}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'usuarios.egresado': {
            'Meta': {'object_name': 'Egresado'},
            'carnet': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cedula': ('django.db.models.fields.BigIntegerField', [], {}),
            'estado': ('django.db.models.fields.CharField', [], {'default': "'vivo'", 'max_length': '10'}),
            'estado_laboral': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'fecha_cumple': ('django.db.models.fields.DateField', [], {}),
            'fecha_grado': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'p_apellido': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'p_nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Programa']"}),
            's_apellido': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            's_nombre': ('django.db.models.fields.CharField', [], {'max_length': '45', 'null': 'True', 'blank': 'True'}),
            'ultima_modificacion': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 2, 7, 0, 0)'}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['usuarios.Usuario']", 'unique': 'True'})
        },
        u'usuarios.empresa': {
            'Meta': {'object_name': 'Empresa'},
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['usuarios.Usuario']", 'unique': 'True'})
        },
        u'usuarios.estudiosrealizados': {
            'Meta': {'object_name': 'EstudiosRealizados'},
            'ano_fin': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'ano_inicio': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'centro_estudio': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ubicacion': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        },
        u'usuarios.experiencialaboral': {
            'Meta': {'object_name': 'ExperienciaLaboral'},
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'descripcion': ('django.db.models.fields.TextField', [], {}),
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            'empresa': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            'fecha_salida': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo_contrato': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'usuarios.idioma': {
            'Meta': {'object_name': 'Idioma'},
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idioma': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'nivel': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'usuarios.premio': {
            'Meta': {'object_name': 'Premio'},
            'descripcion': ('django.db.models.fields.TextField', [], {}),
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'premio': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'usuarios.programa': {
            'Meta': {'object_name': 'Programa'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'usuarios.telefono': {
            'Meta': {'object_name': 'Telefono'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.BigIntegerField', [], {}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'usuario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"})
        },
        u'usuarios.usuario': {
            'Meta': {'object_name': 'Usuario'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'codigo_confirmacion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password_2': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ultimo_ingreso': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 2, 7, 0, 0)'})
        }
    }

    complete_apps = ['usuarios']