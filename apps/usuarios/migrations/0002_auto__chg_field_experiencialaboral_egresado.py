# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'ExperienciaLaboral.egresado'
        db.alter_column(u'usuarios_experiencialaboral', 'egresado_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Egresado'], null=True))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'ExperienciaLaboral.egresado'
        raise RuntimeError("Cannot reverse this migration. 'ExperienciaLaboral.egresado' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'ExperienciaLaboral.egresado'
        db.alter_column(u'usuarios_experiencialaboral', 'egresado_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Egresado']))

    models = {
        u'cities_light.city': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('region', 'name'),)", 'object_name': 'City'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'feature_code': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '5', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '5', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'db_index': 'True'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'population': ('django.db.models.fields.BigIntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Region']", 'null': 'True', 'blank': 'True'}),
            'search_names': ('cities_light.models.ToSearchTextField', [], {'default': "''", 'max_length': '4000', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"})
        },
        u'cities_light.country': {
            'Meta': {'ordering': "['name']", 'object_name': 'Country'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'code2': ('django.db.models.fields.CharField', [], {'max_length': '2', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'code3': ('django.db.models.fields.CharField', [], {'max_length': '3', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'continent': ('django.db.models.fields.CharField', [], {'max_length': '2', 'db_index': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"}),
            'tld': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '5', 'blank': 'True'})
        },
        u'cities_light.region': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('country', 'name'),)", 'object_name': 'Region'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'geoname_code': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'db_index': 'True'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"})
        },
        u'usuarios.administrador': {
            'Meta': {'object_name': 'Administrador'},
            'apellidos': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'telefono': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['usuarios.Usuario']", 'unique': 'True'})
        },
        u'usuarios.asociacion': {
            'Meta': {'object_name': 'Asociacion'},
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'usuarios.direccion': {
            'Meta': {'object_name': 'Direccion'},
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.City']"}),
            'codigo_postal': ('django.db.models.fields.IntegerField', [], {}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'usuarios.direccionactual': {
            'Meta': {'object_name': 'DireccionActual'},
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.City']"}),
            'codigo_postal': ('django.db.models.fields.IntegerField', [], {}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'usuarios.egresado': {
            'Meta': {'object_name': 'Egresado'},
            'carnet': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cedula': ('django.db.models.fields.BigIntegerField', [], {}),
            'estado': ('django.db.models.fields.CharField', [], {'default': "'vivo'", 'max_length': '10'}),
            'estado_laboral': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'fecha_cumple': ('django.db.models.fields.DateField', [], {}),
            'fecha_grado': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'p_apellido': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'p_nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Programa']"}),
            's_apellido': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            's_nombre': ('django.db.models.fields.CharField', [], {'max_length': '45', 'null': 'True', 'blank': 'True'}),
            'ultima_modificacion': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 2, 7, 0, 0)'}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['usuarios.Usuario']", 'unique': 'True'})
        },
        u'usuarios.empresa': {
            'Meta': {'object_name': 'Empresa'},
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['usuarios.Usuario']", 'unique': 'True'})
        },
        u'usuarios.estudiosrealizados': {
            'Meta': {'object_name': 'EstudiosRealizados'},
            'ano_fin': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'ano_inicio': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'centro_estudio': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ubicacion': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        },
        u'usuarios.experiencialaboral': {
            'Meta': {'object_name': 'ExperienciaLaboral'},
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'descripcion': ('django.db.models.fields.TextField', [], {}),
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']", 'null': 'True'}),
            'empresa': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            'fecha_salida': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo_contrato': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'usuarios.idioma': {
            'Meta': {'object_name': 'Idioma'},
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idioma': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'nivel': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'usuarios.premio': {
            'Meta': {'object_name': 'Premio'},
            'descripcion': ('django.db.models.fields.TextField', [], {}),
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'premio': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'usuarios.programa': {
            'Meta': {'object_name': 'Programa'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'usuarios.telefono': {
            'Meta': {'object_name': 'Telefono'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.BigIntegerField', [], {}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'usuario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"})
        },
        u'usuarios.usuario': {
            'Meta': {'object_name': 'Usuario'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'codigo_confirmacion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password_2': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ultimo_ingreso': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 2, 7, 0, 0)'})
        }
    }

    complete_apps = ['usuarios']