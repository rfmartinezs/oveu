# -*- coding: utf-8 -*-
from django.forms import ModelForm
from django.contrib.admin.widgets import  AdminDateWidget
from django.core.exceptions import ObjectDoesNotExist
from django.forms.models import inlineformset_factory
from django.contrib.auth.hashers import *
from .models import *
import autocomplete_light
from django import forms
from cities_light.models import City
from tekextensions.widgets import SelectWithPopUp
from django.forms.models import BaseInlineFormSet
#from __future__ import unicode_literals

#validacion para que el primero de los inline formset sea requerido
class CustomInlineFormSet(BaseInlineFormSet):
	def clean(self):
        # get forms that actually have valid data
		count = 0
		for form in self.forms:
			try:
				if form.cleaned_data:
					count += 1
			except AttributeError:
				# annoyingly, if a subform is invalid Django explicity raises
				# an AttributeError for cleaned_data
				pass
		if count < 1:
			raise forms.ValidationError('You must have at least one order')
            

def get_validate(email):
    return requests.get(
        "https://api.mailgun.net/v2/address/validate",
        auth=("api", "pubkey-9s1ju5068f-8c6-cbnsbcaetkyfj05y5"),
        params={"address": email})

class UsuarioForm(ModelForm):
	class Meta:
		model = Usuario
		exclude = ['fecha_creacion','ultimo_ingreso','estado','codigo_confirmacion']
		labels = {
			'password_2': ('comfirmar password'),
		}
		widgets = {
			'password': forms.PasswordInput,
			'password_2': forms.PasswordInput,
			
		}

	def clean_codigo(self):
		codigo = self.cleaned_data['codigo']
		try:
			u = Usuario.objects.get(codigo=codigo)
			raise forms.ValidationError('El codigo esta siendo utilizado por otro usuario')
		except ObjectDoesNotExist, e:
			
			return codigo
			
	def clean_password_2(self):
		# Check that the two password entries match
		password = self.cleaned_data.get("password")
		password_2 = self.cleaned_data.get("password_2")
		if password and password_2 and password != password_2:
			raise forms.ValidationError("Passwords no coinciden")
		else:
			password = make_password(password_2)
			if is_password_usable(password):
				return password
			else:
				raise forms.ValidationError("Porfavor cambiar las password, esta no puede ser usada. (no utilice espacios)")
		

class DireccionActualForm(forms.ModelForm):
	ciudad = forms.ModelChoiceField(City.objects.all(), widget=autocomplete_light.ChoiceWidget('CityAutocomplete'))
	class Meta:
		model = DireccionActual
		exclude = ['egresado',]
		fields = '__all__'
		widgets={'ciudad': forms.TextInput(attrs={'class': 'name'})}
    

class EgresadoForm(ModelForm):
	class Meta:
		model = Egresado
		exclude = ['ultima_modificacion','usuario']
		labels = {
			'p_nombre': ('Primer Nombre'),
			's_nombre': ('Segundo Nombre'),
			'p_apellido': ('Primer Apellido'),
			's_apellido': ('Segundo Apellido'),
			'estado': ('Estado'),
			'fecha_grado': ('Fecha de grado'),
			'fecha_cumple': ('Fecha de nacimiento'),
		}
		widgets={
			"estado":forms.Select(choices=(("vivo","Vivo" ),("fallecido","Fallecido"))),
			'programa': SelectWithPopUp(attrs={'style': 'width:96%;display:inline-block;'}),

		}


class TelefonoForm(forms.ModelForm):

	class Meta:
		model = Telefono
		exclude = ['usuario',]
        


class DireccionForm(forms.ModelForm):
	ciudad = forms.ModelChoiceField(City.objects.all(), widget=autocomplete_light.ChoiceWidget('CityAutocomplete'))
	class Meta:
		model = Direccion
		fields = '__all__'
		widgets={'ciudad': forms.TextInput(attrs={'class': 'name'})}

class EstudiosRealizadosForm(forms.ModelForm):
    class Meta:
        model = EstudiosRealizados
        labels = {
			'ano_inicio': (u"Año de inicio"),
			'ano_fin': (u"Año de finalizacion"),
		
		}
#se coloca formset=CustomInlineFormSet en inlineformset_factory cuando por lo menos 1 form del formset se requerido
#TelefonoFormSet = inlineformset_factory(Egresado, Telefono, can_delete=True, extra=2, formset=CustomInlineFormSet)
TelefonoFormSet = inlineformset_factory(Egresado, Telefono, can_delete=True, extra=2)
ExperienciaLaboralFormSet = inlineformset_factory(Egresado, ExperienciaLaboral, can_delete=True, extra=1)		
DireccionFormSet = inlineformset_factory(Egresado, Direccion,form=DireccionForm, extra=1, can_delete=True)
EstudiosFormSet = inlineformset_factory(Egresado, EstudiosRealizados, form=EstudiosRealizadosForm, extra=1, can_delete=True)
AsociacionFormSet = inlineformset_factory(Egresado, Asociacion, can_delete=True, extra=1)
PremioFormSet = inlineformset_factory(Egresado, Premio, can_delete=True, extra=1)
IdiomaFormSet = inlineformset_factory(Egresado, Idioma, can_delete=True, extra=1)

CONTINENT_CHOICES = (
	('DF', 'Escoger...'),
    ('OC', 'Oceania'),
    ('EU', 'Europe'),
    ('AF', 'Africa'),
    ('NA', 'North America'),
    ('AN', 'Antarctica'),
    ('SA', 'South America'),
    ('AS', 'Asia'),
)


class usuarioFormPassword(forms.Form):
	password_actual = forms.CharField(max_length=60, label=u'Contraseña actual', required=False, widget=forms.PasswordInput)
	password_nueva = forms.CharField(max_length=60, label=u'Nueva Contraseña', required=False, widget=forms.PasswordInput)
	
	def clean(self):
		cleaned_data = super(usuarioFormPassword, self).clean()
		password1 = cleaned_data.get("password_actual")
		password2 = cleaned_data.get("password_nueva")
		if not password1 and password2:
			# Only do something if both fields are valid so far.
			raise forms.ValidationError("Para cambiar la contraseña debe digitar los campos, Contraseña actual y contraseña nueva")
		if not password2 and password1:
			raise forms.ValidationError("Para cambiar la contraseña debe digitar los campos, Contraseña actual y contraseña nueva")			
		# Always return the full collection of cleaned data.
		return cleaned_data



class AdministradorForm(forms.ModelForm):
    class Meta:
        model = Administrador
        exclude = ['fecha_creacion','usuario']
        labels = {
			'nombre': ('Nombres'),
			'apellidos': ('Apellidos'),
			'cargo': ('Cargo'),
			'direccion': ('Direccion'),
			'correo': ('Correo'),
			'telefono': ('Telefono'),
		}

class addAdministradorForm(forms.Form):
	codigo		 = forms.CharField(max_length=60, label='Usuario ')
	password 	 = forms.CharField(max_length=100, widget=forms.PasswordInput, label='Password')
	password2 	 = forms.CharField(max_length=100, widget=forms.PasswordInput, label='Password Confirmacion')
	nombre = forms.CharField(max_length=45, label='Nombre(*)' )
	apellidos = forms.CharField(max_length=45, label='Apellidos(*)' )
	foto = forms.ImageField(required=False)
	direccion = forms.CharField(max_length=120, label='Direccion')
	correo = forms.EmailField()
	telefono = forms.IntegerField()

	def clean_codigo(self):
		codigo = self.cleaned_data['codigo']
		try:
			u = Usuario.objects.get(codigo=codigo)
			raise forms.ValidationError('El codigo esta siendo utilizado por otro usuario')
		except ObjectDoesNotExist, e:
			
			return codigo

class actualizarAdministradorForm(forms.Form):
	codigo    = forms.CharField(max_length=60, label='Usuario ')
	nombre    = forms.CharField(max_length=45, label='Nombre(*)' )
	apellidos = forms.CharField(max_length=45, label='Apellidos(*)' )
	direccion = forms.CharField(max_length=120, label='Direccion')
	cargo = forms.CharField(max_length=100, label='Cargo')
	correo    = forms.EmailField(label='Correo Electronico')
	telefono  = forms.IntegerField(label='Telefono')
	foto      = forms.ImageField(required=False, label='Foto')
    


class InicioSesionForm(forms.Form):
	codigo 	= forms.CharField(max_length=50 , required=True)
	clave 	= forms.CharField(max_length=50, widget=forms.PasswordInput)
	#clave2 	= forms.CharField(max_length=50, widget=forms.PasswordInput)


class CrearEgresadoForm(forms.Form):
	codigo		 = forms.CharField(max_length=60, label='Codigo(*) ')
	password 	 = forms.CharField(max_length=100, widget=forms.PasswordInput, label='Password')
	password2 	 = forms.CharField(max_length=100, widget=forms.PasswordInput, label='Password Confirmacion')
	p_nombre 	 = forms.CharField(max_length=45, label='Primer Nombre (*)' )
	s_nombre 	 = forms.CharField(max_length=45, label='Segundo Nombre', required=False)
	p_apellido 	 = forms.CharField(max_length=45, label='Primer Apellido (*)')
	s_apellido 	 = forms.CharField(max_length=45, label='Segundo Apellido (*)')
	programa 	 = forms.ModelChoiceField(queryset=Programa.objects.all())
	fecha_cumpleano = forms.DateField(label='Fecha de cumpleanos (*)', widget=forms.TextInput(attrs={'placeholder': '1993-12-5'}))
	direccion 	 = forms.CharField(max_length=45, label='Direccion', help_text='Nomeclatura completa', widget=forms.TextInput(attrs={'placeholder': 'Calle 13 #20 con Carrera 13 Barrio X'}), required=True)
	continente = forms.CharField(max_length=2, label='Continente(*)',widget=forms.Select(choices=(CONTINENT_CHOICES)), required=True)
	pais = forms.CharField(max_length=10, label='Pais(*)',widget=forms.Select(), required=True)
	ciudad = forms.CharField(max_length=10, label='Ciudad(*)',widget=forms.Select(), required=True)
	codigo_postal = forms.IntegerField(label='Codigo Postal', help_text='Buscar codigo postal http://www.codigopostal4-72.com.co/codigosPostales/')
	telefono_fijo = forms.IntegerField(label='Telefono Fijo', required=False) 
	celular = forms.IntegerField(label='Telefono Celular')
	telefono_oficina = forms.IntegerField(label='Telefono Oficina', required=False)
	email = forms.EmailField(label='Correo Electronico')
	fecha_grado  = forms.DateField(label='Fecha de Grado (*)', widget=forms.TextInput(attrs={'placeholder': '2013-12-5'}))
	imagen 		 = forms.ImageField(required=False)
	estado 		 = forms.BooleanField(label='Estado', required=False)
	cedula = forms.IntegerField()

	def clean_codigo(self):
		codigo = self.cleaned_data['codigo']
		try:
			u = Usuario.objects.get(codigo=codigo)
			raise forms.ValidationError('El codigo esta siendo utilizado por otro usuario')
		except ObjectDoesNotExist, e:
			
			return codigo

#formulario para editar los egresados
class EditEgresadoAdminForm(forms.Form):
	#codigo		 = forms.CharField(max_length=60, label='Codigo(*) ')
	p_nombre 	 = forms.CharField(max_length=45, label='Primer Nombre (*)' )
	s_nombre 	 = forms.CharField(max_length=45, label='Segundo Nombre', required=False)
	p_apellido 	 = forms.CharField(max_length=45, label='Primer Apellido (*)')
	s_apellido 	 = forms.CharField(max_length=45, label='Segundo Apellido (*)')
	programa 	 = forms.ModelChoiceField(queryset=Programa.objects.all())
	fecha_cumpleano = forms.DateField(label='Fecha de cumpleanos (*)', widget=AdminDateWidget)
	direccion = forms.CharField(max_length=100, label='Direccion Residencia(*)')
	codigo_postal = forms.IntegerField(label='Codigo Postal')
	telefono_fijo = forms.IntegerField(label='Telefono Fijo', required=False) 
	celular = forms.IntegerField(label='Telefono Celular')
	telefono_oficina = forms.IntegerField(label='Telefono Oficina', required=False)
	email = forms.EmailField(label='Correo Electronico')
	fecha_grado  = forms.DateField(label='Fecha de Grado (*)', widget=forms.TextInput(attrs={'placeholder': '2013-12-5'}))
	imagen 		 = forms.ImageField(required=False)
	estado 		 = forms.BooleanField(label='Estado', required=False)
	cedula = forms.IntegerField()

	
class AddEmpresaForm(forms.Form):
	codigo = forms.CharField(max_length=30, label='Usuario')
	password = forms.CharField(max_length=30, widget=forms.PasswordInput, label='clave')
	password2 = forms.CharField(max_length=30, widget=forms.PasswordInput, label='Clave(otra vez)')
	nombre = forms.CharField(max_length=100, label='nombre')
	tipo = forms.CharField(max_length=50, label='Tipo de empresa',widget=forms.Select(choices=(TE_CHOICES)))
	email= forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'example@correo.com'}))

	def clean_codigo(self):
		codigo = self.cleaned_data['codigo']
		try:
			u = Usuario.objects.get(codigo=codigo)
			raise forms.ValidationError('El Usuario esta siendo utilizado por otro usuario, por favor ingrese otro')
		except ObjectDoesNotExist, e:
			
			return codigo

class ProgramaForm(forms.ModelForm):
    class Meta:
        model = Programa
        fields = ['nombre']
    
	def clean_nombre(self):
		nombre = self.cleaned_data['nombre']
		try:
			u = Programa.objects.get(nombre=nombre)
			raise forms.ValidationError('El nombre ya esta siendo utilizado')
		except ObjectDoesNotExist, e:
			return nombre





	
		



    

    
