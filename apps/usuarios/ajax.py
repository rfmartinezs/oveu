from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register
from cities_light.models import Country, City
#from dajaxice.core import dajaxice_functions

@dajaxice_register
def updatecombo(request, option):
	dajax = Dajax()
	countries = Country.objects.filter(continent=option)
	out = []
	for country in countries:
		out.append("<option value='%s'>%s</option>" % (country.id,country.name))

	dajax.assign('#id_pais', 'innerHTML', ''.join(out))
	return dajax.json()

@dajaxice_register
def llenarCiudades(request, option):
	dajax = Dajax()
	cities = City.objects.filter(country=option)
	out = []
	for city in cities:
		out.append("<option value='%s'>%s</option>" % (city.id, city.name))

	dajax.assign('#id_ciudad', 'innerHTML', ''.join(out))
	return dajax.json()
