def ejemplo(request):
	return {'frase': 'Holaa'}

from django.core.urlresolvers import reverse

def menu(request):
	menu = {'menu': [
		{'name': 'Egresados', 'icono': 'fa-male' , 'url': '/dashboard/egresado','submenu':[
				{ 'sname':'Listar Egresados', 'surl':reverse('listaEgresados')},
				{ 'sname':'Listar Egresados Desactualizados', 'surl':reverse('listaEgresadosDesactualizados')},
				{ 'sname':'Registrar Egresado', 'surl':reverse('addEgresado')},

				] 
		},
		{'name': 'Portal Web', 'icono':'fa-keyboard-o', 'url': '/dashboard/portal', 'submenu':[
				{ 'sname':'Listar categoria', 'surl':reverse('listaCategoria')},
				{ 'sname':'Listar noticia', 'surl':reverse('listaEntrada')},
				{ 'sname':'Publicar noticia', 'surl':reverse('addEntrada')},
				]},

		{'name': 'Encuestas', 'icono':'fa-truck', 'url': '/dashboard/encuesta', 'submenu':[
				{ 'sname':'Crear Encuesta', 'surl':reverse('encuesta:crearEncuesta')},
				{ 'sname':'Listar Encuestas', 'surl':reverse('encuesta:listaEncuesta')},

				]},
		{'name': 'Eventos', 'icono':'fa-calendar', 'url': '/dashboard/eventos', 'submenu':[
				{ 'sname':'Crear Evento', 'surl':reverse('eventos:addEvento')},
				{ 'sname':'Listar eventos', 'surl':reverse('eventos:listaEventos')},

				]},
		{'name': 'Empresas', 'icono':'fa-briefcase', 'url': '/dashboard/empresas', 'submenu':[
				{ 'sname':'Listar empresas', 'surl':reverse('listaEmpresas')},

				]},
		{'name': 'Programas', 'icono':'fa-cubes', 'url': '/dashboard/programas', 'submenu':[
				{ 'sname':'Crear programa', 'surl':reverse('addPrograma')},
				{ 'sname':'Listar programas', 'surl':reverse('listaProgramas')},

				]},
		{'name': 'Oferta Laboral', 'icono':'fa-bullhorn', 'url': '/dashboard/oferta', 'submenu':[
				{ 'sname':'Listar Ofertas', 'surl':reverse('listaOfertaLaboral')},

				]},
	]}
	for men in menu['menu']:
		if men['url'] in request.path:
			men['active'] = True
	return menu