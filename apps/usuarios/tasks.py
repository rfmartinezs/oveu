# -*- coding: utf-8 -*-
from oveu.celery import app
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import EmailMultiAlternatives
from celery.task.schedules import crontab
from celery.decorators import periodic_task
from celery.utils.log import get_task_logger
import datetime
from apps.usuarios.models import Egresado
from django.conf import settings
import requests

@app.task(bind=True)
def CreateUserTask(self, email, nombre, link):
	subject = 'Bienvenido egresado, confirma tu cuenta'
	from_email = settings.CORREO_COORDINADOR
	to = email
	html_content = render_to_string('registrar_action.html', {'egresado':nombre, 'link':link})
	text_content = strip_tags(html_content)
	msg = EmailMultiAlternatives(subject,text_content, from_email, [to])
	msg.attach_alternative(html_content, "text/html")
	msg.send()



 
 
logger = get_task_logger(__name__)
 
 
# Tarea periodica que se ejecuta cada dia a las 9:00 AM, y envia un correo de felicitacion #a los egresados que estan de cumpleaños

@periodic_task(run_every=(crontab(hour=9, minute=0, day_of_week="*")))
def HappyBirthdayAllTask():
	logger.info("Start task HappyBirthdayAllTask")
	dia = datetime.datetime.now().day
	mes = datetime.datetime.now().month
	egresados = Egresado.objects.filter(fecha_cumple__month=mes).filter(fecha_cumple__day=dia).exclude(estado__iexact='fallecido')

	if egresados.count():

		for egresado in egresados:
			html_content = render_to_string('usuarios/happybirthday.html', {'egresado':egresado })
			text_content = strip_tags(html_content)
			send = requests.post(
		        "https://api.mailgun.net/v2/%s/messages" % (settings.MAILGUN_SERVER_NAME),
		        auth=("api", settings.MAILGUN_ACCESS_KEY),
		        data={"from": "Coordinador egresados <%s>" % (settings.CORREO_COORDINADOR),
		              "to": egresado.usuario.email,
		              "subject": "%s, Feliz cumpleaños" % (egresado.p_nombre),
		              "text": text_content,
		              "html": html_content })
			logger.info("Response mailgun: %S" % (send))

			