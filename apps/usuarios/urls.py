from django.conf.urls import patterns, url
from apps.usuarios.views import *



urlpatterns = patterns('apps.usuarios.views',
    # Examples:
    url(r'^/$', 'usuarios', name='usuarios'),
    url(r'^/empresas/$', 'listaEmpresas', name='listaEmpresas'),
    url(r'^/egresado/detail/(?P<egresado_id>\d+)/$', 'detailEgresado', name='detailEgresado'),
    url(r'^/addEgresado/$', EgresadoCreateView.as_view(), name='addEgresado'),
    
    
    #url(r'^listaUsuarios/$', UsuariosList.as_view(), name='listaUsuarios'),
    #url(r'^addUsuario$', 'addUsuario', name='addUsuario'),
    #url(r'^listarUsuarios/$', 'listaUsuarios', name='listaUsuarios'),
    
    # url(r'^blog/', include('blog.urls')),


)
