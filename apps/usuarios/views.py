# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, render, get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse, reverse_lazy
from django.template import RequestContext
from django.views.generic import ListView
from django.views.generic.edit import UpdateView
from .decorators import loginRequired
from django.core.mail import send_mail
from django.contrib.auth.hashers import *
import hashlib, datetime, random
from .forms import *
from .models import *
from cities_light.models import *
from django.db.models import Q
from django.forms.models import modelformset_factory
from django.views.generic import CreateView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .tasks import CreateUserTask
from django.core import serializers
from django.http import Http404


def confimar(request, key):
	user = get_object_or_404(Usuario,codigo_confirmacion=key)
	print user.estado
	if user.estado == False:
		user.estado = True
		user.save()
		mensaje = 'Tu cuenta ha sido verificada!'
		h = 1
	else:
		mensaje = 'Tu cuenta ha sido verificada con anterioridad!'
		h = 2
	return render(request, 'confirmar.html', locals())

def reenviarCodigo(request, id_usuario):
	usuario = get_object_or_404(Usuario, pk=id_usuario)
	egresado = Egresado.objects.get(usuario=usuario)
	if request.method == 'GET':
		if egresado.usuario.estado == True:
			mensaje = 'Ya se ha confirmado el usuario!'
		return render(request, 'usuarios/reenviar_codigo.html', locals())

	if request.method == 'POST':
		if request.POST['email']:
			usuario.email = request.POST['email']
			if egresado.usuario.codigo_confirmacion:
				egresado.save()
			else:
				salt = hashlib.sha1(str(random.random())).hexdigest()[:7]
				activation_key = hashlib.sha1(salt).hexdigest()
				egresado.usuario.codigo_confirmacion = activation_key
				egresado.save()
			usuario.save()
			email_body = "Hola %s, te enviamos la url para que confirmes tu cuenta de egresado http://127.0.0.1:8000/confirm/%s" % (egresado.p_nombre, egresado.usuario.codigo_confirmacion)

			send_mail('Codigo de confirmacion, para cuenta egresado', email_body, 'rafaelmartinezs@live.com',[egresado.usuario.email], fail_silently=False)
			mensaje = "se ha enviado el correo!"
			return render(request,'usuarios/reenviar_codigo.html', locals())




def instalar(request):

	administrador = Administrador.objects.all().count()

	if not administrador:
		form = addAdministradorForm()
		if request.method == 'POST':
			form = addAdministradorForm(request.POST, request.FILES)
			if request.POST['password'] == request.POST['password2']:
				if form.is_valid():
				
					codigo = form.cleaned_data['codigo']
					password  = form.cleaned_data['password']
					passencryp = make_password(password)
					nombre = form.cleaned_data['nombre']
					apellidos = form.cleaned_data['apellidos']
					direccion = form.cleaned_data['direccion']
					telefono = form.cleaned_data['telefono']
					correo = form.cleaned_data['correo']
					foto = form.cleaned_data['foto']
					nuevo_usuario = Usuario.objects.create(codigo=codigo,password=passencryp, ultimo_ingreso=datetime.datetime.now(),email=correo)
					nuevo_coordinador = Administrador.objects.create(nombre=nombre,apellidos=apellidos, direccion=direccion,telefono=telefono, usuario=nuevo_usuario)
					if foto:
						nuevo_coordinador.foto = foto
						nuevo_coordinador.save()
					return HttpResponseRedirect(reverse_lazy('agregarProgramas'))

		return render(request, "instalar.html", { 
	    	'form':form,
	    })
	raise Http404("No existe!")




#funcion para agregar programas en la configuracion de la aplicacion
def ProgramaConfFormView(request):
    ProgramaFormset = modelformset_factory(Programa, form=ProgramaForm, extra=2)
    if request.method == 'POST':
        formset = ProgramaFormset(request.POST)
        if formset.is_valid():
            formset.save()
            return HttpResponseRedirect(reverse_lazy('login'))
        else:
        	formset = ProgramaFormset(request.POST)	
        	return render(request, 'addProgramaConf.html', {
		    'form': formset,
		    })
    else:
        formset = ProgramaFormset()

    return render(request, 'addProgramaConf.html', {
    'form': formset,
    })


@loginRequired
def toggleEmpresa(request, empresa_id):
	e = get_object_or_404(Empresa,pk=empresa_id)
	u = Usuario.objects.get(pk=e.usuario.id)
	if request.method == 'GET':
		action = request.GET.get('action');
		if action == 'habilitar':
			if not u.estado:
				u.estado = True
				u.save()
				return HttpResponseRedirect(reverse('listaEmpresas'));
		elif action == 'deshabilitar':
			if u.estado:
				u.estado = False
				u.save()
				return HttpResponseRedirect(reverse('listaEmpresas'));
		else:
			raise Http404("No existe!")



class EgresadoCreateView(CreateView):
	template_name = 'usuarios/crearUsuario.html'
	model = Egresado
	form_class = EgresadoForm
	success_url = reverse_lazy('listaEgresados')

	#solo podra acceder si ha iniciado session
	@method_decorator(loginRequired)
	def dispatch(self, request, *args, **kwargs):
	    return super(EgresadoCreateView, self).dispatch(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		telefono_form = TelefonoFormSet()
		usuario_form = UsuarioForm()
		experiencia_form = ExperienciaLaboralFormSet()
		direccion_form = DireccionFormSet()
		estudios_form = EstudiosFormSet()
		idioma_form = IdiomaFormSet()
		asociacion_form = AsociacionFormSet()
		premio_form = PremioFormSet()
		direccion_actual_form = DireccionActualForm()
		return self.render_to_response(
            self.get_context_data(form=form,
                                  telefono_form=telefono_form,
                                  usuario_form =usuario_form,
                                  direccion_form = direccion_form,
                                  experiencia_form = experiencia_form,
                                  estudios_form = estudios_form,
                                  idioma_form = idioma_form,
                                  asociacion_form = asociacion_form,
                                  premio_form = premio_form,
                                  direccion_actual_form = direccion_actual_form ))

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		#form = EgresadoForm(self.request.POST, self.request.FILES)
		usuario_form = UsuarioForm(self.request.POST)
		experiencia_form = ExperienciaLaboralFormSet(self.request.POST)
		direccion_form = DireccionFormSet(self.request.POST)
		telefono_form = TelefonoFormSet(self.request.POST)
		estudios_form = EstudiosFormSet(self.request.POST)
		idioma_form = IdiomaFormSet(self.request.POST)
		asociacion_form = AsociacionFormSet(self.request.POST)
		premio_form = PremioFormSet(self.request.POST)
		direccion_actual_form = DireccionActualForm(self.request.POST)

		if (form.is_valid() and usuario_form.is_valid() and experiencia_form.is_valid() and telefono_form.is_valid() and direccion_form.is_valid() and estudios_form.is_valid() and idioma_form.is_valid() and asociacion_form.is_valid() and premio_form.is_valid() and direccion_actual_form.is_valid()):
			return self.form_valid(form, usuario_form, experiencia_form, telefono_form, direccion_form, estudios_form, idioma_form, asociacion_form, premio_form, direccion_actual_form)
		else:
			return self.form_invalid(form, usuario_form, experiencia_form, telefono_form, direccion_form, estudios_form, idioma_form, asociacion_form, premio_form, direccion_actual_form)

	def form_valid(self, form, usuario_form, experiencia_form, telefono_form, direccion_form, estudios_form, idioma_form, asociacion_form, premio_form, direccion_actual_form):
		"""
		Called if all forms are valid. Creates a Recipe instance along with
		associated Ingredients and Instructions and then redirects to a
		success page.
		"""
		#instanciamos el modelForm de usuario y lo guardamos pero no le damos commit
		user = usuario_form.save(commit=False)
		user.password = user.password_2

		
		salt = hashlib.sha1(str(random.random())).hexdigest()[:7]
		activation_key = hashlib.sha1(salt).hexdigest()
		user.codigo_confirmacion = activation_key
		user.save()
		#como exclui el campo ultimo ingreso necesito agregarlo antes de guardar
		
		#guardamos porque ya tenemos el usuario con todos sus datos
		
		
		#como un objeto egresado necesita una instancia de usuario entonces le pasamos la instancia que acabamos de crear de usuario
		egresado = form.save(commit=False)
		#le paso la instancia de usuario a egresado para poder guardar correctamente
		egresado.usuario = user
		egresado.save()
		#print egresado
		
		#ahora guardamos el form de egresado pero lo guardamos en el self.object
		self.object = egresado
		print egresado
		daf = direccion_actual_form.save(commit=False)
		daf.egresado = self.object
		daf.save()
		#como la direccion necesita una instancia de usuario se la asignamos al form de direccion
		direccion_form.instance = self.object
		direccion_form.save()
		idioma_form.instance = self.object
		idioma_form.save()
		asociacion_form.instance = self.object
		asociacion_form.save()
		premio_form.instance = self.object
		premio_form.save()

		telefono_form.instance = self.object
		telefono_form.save()
		experiencia_form.instance = self.object
		experiencia_form.save()
		estudios_form.instance = self.object
		estudios_form.save()
		#email_body = "Hola %s, se ha creado la cuenta de egresados te enviamos la url para que la actives http://127.0.0.1:8000/confirm/%s" % (self.object.p_nombre, self.object.usuario.codigo_confirmacion)
		#send_mail('Bienvenido egresado unisinuano, cuenta egresado', email_body,'rafaelmartinezs@live.com',[self.object.email], fail_silently=False)
		link = "http://127.0.0.1:8000/confirm/%s" % (self.object.usuario.codigo_confirmacion)
		print user.email
		CreateUserTask.delay(user.email, self.object.p_nombre, link)
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form, usuario_form, experiencia_form, telefono_form, direccion_form, estudios_form, idioma_form, asociacion_form, premio_form, direccion_actual_form):
		"""
		Called if a form is invalid. Re-renders the context data with the
		data-filled forms and errors.
		"""
		return self.render_to_response(
			self.get_context_data(form = form,
                                  telefono_form = telefono_form,
                                  usuario_form = usuario_form,
                                  experiencia_form = experiencia_form,
                                  direccion_form = direccion_form,
                                  estudios_form= estudios_form,
                                  idioma_form = idioma_form,
                                  premio_form = premio_form,
                                  asociacion_form = asociacion_form,
                                  direccion_actual_form = direccion_actual_form))
	

class EgresadoUpdateView(UpdateView):
	model = Egresado
	form_class = EgresadoForm
	template_name = 'usuarios/editarEgresado.html'
	success_url = reverse_lazy('listaEgresados')


	#solo podra acceder si ha iniciado session
	@method_decorator(loginRequired)
	def dispatch(self, request, *args, **kwargs):
	    return super(EgresadoUpdateView, self).dispatch(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		self.object = get_object_or_404(Egresado, pk=self.kwargs['pk'])
		direccion_actual_object = DireccionActual.objects.filter(egresado=self.object).first()
		#usuario = self.object.usuario
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		print self.object
		
		telefono_form = TelefonoFormSet(instance=self.object)
		#usuario_form = UsuarioForm(instance=usuario)
		experiencia_form = ExperienciaLaboralFormSet(instance=self.object)
		direccion_form = DireccionFormSet(instance=self.object)
		estudios_form = EstudiosFormSet(instance=self.object)
		idioma_form = IdiomaFormSet(instance=self.object)
		asociacion_form = AsociacionFormSet(instance=self.object)
		premio_form = PremioFormSet(instance=self.object)
		direccion_actual_form = DireccionActualForm(instance=direccion_actual_object)
		return self.render_to_response(
            self.get_context_data(form = form,
                                  telefono_form = telefono_form,
                                  experiencia_form = experiencia_form,
                                  direccion_form = direccion_form,
                                  estudios_form= estudios_form,
                                  idioma_form = idioma_form,
                                  premio_form = premio_form,
                                  asociacion_form = asociacion_form,
                                  direccion_actual_form = direccion_actual_form
                                   ))

	

	def post(self, request, *args, **kwargs):
		self.object = Egresado.objects.get(pk=self.kwargs['pk'])
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		direccion_actual_object = DireccionActual.objects.filter(egresado=self.object).first()
		#form = EgresadoForm(self.request.POST, self.request.FILES)
		#usuario_form = UsuarioForm(self.request.POST, instance=self.object.usuario)
		experiencia_form = ExperienciaLaboralFormSet(self.request.POST,instance =self.object)
		direccion_form = DireccionFormSet(self.request.POST, instance=self.object)
		telefono_form = TelefonoFormSet(self.request.POST, instance= self.object)
		estudios_form = EstudiosFormSet(self.request.POST, instance=self.object)
		idioma_form = IdiomaFormSet(self.request.POST, instance=self.object)
		asociacion_form = AsociacionFormSet(self.request.POST, instance=self.object)
		premio_form = PremioFormSet(self.request.POST, instance=self.object)
		direccion_actual_form = DireccionActualForm(self.request.POST, instance=direccion_actual_object)
		if (form.is_valid() and experiencia_form.is_valid() and telefono_form.is_valid() and direccion_form.is_valid() and estudios_form.is_valid() and idioma_form.is_valid() and asociacion_form.is_valid() and premio_form.is_valid() and direccion_actual_form.is_valid() ):
			return self.form_valid(form, experiencia_form, telefono_form, direccion_form, estudios_form, asociacion_form, premio_form, idioma_form, direccion_actual_form)
		else:
			return self.form_invalid(form, experiencia_form, telefono_form, direccion_form, estudios_form, asociacion_form, premio_form, idioma_form, direccion_actual_form)

	def form_valid(self, form, experiencia_form, telefono_form, direccion_form, estudios_form, asociacion_form, premio_form, idioma_form, direccion_actual_form):
		"""
		Called if all forms are valid. Creates a Recipe instance along with
		associated Ingredients and Instructions and then redirects to a
		success page.
		"""
		#instanciamos el modelForm de usuario y lo guardamos pero no le damos commit
		
		#como exclui el campo ultimo ingreso necesito agregarlo antes de guardar
		
		#guardamos porque ya tenemos el usuario con todos sus datos
		
		
		#como un objeto egresado necesita una instancia de usuario entonces le pasamos la instancia que acabamos de crear de usuario
		egresado = form.save()
		#egresado.usuario = user
		#le paso la instancia de usuario a egresado para poder guardar correctamente
		egresado.ultima_modificacion = datetime.datetime.now()
		egresado.save()
		
		
		#ahora guardamos el form de egresado pero lo guardamos en el self.object
		self.object = egresado
		#como la direccion necesita una instancia de usuario se la asignamos al form de direccion
		#direccion_form.instance = user
		direccion_form.save()
		d_actual_object = direccion_actual_form.save(commit=False)
		d_actual_object.egresado = self.object
		d_actual_object.save()

		#print self.object
		idioma_form.save()
		asociacion_form.save()
		premio_form.save()
		#telefono_form.instance = self.object
		telefono_form.save()

		#experiencia_form.instance = self.object
		experiencia_form.save()
		estudios_form.save()

		
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form, experiencia_form, telefono_form, direccion_form, estudios_form, asociacion_form, premio_form, idioma_form, direccion_actual_form):
		"""
		Called if a form is invalid. Re-renders the context data with the
		data-filled forms and errors.
		"""
		return self.render_to_response(
			self.get_context_data(form=form,
                                  telefono_form=telefono_form,
                                  experiencia_form = experiencia_form,
                                  direccion_form = direccion_form,
                                  estudios_form = estudios_form,
                                  asociacion_form=asociacion_form,
                                  premio_form=premio_form,
                                  idioma_form=idioma_form,
                                  direccion_actual_form=direccion_actual_form))



@loginRequired
def usuarios(request):
	#con el try capturamos la excepcion si la variable de session no existe quiere decir que no esta logueado
	egresados = Egresado.objects.all().count()
	empresa = Empresa.objects.all().count()
	administrador = Administrador.objects.all().count()

	return render(request, 'usuarios/gestionUsuarios.html',{'egresados':egresados, 'empresa': empresa, 'administrador': administrador})
	

def addCoordinador(request):
	form = addAdministradorForm()
	
	if request.method == 'POST':
		form = addAdministradorForm(request.POST, request.FILES)
		if request.POST['password'] == request.POST['password2']:
			if form.is_valid():
			
				codigo = form.cleaned_data['codigo']
				password  = form.cleaned_data['password']
				passencryp = make_password(password)
				nombre = form.cleaned_data['nombre']
				apellidos = form.cleaned_data['apellidos']
				direccion = form.cleaned_data['direccion']
				telefono = form.cleaned_data['telefono']
				correo = form.cleaned_data['correo']
				foto = form.cleaned_data['foto']
				nuevo_usuario = Usuario.objects.create(codigo=codigo,password=passencryp, ultimo_ingreso=datetime.datetime.now())
				nuevo_coordinador = Administrador.objects.create(nombre=nombre,apellidos=apellidos, direccion=direccion,correo=correo,telefono=telefono, usuario=nuevo_usuario)
				if foto:
					nuevo_coordinador.foto = foto
					nuevo_coordinador.save()
				return HttpResponse('success')

	return render(request, "usuarios/addCoordinador.html", { 
    	'form':form,
    	})



class CoordinadorUpdateView(UpdateView):
	model = Administrador
	form_class = AdministradorForm
	#solo podra acceder si ha iniciado session
	@method_decorator(loginRequired)
	def dispatch(self, request, *args, **kwargs):
	    return super(CoordinadorUpdateView, self).dispatch(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		id_usuario = request.session['id_usuario']
		usuario = get_object_or_404(Usuario, pk=id_usuario)
		self.object = get_object_or_404(Administrador, usuario=usuario)
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		usuario_form = usuarioFormPassword({'codigo':usuario.codigo})
		return render(request,"usuarios/actualizarCoordinador.html", {'form':form, 'usuario_form':usuario_form} )

	def post(self, request, *args, **kwargs):
		id_usuario = request.session['id_usuario']
		usuario = get_object_or_404(Usuario, pk=id_usuario)
		print usuario
		self.object = get_object_or_404(Administrador, usuario=usuario)
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		usuario_form = usuarioFormPassword(request.POST)
		if (form.is_valid() and usuario_form.is_valid()):
			password_actual = usuario_form.cleaned_data['password_actual']
			password_nueva = usuario_form.cleaned_data['password_nueva']
			if password_actual and password_nueva:
				if check_password(password_actual, usuario.password):
					form.save()
					passencryp = make_password(password_nueva)
					usuario.password = passencryp
					usuario.save()
					mensaje = u'se ha guardado correctamente'
					clase = 'success'
					return render(request,"usuarios/actualizarCoordinador.html",{'form':form, 'usuario_form':usuario_form, 'mensaje':mensaje, 'clase':clase})
				else:
					mensaje = u'La contaseña actual es invalida'
					return render(request,"usuarios/actualizarCoordinador.html",{'form':form, 'usuario_form':usuario_form, 'mensaje':mensaje, 'clase':'danger'})
			else:
				adm_save= form.save()
				if adm_save:
					mensaje = u'se ha guardado correctamente'
					clase = 'success'
				else:
					mensaje = u'No se ha podido guardar'
					clase = 'danger'
				return render(request,"usuarios/actualizarCoordinador.html",{'form':form, 'usuario_form':usuario_form, 'mensaje':mensaje, 'clase':clase})	
					
		else:
			return render(request,"usuarios/actualizarCoordinador.html", {'form':form,
                                  'usuario_form':usuario_form,
                                  })



@loginRequired
def actualizarCoordinador(request):
	id_usuario = request.session['id_usuario']
	usuario =  get_object_or_404(Usuario, pk=id_usuario)
	admin = get_object_or_404(Administrador, usuario=usuario)

	form = actualizarAdministradorForm({'codigo':usuario.codigo,
										'nombre': admin.nombre,
										'apellidos': admin.apellidos,
										'direccion': admin.direccion,
										'correo': admin.correo,
										'telefono': admin.telefono,
										'foto': admin.foto})
	
	if request.method == 'POST':
		form = actualizarAdministradorForm(request.POST, request.FILES)
		
		if form.is_valid():
			codigo = form.cleaned_data['codigo']
			nombre = form.cleaned_data['nombre']
			apellidos =  form.cleaned_data['apellidos']
			direccion = form.cleaned_data['direccion']
			correo = form.cleaned_data['correo']
			telefono = form.cleaned_data['telefono']
			foto = form.cleaned_data['foto']
			cargo = form.cleaned_data['cargo']
			usuario.codigo = codigo
			usuario.save()
			admin.nombre = nombre
			admin.apellidos = apellidos
			admin.direccion = direccion
			admin.cargo = cargo
			admin.correo = correo
			admin.telefono = telefono
			if foto:
				admin.foto = foto
			guardado = admin.save()
			if guardado:
				mensaje = 'Se ha guardado correctamente'
			else:
				mensaje = 'No se pudo guardar en este momento, intente mas tarde'
						
			return render(request, "usuarios/actualizarCoordinador.html", { 
		    	'form':form,
		    	'mensaje': mensaje,
    		})


	return render(request, "usuarios/actualizarCoordinador.html", { 
    	'form':form,
    	})

@loginRequired
def addEmpresa(request):
	if request.method == 'POST':
		form = AddEmpresaForm(request.POST)

		#verificamos si las password concuerdan
		if request.POST['password'] == request.POST['password2']:
				#verificamos si es valido
			if form.is_valid():
				#obtenemos las variables del arreglo request.POST[] que contiene los atributos del formulario
				codigo = form.cleaned_data['codigo']
				password  = form.cleaned_data['password']
				passencryp = make_password(password)
				nombre = form.cleaned_data['nombre']
				tipo = form.cleaned_data['tipo']
				email = form.cleaned_data['email']
				#creacion de objeto usuario para obtener una instancia y poder relacionarla con un egresado
				nuevo_usuario = Usuario.objects.create(codigo=codigo,password=passencryp, ultimo_ingreso=datetime.datetime.now())
				#creacion de objeto egresado con relacion al objeto usuario creado anteriormente
				nuevo_empresa = Empresa.objects.create(nombre=nombre, tipo=tipo, email=email, usuario=nuevo_usuario)
				#si todo esta correcto redirigimos hacia la vista de la lista de usuarios
				return HttpResponseRedirect(reverse('listaEmpresas'))
	else:
		form = AddEmpresaForm()
	return render(request, 'usuarios/crearEmpresa.html',{'form' :form})



def home(request):
	return render_to_response('base.html', context_instance=RequestContext(request))


def panel(request):
	return render_to_response('usuarios/basePanel.html', context_instance=RequestContext(request))



def logout(request):
    try:
        del request.session['id_usuario']
    except KeyError:
        pass
    return HttpResponseRedirect(reverse('index'))

def iniciar_sesion(request):
	if request.method == 'POST':
		form = InicioSesionForm(request.POST)
		if form.is_valid():
			
			codigo = request.POST['codigo']
			clave = request.POST['clave']
			try:
				u = Usuario.objects.get(codigo=codigo)
				try:
					c = Administrador.objects.get(usuario=u)

					if check_password(clave, u.password):
						request.session['id_usuario']=u.id
						request.session['nombre_usuario']=c.nombre
						request.session['id_coordinador']=c.id
						if c.foto:
							request.session['urlimagen']=c.get_absolute_image_url()
						else:
							request.session['urlimagen']='http://placehold.it/50x50'
						if 'url_redirect'in request.GET:
							url = '%s' % request.GET['url_redirect']
						else:
							url = reverse('dashboard')
						
						return HttpResponseRedirect(url)
					else:
						form = InicioSesionForm()
						info = "Credenciales Invalidas"	
						return render_to_response('usuarios/iniciarSesion.html',{'info' :info,'form' :form}, context_instance=RequestContext(request))
				except ObjectDoesNotExist:
					info = "No tiene permisos para Acceder al Sistema"
					form = InicioSesionForm()
					return render_to_response('usuarios/iniciarSesion.html',{'info':info,'form' :form}, context_instance=RequestContext(request))
			except ObjectDoesNotExist:
				info = "Credenciales Invalidas"
				form = InicioSesionForm()
				return render_to_response('usuarios/iniciarSesion.html',{'info':info,'form' :form}, context_instance=RequestContext(request))

	else:
		form = InicioSesionForm()
	return  render(request, 'usuarios/iniciarSesion.html',{'form' :form})

@loginRequired
def listaEmpresas(request):
	query = request.POST.get('query')
	busqueda = request.POST.get('busqueda')
	estado = request.POST.get('estado')
	tipo = request.POST.get('tipo')
	if query:
		if busqueda == 'codigo':
			if estado == 'todos':
				if tipo == 'todos':
					empresas_list = Empresa.objects.filter(usuario__codigo__istartswith=query)
				else:
					empresas_list = Empresa.objects.filter(usuario__codigo__istartswith=query).filter(tipo__istartswith=tipo)
			else:
				if estado == 'habilitado':
					estado_bool = True
				elif estado == 'deshabilitado':
					estado_bool = False
				if tipo == 'todos':
					empresas_list = Empresa.objects.filter(usuario__codigo__istartswith=query).filter(estado=estado_bool)
				else:
					empresas_list = Empresa.objects.filter(usuario__codigo__istartswith=query).filter(estado=estado_bool).filter(tipo__istartswith=tipo)
		elif busqueda == 'nombre':
			if estado == 'todos':
				if tipo == 'todos':
					empresas_list = Empresa.objects.filter(nombre__istartswith=query)
				else:
					empresas_list = Empresa.objects.filter(nombre__istartswith=query).filter(tipo__istartswith=tipo)
			else:
				if estado == 'habilitado':
					estado_bool = True
				elif estado == 'deshabilitado':
					estado_bool = False
				if tipo == 'todos':
					empresas_list = Empresa.objects.filter(nombre__istartswith=query).filter(estado=estado_bool)
				else:
					empresas_list = Empresa.objects.filter(nombre__istartswith=query).filter(estado=estado_bool).filter(tipo__istartswith=tipo)
		elif busqueda == 'email':
			if estado == 'todos':
				if tipo == 'todos':
					empresas_list = Empres.objects.filter(email__istartswith=query)
				else:
					empresas_list = Empres.objects.filter(email__istartswith=query).filter(tipo__istartswith=tipo)
			else:
				if estado == 'habilitado':
					estado_bool = True
				elif estado == 'deshabilitado':
					estado_bool = False
				if tipo == 'todos':
					empresas_list = Empresa.objects.filter(email__istartswith=query).filter(estado=estado_bool)
				else:
					empresas_list = Empresa.objects.filter(email__istartswith=query).filter(estado=estado_bool).filter(tipo__istartswith=tipo)
		else:
			empresas_list = Empresa.objects.all()	
	else:
		empresas_list = Empresa.objects.all()

	return render_to_response('usuarios/empresa_list.html',{'empresas':empresas_list}, context_instance=RequestContext(request))

@loginRequired	
def listaEgresados(request):
	query = request.GET.get('query')
	busqueda = request.GET.get('busqueda')
	programa = request.GET.get('programa')
	if query:
		if busqueda == 'codigo':
			if programa == 'todas':
				egresados_list = Egresado.objects.filter(usuario__codigo__startswith=query)
			else:
				egresados_list = Egresado.objects.filter(usuario__codigo__startswith=query).filter(programa__nombre=programa)
		if busqueda == 'nombre':
			if programa == 'todas':
				egresados_list = Egresado.objects.filter(Q(p_nombre__istartswith=query) | Q(s_nombre__istartswith=query))
			else:
				egresados_list = Egresado.objects.filter(Q(p_nombre__istartswith=query) | Q(s_nombre__istartswith=query)).filter(programa__nombre=programa)
		if busqueda == 'apellido':
			if programa == 'todas':
				egresados_list = Egresado.objects.filter(Q(p_apellido__istartswith=query) | Q(s_apellido__istartswith=query))
			else:
				egresados_list = Egresado.objects.filter(Q(p_apellido__istartswith=query) | Q(s_apellido__istartswith=query)).filter(programa__nombre=programa)
		if busqueda == 'email':
			if programa == 'todas':
				egresados_list = Egresado.objects.filter(Q(usuario__email__istartswith=query) | Q(usuario__email__istartswith=query))
			else:
				egresados_list = Egresado.objects.filter(Q(usuario__email__istartswith=query) | Q(usuario__email__istartswith=query)).filter(programa__nombre=programa)
		if busqueda == 'cedula':
			if programa == 'todas':
				egresados_list = Egresado.objects.filter(Q(cedula__istartswith=query) | Q(cedula__istartswith=query))
			else:
				egresados_list = Egresado.objects.filter(Q(cedula__istartswith=query) | Q(cedula__istartswith=query)).filter(programa__nombre=programa)
	else:
		egresados_list = Egresado.objects.all()
		
	"""paginator = Paginator(egresados_list, 10)
	page = request.GET.get('page')
	try:
		egresados = paginator.page(page)
	except PageNotAnInteger:
        # If page is not an integer, deliver first page.
		egresados = paginator.page(1)
	except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
		egresados = paginator.page(paginator.num_pages)"""
	programas = Programa.objects.all()
	
	return render(request,'usuarios/egresado_list.html',{'egresados':egresados_list, 'accion':'Egresados', 'programa':programas}, context_instance=RequestContext(request))

@loginRequired
def listaEgresadosDesactualizados(request):
	query = request.GET.get('query')
	busqueda = request.GET.get('busqueda')
	programa = request.GET.get('programa')
	qs = Egresado.objects.filter(ultima_modificacion__lte=datetime.datetime.now() - datetime.timedelta(days=180)).exclude(estado='fallecido')
	if query:
		if busqueda == 'codigo':
			if programa == 'todas':
				egresados_list = qs.filter(usuario__codigo__startswith=query)
			else:
				egresados_list = qs.filter(usuario__codigo__startswith=query).filter(programa__nombre=programa)
		if busqueda == 'nombre':
			if programa == 'todas':
				egresados_list = qs.filter(Q(p_nombre__istartswith=query) | Q(s_nombre__istartswith=query))
			else:
				egresados_list = qs.filter(Q(p_nombre__istartswith=query) | Q(s_nombre__istartswith=query)).filter(programa__nombre=programa)
		if busqueda == 'apellido':
			if programa == 'todas':
				egresados_list = qs.filter(Q(p_apellido__istartswith=query) | Q(s_apellido__istartswith=query))
			else:
				egresados_list = qs.filter(Q(p_apellido__istartswith=query) | Q(s_apellido__istartswith=query)).filter(programa__nombre=programa)
		if busqueda == 'email':
			if programa == 'todas':
				egresados_list = qs.filter(Q(usuario__email__istartswith=query) | Q(usuario__email__istartswith=query))
			else:
				egresados_list = qs.filter(Q(usuario__email__istartswith=query) | Q(usuario__email__istartswith=query)).filter(programa__nombre=programa)
		if busqueda == 'cedula':
			if programa == 'todas':
				egresados_list = qs.filter(Q(cedula__istartswith=query) | Q(cedula__istartswith=query))
			else:
				egresados_list = qs.filter(Q(cedula__istartswith=query) | Q(cedula__istartswith=query)).filter(programa__nombre=programa)
	else:
		egresados_list = qs
		
	"""paginator = Paginator(egresados_list, 10)
	page = request.GET.get('page')
	try:
		egresados = paginator.page(page)
	except PageNotAnInteger:
        # If page is not an integer, deliver first page.
		egresados = paginator.page(1)
	except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
		egresados = paginator.page(paginator.num_pages)"""
	programas = Programa.objects.all()
	
	return render(request,'usuarios/egresado_list.html',{'egresados':egresados_list, 'accion':'Egresados desactualizados', 'programa':programas}, context_instance=RequestContext(request))




def busquedaAjax(request):
	if request.method == 'GET':
		col = request.GET['col']
		caja = request.GET['caja']
		if col =='codigo':
			egresados = Egresado.objects.filter(usuario__codigo__startswith=caja)

			data = serializers.serialize('json',egresados)
			return HttpResponse(data, content_type="application/json")
		if col == 'nombre':
			#egresados = Egresado.objects.filter(p_nombre__istartswith=caja)
			egresados = Egresado.objects.filter(Q(p_nombre__istartswith=caja) | Q(s_nombre__istartswith=caja))
			print egresados
			data = serializers.serialize('json', egresados)
			return HttpResponse(data, content_type="application/json")
		if col == 'apellido':
			#egresados = Egresado.objects.filter(p_nombre__istartswith=caja)
			egresados = Egresado.objects.filter(Q(p_apellido__istartswith=caja) | Q(s_apellido__istartswith=caja))
			print egresados
			data = serializers.serialize('json', egresados)
			return HttpResponse(data, content_type="application/json")
		if col == 'escuela':
			egresados = Egresado.objects.filter(programa__nombre__istartswith=caja)
			data = serializers.serialize('json', egresados)
			return HttpResponse(data, content_type="application/json")


@loginRequired
def detailEgresado(request, egresado_id):
	egresado = get_object_or_404(Egresado, pk=egresado_id)
	direcciones = Direccion.objects.filter(egresado=egresado)
	telefonos = Telefono.objects.filter(usuario=egresado)
	idiomas = Idioma.objects.filter(egresado=egresado)
	experiencia = ExperienciaLaboral.objects.filter(egresado=egresado)
	asociacion = Asociacion.objects.filter(egresado=egresado)
	estudios = EstudiosRealizados.objects.filter(egresado=egresado)
	direccionActual = DireccionActual.objects.filter(egresado=egresado)
	print direccionActual
	return render(request, 'usuarios/egresado_detalle.html', {'egresado': egresado, 'direcciones': direcciones, 'telefonos':telefonos, 'idiomas':idiomas, 'experiencia': experiencia, 'asociacion': asociacion, 'estudios':estudios, 'direccionActual':direccionActual})


class ProgramaListView(ListView):
	model = Programa
	template_name = 'usuarios/lista_programas.html'
	context_object_name = 'programas'

	@method_decorator(loginRequired)	
	def dispatch(self, request, *args, **kwargs):
	     return super(ProgramaListView, self).dispatch(request, *args, **kwargs) 


class ProgramaFormView(View):
	form_class = ProgramaForm
	template_name = 'usuarios/addPrograma.html'
	visible_fields = ['nombre']

	@method_decorator(loginRequired)
	def dispatch(self, request, *args, **kwargs):
	    return super(ProgramaFormView, self).dispatch(request, *args, **kwargs)
	def get(self, request, *args, **kwargs):
		form = self.form_class()
		return render(request, self.template_name,{'form': form})

	def post(self, request, *args, **kwargs):
		form_empty = self.form_class()
		form = self.form_class(request.POST)
		if form.is_valid():
			guardado = form.save()
			if guardado:
				mensaje = 'Se ha guardado correctamente!'
				clase = 'success'
			else:
				mensaje = 'No se ha guardado correctamente, intentelo de nuevo'
				clase = 'danger'
			return render(request, self.template_name, {'form': form_empty, 'clase':clase, 'mensaje':mensaje })
		else:
			mensaje = 'El formulario es incorrecto!'
			clase = 'danger'
			return render(request, self.template_name, {'form': form, 'clase':clase, 'mensaje':mensaje })

@loginRequired
def ProgramaUpdate(request, pk):#con el try capturamos la excepcion si la variable de session no existe quiere decir que no esta logueado
	p = get_object_or_404(Programa,pk=pk)

	if request.method == 'POST':
		form = ProgramaForm(request.POST)
		if form.is_valid():
			nombre = form.cleaned_data['nombre']
			#modificacion del objeto
			p.nombre = nombre
			p.save()
			return HttpResponseRedirect(reverse('listaProgramas'))

	if request.method == 'GET':
		form = ProgramaForm(initial={'nombre': p.nombre})
	

	return render(request, 'usuarios/updatePrograma.html', {'form': form})

@loginRequired
def eliminarPrograma(request, programa_id):
	if request.method == 'GET':
		programa = get_object_or_404(Programa, pk=programa_id)
		egresados = Egresado.objects.filter(programa=programa).count()
		if egresados > 0:
			return render(request,'usuarios/eliminarPrograma.html',{'mensaje':'El programa no se puede borrar porque contiene egresados'})
		else:
			programa.delete()
			return HttpResponseRedirect(reverse('listaProgramas'))

@loginRequired
def eliminarEgresado(request, egresado_id):
	if request.method == 'GET':
		egresado = get_object_or_404(Egresado, pk=egresado_id)
		usuario = egresado.usuario
		egresado.delete()
		usuario.delete()
		return HttpResponseRedirect(reverse('listaEgresados'))

@loginRequired		
def dashboard(request):
	if request.method == 'GET':
		fecha_actual = datetime.datetime.now()
		dia = datetime.datetime.now().day
		mes = datetime.datetime.now().month
		egresados = Egresado.objects.filter(fecha_cumple__month=mes).filter(fecha_cumple__day=dia)
		return render(request, 'usuarios/dashboard.html', {'egresados':egresados, 'fecha':fecha_actual})
	if request.method == 'POST':
		pass


