import autocomplete_light

from cities_light.models import City

#from cities_light.contrib.autocompletes import

class CityAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['search_names', ]

autocomplete_light.register(City, CityAutocomplete)