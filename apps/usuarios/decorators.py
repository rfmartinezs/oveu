from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from functools import wraps

def loginRequired(a):
	@wraps(a)
	def wrapper(request, *args, **kwargs):
		if 'id_usuario' in request.session:
			return a(request, *args, **kwargs)
		else:
			url_redirect =  request.get_full_path()
			url_callback= "%s?url_redirect=%s" % (reverse('login'), url_redirect)
			return HttpResponseRedirect(url_callback)
	return wrapper
		
def loginBlogRequired(a):
	@wraps(a)
	def wrapper(request, *args, **kwargs):
		try:
			user = request.session['id_usuario']
			return a(request, *args, **kwargs)
		except Exception, e:
			return HttpResponseRedirect(reverse('index'))
	return wrapper