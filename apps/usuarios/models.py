from django.db import models
from django.conf import settings
from cities_light.models import *
import datetime
from django.utils import timezone





class Usuario(models.Model):


	codigo = models.CharField(max_length=60)
	password = models.CharField(max_length=100)
	password_2 = models.CharField(max_length=100)
	fecha_creacion = models.DateTimeField(auto_now=True,auto_now_add=True)
	ultimo_ingreso = models.DateTimeField(auto_now=False, auto_now_add=False, default=datetime.datetime.now())
	estado = models.BooleanField(default=False)
	email = models.EmailField()
	#quitar null y blank para produccion
	codigo_confirmacion = models.CharField(max_length=100, null=True, blank=True)
   

	def __unicode__(self):
		return self.codigo

TE_CHOICES = (
    ('Publica','Publica'),
    ('Privada','Privada'),
    ('Mixta','Mixta')
)

class Empresa(models.Model):


	nombre = models.CharField(max_length=100)
	tipo = models.CharField(max_length=10, choices=TE_CHOICES)
	estado = models.BooleanField(default=True)
	usuario = models.OneToOneField(Usuario)

	def __unicode__(self):
		return self.nombre
    

class Administrador(models.Model):


	nombre = models.CharField(max_length=7)
	apellidos = models.CharField(max_length=40)
	cargo	= models.CharField(max_length=70)
	fecha_creacion = models.DateTimeField(auto_now_add=True)
	foto = models.ImageField(upload_to="foto_administrador", blank=True, null=True)
	usuario = models.OneToOneField(Usuario)
	direccion = models.CharField(max_length=120)
	telefono = models.BigIntegerField(null=True)

	def __unicode__(self):
		return self.nombre

	def get_absolute_image_url(self):
	    return '%s' %(self.foto.url)

class Programa(models.Model):


	nombre = models.CharField(max_length=100)
	#administrador = models.ForeignKey(Administrador)

	def __unicode__(self):
		return self.nombre

ESTADO = (
		('Desempleado', 'Desempleado'),
		('laborando en su profesion', 'Laborando en su Profesion'),
		('Laborando fuera de su profesion', 'Laborando fuera de su profesion'),
		('Laborando y estudiando', 'Laborando y Estudiando'),
		('Estudiando', 'Estudiando'),
		('Pensionado', 'Pensionado'),
	)

class Egresado(models.Model):


	p_nombre = models.CharField(max_length=45)
	s_nombre = models.CharField(max_length=45, null=True, blank=True)
	p_apellido = models.CharField(max_length=45)
	s_apellido = models.CharField(max_length=45)
	programa = models.ForeignKey(Programa)
	fecha_grado = models.DateField()
	estado_laboral = models.CharField(max_length=50, choices=ESTADO)
	ultima_modificacion = models.DateTimeField(auto_now_add=False, auto_now=False, default=datetime.datetime.now())
	imagen = models.ImageField(upload_to='foto_egresado',blank=True, null=True)
	usuario = models.OneToOneField(Usuario)
	fecha_cumple = models.DateField()
	cedula = models.BigIntegerField()
	estado = models.CharField(max_length=10, default="vivo")
	carnet = models.BooleanField(default=False)	

	def __unicode__(self):
		return self.p_nombre

	def esta_desactualizado(self):
		return self.ultima_modificacion <= datetime.datetime.now() - datetime.timedelta(days=180) 
		


class Direccion(models.Model):


	direccion = models.CharField(max_length=100)
	codigo_postal = models.IntegerField()
	ciudad = models.ForeignKey(City)
	egresado = models.ForeignKey(Egresado)
	

	def __unicode__(self):
		return self.direccion

class DireccionActual(models.Model):


	direccion = models.CharField(max_length=100)
	codigo_postal = models.IntegerField()
	ciudad = models.ForeignKey(City)
	egresado = models.ForeignKey(Egresado)
	

	def __unicode__(self):
		return self.direccion

TIPO_TELEFONO = (
		('casa', 'Casa'),
		('oficina', 'Oficina'),
		('celular', 'Celular'),
		('otro', 'Otro')
	)

class Telefono(models.Model):


	numero = models.BigIntegerField()
	tipo = models.CharField(max_length=20, choices=TIPO_TELEFONO)
	usuario = models.ForeignKey(Egresado)

	def __unicode__(self):
		return unicode(self.numero)

def listYear():
	YearWithString = [('actual','Actual')]
	fecha = datetime.datetime.now()
	fecha_inicio = fecha.year-70
	for x in xrange(fecha_inicio,fecha.year + 1 ):
		YearWithString.append((str(x),str(x)))

	return YearWithString

def listYearsOnly():
	Year = []
	fecha = datetime.datetime.now()
	fecha_inicio = fecha.year-70
	fecha_fin = fecha.year + 1
	for x in xrange(fecha_inicio,fecha_fin):
		Year.append((str(x),str(x)))

	return Year
	
TIPO_ESTUDIO = (
		('pregrado', 'Pregrado'),
		('diplomado', 'Diplomado'),
		('especializacion', 'Especializacion'),
		('subespecializacion', 'Subespecializacion'),
		('postgrado', 'Postgrado'),
		('doctorado', 'Doctorado'),
		('magister', 'Maestria'),
	)
class EstudiosRealizados(models.Model):


	tipo = models.CharField(choices=TIPO_ESTUDIO, max_length=40)
	titulo = models.CharField(max_length=100)
	ano_inicio = models.CharField(choices=listYearsOnly(),max_length=6)
	ano_fin = models.CharField(choices=listYear(),max_length=6)
	egresado = models.ForeignKey(Egresado)
	centro_estudio = models.CharField(max_length=100)
	ubicacion = models.CharField(max_length=70)

	def __unicode__(self):
		return self.tipo

class ExperienciaLaboral(models.Model):


	fecha_ingreso = models.DateField()
	fecha_salida = models.DateField()
	tipo_contrato = models.CharField(max_length=10)
	empresa = models.CharField(max_length=100)
	cargo = models.CharField(max_length=100)
	descripcion = models.TextField()
	egresado = models.ForeignKey(Egresado,null=True)

	def __unicode__(self):
		return self.tiempo_transcurrido

class Premio(models.Model):


	premio = models.CharField(max_length=100)
	fecha = models.DateField()
	descripcion = models.TextField()
	egresado = models.ForeignKey(Egresado)

	def __unicode__(self):
		return self.premio


NIVEL = (
		('BA', 'Bajo'),
		('ME', 'Medio'),
		('AL', 'Alto'),
	)

class Idioma(models.Model):
	idioma = models.CharField(max_length=20)
	nivel = models.CharField(max_length=2, choices=NIVEL)
	egresado = models.ForeignKey(Egresado)

	def __unicode__(self):
		return sel.idioma


class Asociacion(models.Model):
	nombre = models.CharField(max_length=100)
	fecha_ingreso = models.DateField()
	egresado = models.ForeignKey(Egresado)

	def __unicode__(self):
		return self.nombre
