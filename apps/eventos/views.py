from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.core.urlresolvers import reverse
from apps.eventos.forms import *
from apps.eventos.models import *
from apps.usuarios.decorators import loginRequired
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from apps.eventos.filters import EventoFilter
from .tasks import SendEmailEventTask
from django.core import serializers
import json
from django.conf import settings



#prueba de django_filters

def evento_list(request):
    f = EventoFilter(request.GET, queryset=Evento.objects.all())
    return render(request, 'eventos/template.html', {'filter': f})


@loginRequired
def listaEventos(request):
	f = EventoFilter(request.GET, queryset=Evento.objects.all())
	if request.method == 'POST':
		query = request.POST.get('query')
		busqueda = request.POST.get('busqueda')
		estado = request.POST.get('estado')
		print query
		if query and busqueda and estado:
			if busqueda == 'titulo':
				if estado == 'todos':
					evento = Evento.objects.filter(titulo__istartswith=query)
				if estado == 'publicados':
					evento = Evento.objects.filter(titulo__istartswith=query).filter(estado=True)
				if estado == 'sinpublicar':
					evento = Evento.objects.filter(titulo__istartswith=query).filter(estado=False)
			if busqueda == 'descripcion':
				if estado == 'todos':
					evento = Evento.objects.filter(descripcion__icontains=query)
				if estado == 'publicados':
					evento = Evento.objects.filter(descripcion__icontains=query).filter(estado=True)
				if estado == 'sinpublicar':
					evento = Evento.objects.filter(descripcion__icontains=query).filter(estado=False)

	else:
		evento = Evento.objects.all()

	return render_to_response('eventos/evento_list.html',{'eventos':evento, 'filter': f}, context_instance=RequestContext(request))
	
@loginRequired
def addEvento(request):
		if request.method == 'POST':
			form = EventoForm(request.POST, request.FILES)
			#verificamos si es valido
			if form.is_valid():
				a = Administrador.objects.get(pk=request.session['id_coordinador'])
				evento = form.save(commit=False)
				evento.coordinador = a
				#recogemos a quien le enviaremos la invitacion al evento por correo
				destinatarios = request.POST.get('envioCorreo',)
				asunto = request.POST.get('asunto',)
				
				if destinatarios != 0:
					if evento.estado:
						link = 'http://localhost:8000/evento/ver/%s' % (evento.id)

						SendEmailEventTask.delay(destinatarios, evento.titulo, evento.descripcion, evento.fecha_inicio.strftime('%d-%m-%Y %H:%M'), evento.fecha_fin.strftime('%d-%m-%Y %H:%M'), evento.telefono, evento.correo, link, asunto, evento.imagen.url)
						evento.enviado = True
					
				evento.save()
				return 	HttpResponseRedirect(reverse('eventos:listaEventos'))
			else:
				accion ="Crear"
				return render(request, 'eventos/addEvento.html',{'form' :form, 'accion':accion})
		
		else:
			form = EventoForm()
			accion ="Crear"
		return render(request, 'eventos/addEvento.html',{'form' :form, 'accion':accion})

@loginRequired
def editEvento(request, evento_id):
	e = get_object_or_404(Evento,pk=evento_id)
	if request.method == 'POST':
		form = EventoForm(request.POST, request.FILES)
		if form.is_valid():
			#Creo una isntancia de evento
			e.titulo = form.cleaned_data['titulo']
			e.descripcion = form.cleaned_data['descripcion']
			e.fecha_inicio = form.cleaned_data['fecha_inicio']
			e.fecha_fin = form.cleaned_data['fecha_fin']
			e.telefono = form.cleaned_data['telefono']
			e.correo = form.cleaned_data['correo']
			e.direccion = form.cleaned_data['direccion']
			imagen = form.cleaned_data['imagen']
			if imagen:
				e.imagen = imagen
			e.estado = form.cleaned_data['estado']
			a = Administrador.objects.get(pk=request.session['id_coordinador'])
			e.coordinador = a
			#recogemos a quien le enviaremos la invitacion al evento por correo
			destinatarios = request.POST.get('envioCorreo',)
			asunto = request.POST.get('asunto',)
			if not e.enviado:
				if destinatarios != 0:
					evento = e
					if evento.estado:
						link = 'http://localhost:8000/evento/ver/%s' % (evento.id)

						SendEmailEventTask.delay(destinatarios, evento.titulo, evento.descripcion, evento.fecha_inicio.strftime('%d-%m-%Y %H:%M'), evento.fecha_fin.strftime('%d-%m-%Y %H:%M'), evento.telefono, evento.correo, link, asunto, evento.imagen.url)
						e.enviado = True
			
			evento = e.save()

			return 	HttpResponseRedirect(reverse('eventos:listaEventos'))

	if request.method == 'GET':
		form = EventoForm(initial={
									'titulo': e.titulo,
									'descripcion': e.descripcion,
									'fecha_inicio': e.fecha_inicio,
									'fecha_fin': e.fecha_fin,
									'imagen': e.imagen,
									'estado': e.estado,
									'telefono': e.telefono,
									'correo': e.correo,
									'direccion': e.direccion
									
			})
	
		accion = "Editar"
		return render(request, 'eventos/addEvento.html', {'form': form, 'accion':accion, 'evento_object':e})

@loginRequired
def detalleEvento(request, evento_id):
	e = get_object_or_404(Evento,pk=evento_id)
	asistentes = Asistente.objects.filter(evento=e).count()
	return render(request, 'eventos/detalleEvento.html', {'evento':e, 'asistentes':asistentes})
	

@loginRequired
def PublicarEvento(request, evento_id):
	e = get_object_or_404(Evento,pk=evento_id)
	e.estado = True
	e.save()
	return HttpResponseRedirect(reverse('eventos:listaEventos'))

@loginRequired
def BorrarEvento(request, evento_id):
	e = get_object_or_404(Evento,pk=evento_id)
	e.delete()
	return HttpResponseRedirect(reverse('eventos:listaEventos'))
