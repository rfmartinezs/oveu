# -*- coding: utf-8 -*-
from django.forms import ModelForm
from django import forms
from .models import *


class EventoForm(ModelForm):
	
	class Meta:
		model = Evento
		exclude = ['coordinador','enviado']
		labels = {
			'estado': ('Publicar?'),
			'fecha_fin':('Fecha de finalizacion'),
		}
		help_texts = {
            'imagen': ('La imagen debe tener un ancho de 518 pixel, para mayor compatibilidad con el diseño, si no el diseño se podria ver mal.'),
        }

    