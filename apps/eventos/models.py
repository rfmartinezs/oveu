from django.db import models
from django.db.models.signals import pre_delete, pre_save
from django.dispatch import receiver
from apps.usuarios.models import Administrador, Egresado
import datetime

class Evento(models.Model):
	titulo = models.CharField(max_length=200)
	descripcion = models.TextField()
	fecha_inicio = models.DateTimeField()
	fecha_fin = models.DateTimeField()
	imagen = models.ImageField(upload_to='img_evento', blank=True, null=True)
	telefono = models.CharField(max_length=20)
	correo = models.EmailField()
	direccion = models.CharField(max_length=120)
	estado = models.BooleanField(default=False, verbose_name='Publicado')
	coordinador = models.ForeignKey(Administrador)
	fecha_creacion = models.DateTimeField(auto_now=True,auto_now_add=True, default=datetime.datetime.now())
	enviado = models.BooleanField(default=False, verbose_name='Correo enviado?')

	def __unicode__(self):
		return self.titulo

# borrar imagen del disco duro
@receiver(pre_delete, sender=Evento)
def post_pre_delete_handler(sender, instance, **kwargs):
	instance.imagen.delete(False)


class Asistente(models.Model):
	"""docstring for Asistente"""
	egresado = models.ForeignKey(Egresado)
	evento = models.ForeignKey(Evento)
	