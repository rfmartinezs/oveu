from oveu.celery import app
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
import requests

@app.task
def SendEmailEventTask(destinatarios, titulo, descripcion, fecha_inicio, fecha_fin, telefono, correo, link, asunto, imagen=None):

	""" 
		Esta tarea se encarga de enviar el correo de invitacion a las listas de emails que se desee, la lista de empresas, la de egresados o juntas.
		las tareas son manejadas con Celery, leer sobre esta antes de modificar. 

	"""

	# se identifica que tipo de destinatario se recoge y se asigna el respectivo nombre de la lista de email
	if destinatarios == 'empresa':
		to = settings.MAILGUN_LIST_EMPRESA
	elif destinatarios == 'egresado':
		to = settings.MAILGUN_LIST_EGRESADO
	elif destinatarios == 'todos':
		to = [settings.MAILGUN_LIST_EMPRESA, settings.MAILGUN_LIST_EGRESADO]
	
	
	# si por alguna cirscunstancia no se recoge ninguna variable destinatarios o es vacia o null, entonces no se enviara ningun correo, por el contrario se se recoge la variable destinatarios, se enviara el correo a las respectiva(s) lista(s) de emails

	if to:
		# para saber mas de como manejar los envios meiante la api https de mailgun por favor consultar https://documentation.mailgun.com/user_manual.html#sending-via-api
		full_path_image = '%s%s' % (settings.DOM_NAME, imagen)
		
		html_content = render_to_string('eventos/invitacionEvento.html', {'titulo':titulo,'descripcion':descripcion, 'fecha_inicio':fecha_inicio, 'fecha_fin':fecha_fin, 'telefono':telefono, 'correo':correo, 'link':link, 'imagen':full_path_image})
		text_content = strip_tags(html_content)
		
		send = requests.post(
	        "https://api.mailgun.net/v2/%s/messages" % (settings.MAILGUN_SERVER_NAME),
	        auth=("api", settings.MAILGUN_ACCESS_KEY),
	        data={"from": "Coordinador egresados <%s>" % (settings.CORREO_COORDINADOR),
	              "to": to,
	              "subject": asunto,
	              "text": text_content,
	              "html": html_content })

		

		return send
	