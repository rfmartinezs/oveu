# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Evento.enviado'
        db.add_column(u'eventos_evento', 'enviado',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Evento.enviado'
        db.delete_column(u'eventos_evento', 'enviado')


    models = {
        u'eventos.asistente': {
            'Meta': {'object_name': 'Asistente'},
            'egresado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            'evento': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['eventos.Evento']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'eventos.evento': {
            'Meta': {'object_name': 'Evento'},
            'coordinador': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Administrador']"}),
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'descripcion': ('django.db.models.fields.TextField', [], {}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'enviado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 3, 5, 0, 0)', 'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'fecha_fin': ('django.db.models.fields.DateTimeField', [], {}),
            'fecha_inicio': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'usuarios.administrador': {
            'Meta': {'object_name': 'Administrador'},
            'apellidos': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'telefono': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['usuarios.Usuario']", 'unique': 'True'})
        },
        u'usuarios.egresado': {
            'Meta': {'object_name': 'Egresado'},
            'carnet': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cedula': ('django.db.models.fields.BigIntegerField', [], {}),
            'estado': ('django.db.models.fields.CharField', [], {'default': "'vivo'", 'max_length': '10'}),
            'estado_laboral': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'fecha_cumple': ('django.db.models.fields.DateField', [], {}),
            'fecha_grado': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'p_apellido': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'p_nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Programa']"}),
            's_apellido': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            's_nombre': ('django.db.models.fields.CharField', [], {'max_length': '45', 'null': 'True', 'blank': 'True'}),
            'ultima_modificacion': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 3, 5, 0, 0)'}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['usuarios.Usuario']", 'unique': 'True'})
        },
        u'usuarios.programa': {
            'Meta': {'object_name': 'Programa'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'usuarios.usuario': {
            'Meta': {'object_name': 'Usuario'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'codigo_confirmacion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password_2': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ultimo_ingreso': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 3, 5, 0, 0)'})
        }
    }

    complete_apps = ['eventos']