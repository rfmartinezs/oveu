from django.conf.urls import patterns, url
from apps.eventos.views import *



urlpatterns = patterns('apps.eventos.views',
    # Examples:
    #url(r'^panel$', 'panel', name='panel'),
    #url(r'^login$', 'iniciar_sesion', name='login'),
    #url(r'^', 'listaEgresados', name='listaEgresados'),
    url(r'^/$', 'listaEventos', name='listaEventos'),
    url(r'^/add$', 'addEvento', name='addEvento'),
    url(r'^/edit/(?P<evento_id>\d+)/$', 'editEvento', name='editEvento'),
    url(r'^/publicar/(?P<evento_id>\d+)/$', 'PublicarEvento', name='publicarEvento'),
    url(r'^/borrar/(?P<evento_id>\d+)/$', 'BorrarEvento', name='borrarEvento'),
    url(r'^/detail/(?P<evento_id>\d+)/$', 'detalleEvento', name='detailEvento'),
    url(r'^/listar/', 'evento_list'),
    #url(r'^egresado/addegresado/$', 'addEgresado', name='addEgresado'),
    #url(r'^listaUsuarios/$', UsuariosList.as_view(), name='listaUsuarios'),
    #url(r'^addUsuario$', 'addUsuario', name='addUsuario'),
    #url(r'^listarUsuarios/$', 'listaUsuarios', name='listaUsuarios'),
    
    # url(r'^blog/', include('blog.urls')),


)
