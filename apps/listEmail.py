import requests
from django.conf import settings

def add_list_member(address, name, listemail, description=None):
    return requests.post(
        "https://api.mailgun.net/v2/lists/%s/members" % (listemail,),
        auth=('api', settings.MAILGUN_ACCESS_KEY),
        data={'subscribed': True,
              'address': address,
              'name': name,
              'description': description}
              )