from django.db import models
from apps.usuarios.models import Administrador, Usuario

TIPO_ENCUESTA = (
	('egresado', 'Egresados'),
	('empresa', 'Empresas'),
	('todos', 'Todos (egresados y empresas)')
)

class Encuesta(models.Model):


	titulo = models.CharField(max_length=100)
	descripcion = models.TextField()
	estado = models.BooleanField(default=False, verbose_name='Publicada?')
	imagen = models.ImageField(upload_to="img_encuesta", null=True, blank=True)
	tipo = models.CharField(max_length=15, choices=TIPO_ENCUESTA, default='egresado')
	administrador = models.ForeignKey(Administrador)
	enviado = models.BooleanField(default=False,)

	def __unicode__(self):
		return "%s" % self.titulo

	def realizadas(self):
		respuesta =  Respuesta.objects.filter(encuesta=self).values_list('usuario', flat=True).distinct().count()
		respuestav = RespuestaV.objects.filter(encuesta=self).values_list('usuario', flat=True).distinct().count()
		
		if respuesta == 0:
			if respuestav > 0:
				realizada = respuestav
			else:
				realizada = respuestav
		else:
			realizada = respuesta

		return realizada


TIPO_PREGUNTA = (
	('UN', 'Unica Respuesta'),
	('MU', 'Multiple Respuesta'),
	('AB', 'Desarrollo'),
)


class Pregunta(models.Model):

	pregunta = models.CharField(max_length=50)
	tipo = models.CharField(max_length=2, choices=TIPO_PREGUNTA)
	opcional = models.BooleanField(default=False)
	encuesta = models.ForeignKey(Encuesta)

	def __unicode__(self):
		return "%s" % self.pregunta
    
class Opcion(models.Model):


	opcion = models.CharField(max_length=70)
	pregunta = models.ForeignKey(Pregunta)
	
	def __unicode__(self):
		return self.opcion

class Respuesta(models.Model):


	fecha_creacion = models.DateTimeField(auto_now=True, auto_now_add=True)
	pregunta = models.ForeignKey(Pregunta)
	usuario = models.ForeignKey(Usuario)
	opcion = models.ForeignKey(Opcion)
	encuesta = models.ForeignKey(Encuesta)


class RespuestaV(models.Model):


	respuesta = models.TextField()
	pregunta = models.ForeignKey(Pregunta)
	usuario = models.ForeignKey(Usuario)
	encuesta = models.ForeignKey(Encuesta)

	def __unicode__(self):
		return self.respuesta
    