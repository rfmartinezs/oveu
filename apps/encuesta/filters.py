import django_filters
from .models import Encuesta

class EncuestaFilter(django_filters.FilterSet):
    class Meta:
        model = Encuesta
        fields = {'titulo': ['icontains',],
                  'descripcion': ['icontains'],
                  'estado':['exact'],
                  'tipo': ['exact'],
                  
                 }