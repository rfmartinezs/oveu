from django.shortcuts import render_to_response, render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse, reverse_lazy
from .models import *
from .forms import *
from django.views.generic import ListView, CreateView, View, UpdateView
from django.utils.decorators import method_decorator
from apps.usuarios.decorators import loginRequired
import json
import datetime
from wkhtmltopdf.views import PDFTemplateResponse, PDFTemplateView 
import tablib
from .filters import EncuestaFilter
from .tasks import SendEmailSurveyTask

def encuesta_list(request):
    f = EncuestaFilter(request.GET, queryset=Encuesta.objects.all())
    return render(request, 'encuesta/encuesta_list.html', {'filter': f})

def exportPreguntasA(request, encuesta_id):
	encuesta = get_object_or_404(Encuesta, pk=encuesta_id)
	respuestas = RespuestaV.objects.filter(encuesta=encuesta)
	data = tablib.Dataset()
	for respuesta in respuestas:
		print respuesta.pregunta.pregunta
		print respuesta.respuesta
		data.append((respuesta.pregunta.pregunta, respuesta.respuesta))
	data.headers = ['pregunta', 'respuesta']
	response = HttpResponse(data.xls, content_type='application/vnd.ms-excel;charset=utf-8')
	response['Content-Disposition'] = "attachment; filename=export.xls"
	return response

class EstadisticaView(View):
	def get(self, request, *args, **kwargs):
		encuesta = get_object_or_404(Encuesta, pk=self.kwargs['encuesta_id'])
		#respuestas = Respuesta.objects.filter(encuesta=encuesta)
		preguntas = Pregunta.objects.filter(encuesta=encuesta)
		opcionesList = {}
		ab = 0;
		for pregunta in preguntas:

			if pregunta.tipo == 'UN':
				opciones = Opcion.objects.filter(pregunta=pregunta)
				for op in opciones:
					respuesta = Respuesta.objects.filter(opcion=op).count()
					opcionDict = {op.opcion: respuesta}
					opcionesList.setdefault(pregunta.pregunta, [])
					opcionesList[pregunta.pregunta].append(opcionDict)
			if pregunta.tipo == 'MU':
				opciones = Opcion.objects.filter(pregunta=pregunta)
				for op in opciones:
					respuesta = Respuesta.objects.filter(opcion=op).count()
					opcionDict = {op.opcion: respuesta}
					opcionesList.setdefault(pregunta.pregunta, [])
					opcionesList[pregunta.pregunta].append(opcionDict)
			if pregunta.tipo == 'AB':
				ab +=1
		return render(request, 'encuesta/estadistica.html', locals())

class EstadisticaViewPdf(View):
	def get(self, request, *args, **kwargs):
		encuesta = get_object_or_404(Encuesta, pk=self.kwargs['encuesta_id'])
		#respuestas = Respuesta.objects.filter(encuesta=encuesta)
		preguntas = Pregunta.objects.filter(encuesta=encuesta)
		opcionesList = {}
		for pregunta in preguntas:

			if pregunta.tipo == 'UN':
				opciones = Opcion.objects.filter(pregunta=pregunta)
				for op in opciones:
					respuesta = Respuesta.objects.filter(opcion=op).count()
					opcionDict = {op.opcion: respuesta}
					opcionesList.setdefault(pregunta.pregunta, [])
					opcionesList[pregunta.pregunta].append(opcionDict)
				#preguntasData[pregunta.pregunta] = opcionDict
				#listData.append(opcionesList)
			if pregunta.tipo == 'MU':
				opciones = Opcion.objects.filter(pregunta=pregunta)
				for op in opciones:
					respuesta = Respuesta.objects.filter(opcion=op).count()
					opcionDict = {op.opcion: respuesta}
					opcionesList.setdefault(pregunta.pregunta, [])
					opcionesList[pregunta.pregunta].append(opcionDict) 
				#preguntasData[pregunta.pregunta] = opcionDict"""
			#listData.append(opcionesList)
		#print listData
		#print opcionesList
		nombre_pdf = 'EstadisticaPdf-%s-%s.pdf' % (datetime.datetime.now(), encuesta.titulo)
		return PDFTemplateResponse(request=request,
									template="encuesta/estadisticaPdf.html",
									context=locals(),
									filename=nombre_pdf,
									show_content_in_browser=True,
									header_template='informes/header.html',
									footer_template="informes/footer.html", 
									cmd_options = {'margin-top':25, 'margin-bottom':20}
									)

	
@loginRequired
def EncuestaListView(request):
	busqueda = request.POST.get('busqueda')
	query = request.POST.get('query')
	if request.method == 'POST':
		if query and busqueda:
			if busqueda == 'titulo':
				encuestas = Encuesta.objects.filter(titulo__istartswith=query)
			elif busqueda == 'descripcion':
				encuestas = Encuesta.objects.filter(descripcion__icontains=query)
			else:
				encuestas = Encuesta.objects.all()
			#return render(request, 'encuesta/encuesta_list.html', {'encuestas':encuestas})						
		else:
			encuestas = Encuesta.objects.all()
	else:
		encuestas = Encuesta.objects.all()
	return render(request, 'encuesta/encuesta_list.html', {'encuestas':encuestas})

class EncuestaCreateView(CreateView):
	model = Encuesta
	form_class = EncuestaCreateForm
	template_name = "encuesta/crear_encuesta.html"
	success_url = reverse_lazy('crearPregunta')

	@method_decorator(loginRequired)
	def dispatch(self, request, *args, **kwargs):
		return super(EncuestaCreateView, self).dispatch(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		return self.render_to_response(self.get_context_data(form=form,))

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if (form.is_valid()):
			encuesta = form.save(commit=False)
			ad = get_object_or_404(Administrador, pk=request.session['id_coordinador'])
			encuesta.administrador = ad
			encuesta.save()
			return HttpResponseRedirect(reverse('encuesta:crearPreguntas', args=(encuesta.pk,)))
		else:
			return self.render_to_response(self.get_context_data(form=form,))


class EditarEncuestaView(UpdateView):
	model = Encuesta
	form_class = EncuestaUpdateForm
	template_name = 'encuesta/editar_encuesta.html'

	@method_decorator(loginRequired)
	def dispatch(self, request, *args, **kwargs):
	    return super(EditarEncuestaView, self).dispatch(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		encuesta_id = self.kwargs['encuesta_id']
		self.object = get_object_or_404(Encuesta, pk=encuesta_id)
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		return self.render_to_response(self.get_context_data(form = form,))


	def post(self, request, *args, **kwargs):
		encuesta_id = self.kwargs['encuesta_id']
		self.object = get_object_or_404(Encuesta, pk=encuesta_id)
		form_class = self.get_form_class()
		form = self.get_form(form_class)

		if (form.is_valid()):
			encuesta = form.save()
			encuesta.save()
			if request.POST['btnActionEncuesta'] == 'Guardar y Modificar Preguntas':
				print 'fue presionado el boton preguntas'
			if request.POST['btnActionEncuesta'] == 'Guardar y terminar':
				print 'fue presionado el boton guardar'

			return HttpResponseRedirect(reverse('encuesta:crearPreguntas', args=(encuesta.pk,)))
		else:
			return self.render_to_response(self.get_context_data(form=form,))


class EditarEncuestaPreguntaView(UpdateView):
	model = Encuesta
	form_class = EncuestaUpdateForm
	template_name = 'encuesta/editar_pregunta_encuesta.html'

	@method_decorator(loginRequired)
	def dispatch(self, request, *args, **kwargs):
	    return super(EditarEncuestaPreguntaView, self).dispatch(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		
		encuesta_id = self.kwargs['encuesta_id']

		encuesta = get_object_or_404(Encuesta, pk=encuesta_id)
		self.object = encuesta
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		preguntas = Pregunta.objects.filter(encuesta=encuesta)
		print encuesta.estado
		opciones = {}
		for pregunta in preguntas:
			op = Opcion.objects.filter(pregunta=pregunta)

			for o in op:
				#opc = {}
				opciones.setdefault(pregunta.pk, [])

				opc = [o.pk, o.opcion]
				opciones[pregunta.pk].append(opc)
		print opciones
		return self.render_to_response(self.get_context_data(form = form, preguntas = preguntas, opciones=opciones, encuesta=encuesta))


	def post(self, request, *args, **kwargs):
		encuesta_id = self.kwargs['encuesta_id']
		self.object = get_object_or_404(Encuesta, pk=encuesta_id)
		form_class = self.get_form_class()
		form = self.get_form(form_class)

		if (form.is_valid()):
			encuesta = form.save()
			encuesta.save()
			"""if request.POST['btnActionEncuesta'] == 'Guardar y Modificar Preguntas':
				print 'fue presionado el boton preguntas'
			if request.POST['btnActionEncuesta'] == 'Guardar y terminar':
				print 'fue presionado el boton guardar'"""
			preguntas = Pregunta.objects.filter(encuesta=encuesta)
			opciones = {}
			for pregunta in preguntas:
				op = Opcion.objects.filter(pregunta=pregunta)

				for o in op:
					#opc = {}
					opciones.setdefault(pregunta.pk, [])

					opc = [o.pk, o.opcion]
					opciones[pregunta.pk].append(opc)
			return self.render_to_response(self.get_context_data(form=form,preguntas = preguntas, opciones=opciones, encuesta=self.object))
			#return HttpResponseRedirect(reverse('encuesta:crearPreguntas', args=(encuesta.pk,)))
		else:
			return self.render_to_response(self.get_context_data(form=form,))

@loginRequired
def suspenderEncuesta(request, encuesta_id):
	if request.method=='GET':
		encuesta = get_object_or_404(Encuesta, pk=encuesta_id)
		if encuesta.estado:
			encuesta.estado = False
			encuesta.save()
		return HttpResponseRedirect(reverse_lazy('encuesta:editarEncuesta', kwargs={'encuesta_id':encuesta_id}))

class EditarEncuestaPreguntaViewXeditable(UpdateView):
	model = Encuesta
	form_class = EncuestaUpdateForm
	template_name = 'encuesta/xeditable.html'

	@method_decorator(loginRequired)
	def dispatch(self, request, *args, **kwargs):
	    return super(EditarEncuestaPreguntaViewXeditable, self).dispatch(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		
		encuesta_id = self.kwargs['encuesta_id']

		encuesta = get_object_or_404(Encuesta, pk=encuesta_id)
		self.object = encuesta
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		preguntas = Pregunta.objects.filter(encuesta=encuesta)
		opciones = {}
		for pregunta in preguntas:
			op = Opcion.objects.filter(pregunta=pregunta)

			for o in op:
				opciones.setdefault(pregunta.pk, [])
				opc = [o.pk, o.opcion]
				opciones[pregunta.pk].append(opc)
				print opciones 
		return self.render_to_response(self.get_context_data(form = form, preguntas = preguntas, opciones=opciones, encuesta=encuesta))


	def post(self, request, *args, **kwargs):
		encuesta_id = self.kwargs['encuesta_id']
		self.object = get_object_or_404(Encuesta, pk=encuesta_id)
		form_class = self.get_form_class()
		form = self.get_form(form_class)

		if (form.is_valid()):
			encuesta = form.save()
			preguntas = Pregunta.objects.filter(encuesta=encuesta).count()
			if not preguntas:
				return self.render_to_response(self.get_context_data(form=form,info="la encuesta no tiene preguntas, por favor cree preguntas para esta encuesta"))
			return HttpResponseRedirect(reverse('encuesta:verEncuesta', args=(encuesta.pk,)))
		else:
			return self.render_to_response(self.get_context_data(form=form,))
@loginRequired
def editarOpcion(request, pk):
	opcion = get_object_or_404(Opcion, pk=pk)
	opcion.opcion = request.POST['value']
	opcion.save()
	data = json.dumps(request.POST)
	return HttpResponse(data, mimetype='application/json')

@loginRequired
def editarPregunta(request, pk):
	pregunta = get_object_or_404(Pregunta, pk=pk)
	pregunta.pregunta = request.POST['value']
	pregunta.save()
	data = json.dumps(request.POST)
	return HttpResponse(data, mimetype='application/json')


class EncuestaPreguntaCreateView(CreateView):
	model = Pregunta
	form_class = PreguntaForm
	template_name = "encuesta/crear_pregunta.html"
	success_url = reverse_lazy()

	@method_decorator(loginRequired)
	def dispatch(self, request, *args, **kwargs):
		return super(EncuestaPreguntaCreateView, self).dispatch(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		encuesta = get_object_or_404(Encuesta, pk=self.kwargs['encuesta_id'])
		#encuesta = self.kwargs['encuesta_id']
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		opcion_form = OpcionFormSet()
		return self.render_to_response(self.get_context_data(form=form, opcion_form=opcion_form, encuesta = encuesta))
		
	def post(self, request, *args, **kwargs):
		encuesta = get_object_or_404(Encuesta, pk=self.kwargs['encuesta_id'])
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		opcion_form = OpcionFormSet(self.request.POST)

		if 'add' in request.POST:
			boton = 'add'
		else:
			boton = 'end'

		if (form.is_valid() and opcion_form.is_valid()):
			return self.form_valid(form, opcion_form, encuesta, boton)
		else:
			return self.form_invalid(form, opcion_form, encuesta)


	def form_valid(self, form, opcion_form, encuesta, boton):
		"""
		Called if all forms are valid. Creates a Recipe instance along with
		associated Ingredients and Instructions and then redirects to a
		success page.

		"""


		tipo = form.cleaned_data['tipo']
		opciones_llenas = 0
		if tipo == 'AB':
			""" si el tipo de pregunta es abierta no hay necesidad de validar si se diligenciaron opcion, tampoco guardarlas"""
			pregunta = form.save(commit=False)
			pregunta.encuesta = encuesta
			pregunta.save()
			
		
		if tipo == 'MU' or tipo == 'UN':
			for form_opcion in opcion_form:
				try:
					if form_opcion.cleaned_data['opcion']:
						opciones_llenas +=1
						
				except Exception, e:
					pass

			if opciones_llenas < 2:
				info = "Esta pregunta por lo menos debe tener 2 opciones"
				return render(self.request,'encuesta/crear_pregunta.html', {'form':form, 'opcion_form': opcion_form, 'encuesta':encuesta, 'info':info})
			else:
				pregunta = form.save(commit=False)
				pregunta.encuesta = encuesta
				pregunta.save()
				self.object = pregunta
				opcion_form.instance = self.object
				opcion_form.save()
						


		if boton == 'add':
			return HttpResponseRedirect(reverse('encuesta:crearPreguntas', args=(encuesta.pk,)))
		else:
			print boton
		return HttpResponseRedirect(reverse('encuesta:verEncuesta', kwargs={'encuesta_id':pregunta.encuesta.id}))

	def form_invalid(self, form, opcion_form, encuesta):
		return self.render_to_response(self.get_context_data(form=form, opcion_form=opcion_form, encuesta = encuesta))

		

class EditarPreguntaView(View):
	
	@method_decorator(loginRequired)
	def dispatch(self, request, *args, **kwargs):
	    return super(EditarPreguntaView, self).dispatch(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		#encuesta_id = self.kwargs['encuesta_id']
		pregunta_id = self.kwargs['pregunta_id']
		pregunta = get_object_or_404(Pregunta, pk=pregunta_id)
		form = PreguntaForm(instance=pregunta)
		opcion = OpcionFormSet(instance=pregunta)
		return render(request,"encuesta/editar_pregunta_popup.html", {'opcion':opcion, 'form':form})


	def post(self, request, *args, **kwargs):
		pregunta_id = self.kwargs['pregunta_id']
		pregunta = get_object_or_404(Pregunta, pk=pregunta_id)
		form = PreguntaForm(request.POST, instance=pregunta)
		opcion = OpcionFormSet(request.POST, instance=pregunta)
		pregunta_tipo = pregunta.tipo

		if (form.is_valid() and opcion.is_valid()):
			return self.form_valid(form, opcion, pregunta, pregunta_tipo)
		else:
			return self.form_invalid(form, opcion)


	def form_valid(self, form, opcion, pregunta, pregunta_tipo):
		"""
		Funcion para cuando sea valido el form de la pregunta y el de las opciones

		"""


		tipo = form.cleaned_data['tipo']

		info = "Se ha guardado correctamente"

		opciones_llenas = 0
		if tipo == 'AB':
			""" si el tipo de pregunta es abierta no hay necesidad de validar si se diligenciaron opcion, tampoco guardarlas. ademas si se ha cambiado de una pregunta multiple o unica a pregunta de desarrollo, entonces se deberia borrar las opciones anteriormente guardadas"""
			
			if pregunta_tipo == 'MU' or pregunta_tipo =='UN':
				opcion2 = OpcionFormSet(instance=pregunta)
				opciones = Opcion.objects.filter(pregunta=pregunta)
				print opciones
				opciones.delete()
				form.save()
				info = "Se han elminado las opciones anteriores, para volver la pregunta abierta"
				return render(self.request,"encuesta/editar_pregunta_popup.html", {'form':form,'info':info, 'opcion':opcion2 })
			else:
				opcion2 = OpcionFormSet(instance=pregunta)
				form.save()
				info = "Se ha guardado correctamente"
				return render(self.request,"encuesta/editar_pregunta_popup.html", {'form':form, 'opcion':opcion2, 'info':info})



		
		if tipo == 'MU' or tipo == 'UN':
			for form_opcion in opcion:
				try:
					if form_opcion.cleaned_data['opcion']:
						opciones_llenas +=1
						
				except Exception, e:
					pass

			if opciones_llenas < 2:
				info = "Esta pregunta por lo menos debe tener 2 opciones"
				return render(self.request,"encuesta/editar_pregunta_popup.html", {'form':form, 'opcion_form': opcion,'info':info})
			else:
				form.save()
				#opcion.instance = pregunta_object
				opcion.save()
						


		return render(self.request,"encuesta/editar_pregunta_popup.html", {'opcion':opcion, 'form':form, 'info':info})

	def form_invalid(self, form, opcion):
		return render(self.request,"encuesta/editar_pregunta_popup.html", {'opcion':opcion, 'form':form})


class VisualizarEncuestaCreateView(CreateView):
	

	template_name = "encuesta/ver_encuesta.html"
	success_url = reverse_lazy('encuesta:listaEncuesta')

	@method_decorator(loginRequired)
	def dispatch(self, request, *args, **kwargs):
		return super(VisualizarEncuestaCreateView, self).dispatch(request, *args, **kwargs)


	def get(self, request, *args, **kwargs):

		self.object = None
		encuesta = get_object_or_404(Encuesta, pk=self.kwargs['encuesta_id'])
		usuario = get_object_or_404(Usuario, pk=request.session['id_usuario'])
		p = Pregunta.objects.filter(encuesta=encuesta)
		op = Opcion.objects.filter(pregunta__encuesta=encuesta)
		num_preguntas = p.count()
		return self.render_to_response(self.get_context_data(encuesta=encuesta, preguntas=p, opcion = op, num_preguntas = num_preguntas))
			
		
	def post(self, request, *args, **kwargs):

		self.object = None
		encuesta = get_object_or_404(Encuesta, pk=self.kwargs['encuesta_id'])
		encuesta.estado = True
		#encuesta.save(commit=False)

		enviar = request.POST.get('enviar',)
		print enviar
		if not encuesta.enviado:
			if enviar:

				asunto = request.POST.get('asunto',)
				
				if asunto:

					if encuesta.estado:
						
						link = 'http://localhost:8000/encuesta/ver/%s' % (encuesta.id)

						SendEmailSurveyTask.delay(asunto, encuesta.titulo, encuesta.descripcion, encuesta.tipo, link, encuesta.imagen.url)
						encuesta.enviado = True

		encuesta.save()


		return HttpResponseRedirect(reverse('encuesta:listaEncuesta'))
		

#from django.core import serializers
@loginRequired
def eliminarPregunta(request, pregunta_id):
	if request.method == 'GET':
		pregunta = get_object_or_404(Pregunta, pk=pregunta_id)
		encuesta = pregunta.encuesta.id
		#opciones = Opcion.objects.filter(pregunta=pregunta)
		#opcionesData = serializers.serialize('json', opciones)
		#opciones.delete()
		pregunta.delete()
	
		return HttpResponseRedirect(reverse('encuesta:editarEncuesta', args=(encuesta, )))
	else:
		return HttpResponse('Hola como estas?')


@loginRequired
def eliminarOpcion(request, opcion_id):
	if request.method == 'POST':
		opcion = get_object_or_404(Opcion, pk=opcion_id)
		#opciones.delete()
		#pregunta.delete()
		
		return HttpResponse(opcion)
	else:
		return HttpResponse('Hola como estas?')


@loginRequired
def eliminarEncuesta(request, encuesta_id):
	if request.method=='GET':
		encuesta = get_object_or_404(Encuesta, pk=encuesta_id)
		encuesta.delete()
		return HttpResponseRedirect(reverse_lazy('encuesta:listaEncuesta'))
