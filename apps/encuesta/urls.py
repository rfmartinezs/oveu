from django.conf.urls import patterns, url
from apps.encuesta.views import *



urlpatterns = patterns('apps.encuesta.views',
    # Examples:
    url(r'^/$','encuesta_list', name='listaEncuesta'),
    url(r'^/export/(?P<encuesta_id>\d+)/$','exportPreguntasA', name='export'),   
    url(r'^/helpers/editaropcion/(?P<pk>\d+)/$', 'editarOpcion', name='editOpcion'),
    url(r'^/helpers/editarpregunta/(?P<pk>\d+)/$', 'editarPregunta', name='editPregunta'),
    url(r'^/helpers/eliminarpregunta/(?P<pregunta_id>\d+)/$', 'eliminarPregunta', name='eliminarPregunta'),
    url(r'^/helpers/eliminaropcion/(?P<opcion_id>\d+)/$', 'eliminarOpcion', name='eliminarOpcion'),
    url(r'^/crear$', EncuestaCreateView.as_view(), name='crearEncuesta'),
    #url(r'^encuesta/preguntas/(?P<encuesta_id>\d+)/$', 'crearPregunta', name='crearPreguntas'),
    url(r'^/preguntas/(?P<encuesta_id>\d+)/$', EncuestaPreguntaCreateView.as_view(), name='crearPreguntas'),
    url(r'^/ver/(?P<encuesta_id>\d+)/$', VisualizarEncuestaCreateView.as_view(), name='verEncuesta'),
    url(r'^/estadistica/(?P<encuesta_id>\d+)/$', EstadisticaView.as_view(), name='estadisticaEncuesta'),
    url(r'^/estadistica/(?P<encuesta_id>\d+)/pdf/$', EstadisticaViewPdf.as_view(), name='estadisticaPdfEncuesta'),
    #url(r'^/editar/(?P<encuesta_id>\d+)/$', EditarEncuestaView.as_view(), name='editarEncuesta'),
    #url(r'^/editar/(?P<encuesta_id>\d+)/$', EditarEncuestaPreguntaView.as_view(), name='editarPreguntaEncuesta'),
    url(r'^/suspender/(?P<encuesta_id>\d+)/$', 'suspenderEncuesta', name='suspenderEncuesta'),
    url(r'^/eliminar/(?P<encuesta_id>\d+)/$', 'eliminarEncuesta', name='eliminarEncuesta'),
    url(r'^/editar/(?P<encuesta_id>\d+)/$', EditarEncuestaPreguntaViewXeditable.as_view(), name='editarEncuesta'),
    url(r'^/editar/preguntas/(?P<encuesta_id>\d+)/(?P<pregunta_id>\d+)/$', EditarPreguntaView.as_view(), name='editarPregunta'),
    #url(r'^login$', 'iniciar_sesion', name='login'),
    #url(r'^', 'listaEgresados', name='listaEgresados'),
    #url(r'^egresado/addegresado/$', 'addEgresado', name='addEgresado'),
    #url(r'^listaUsuarios/$', UsuariosList.as_view(), name='listaUsuarios'),
    #url(r'^addUsuario$', 'addUsuario', name='addUsuario'),
    #url(r'^listarUsuarios/$', 'listaUsuarios', name='listaUsuarios'),
    
    # url(r'^blog/', include('blog.urls')),


)
