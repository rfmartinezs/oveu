from django import forms
from django.forms.models import inlineformset_factory
from .models import *
from django.forms.models import BaseInlineFormSet

#validacion para que el primero de los inline formset sea requerido
class CustomInlineFormSet(BaseInlineFormSet):
	def clean(self):
        # get forms that actually have valid data
		count = 0
		for form in self.forms:
			try:
				if form.cleaned_data:
					count += 1
			except AttributeError:
				# annoyingly, if a subform is invalid Django explicity raises
				# an AttributeError for cleaned_data
				pass
		if count < 1:
			raise forms.ValidationError('You must have at least one order')

class EncuestaUpdateForm(forms.ModelForm):
	class Meta:
		model = Encuesta
		exclude = ['administrador', 'estado', 'enviado', 'tipo']

class EncuestaCreateForm(forms.ModelForm):
	class Meta:
		model = Encuesta
		exclude = ['administrador', 'estado', 'enviado']

class PreguntaForm(forms.ModelForm):
    class Meta:
        model = Pregunta
        exclude = ['encuesta',]

class OpcionForm(forms.ModelForm):
    class Meta:
        model = Opcion
        exclude = ['pregunta',]

OpcionFormSet = inlineformset_factory(Pregunta, Opcion, form=OpcionForm, can_delete=True, extra=0)
