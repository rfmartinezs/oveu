# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Encuesta'
        db.create_table(u'encuesta_encuesta', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('descripcion', self.gf('django.db.models.fields.TextField')()),
            ('estado', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('imagen', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(default='egresado', max_length=15)),
            ('administrador', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Administrador'])),
        ))
        db.send_create_signal(u'encuesta', ['Encuesta'])

        # Adding model 'Pregunta'
        db.create_table(u'encuesta_pregunta', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pregunta', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('opcional', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('encuesta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['encuesta.Encuesta'])),
        ))
        db.send_create_signal(u'encuesta', ['Pregunta'])

        # Adding model 'Opcion'
        db.create_table(u'encuesta_opcion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('opcion', self.gf('django.db.models.fields.CharField')(max_length=70)),
            ('pregunta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['encuesta.Pregunta'])),
        ))
        db.send_create_signal(u'encuesta', ['Opcion'])

        # Adding model 'Respuesta'
        db.create_table(u'encuesta_respuesta', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha_creacion', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, auto_now_add=True, blank=True)),
            ('pregunta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['encuesta.Pregunta'])),
            ('usuario', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Usuario'])),
            ('opcion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['encuesta.Opcion'])),
            ('encuesta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['encuesta.Encuesta'])),
        ))
        db.send_create_signal(u'encuesta', ['Respuesta'])

        # Adding model 'RespuestaV'
        db.create_table(u'encuesta_respuestav', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('respuesta', self.gf('django.db.models.fields.TextField')()),
            ('pregunta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['encuesta.Pregunta'])),
            ('usuario', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Usuario'])),
            ('encuesta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['encuesta.Encuesta'])),
        ))
        db.send_create_signal(u'encuesta', ['RespuestaV'])


    def backwards(self, orm):
        # Deleting model 'Encuesta'
        db.delete_table(u'encuesta_encuesta')

        # Deleting model 'Pregunta'
        db.delete_table(u'encuesta_pregunta')

        # Deleting model 'Opcion'
        db.delete_table(u'encuesta_opcion')

        # Deleting model 'Respuesta'
        db.delete_table(u'encuesta_respuesta')

        # Deleting model 'RespuestaV'
        db.delete_table(u'encuesta_respuestav')


    models = {
        u'encuesta.encuesta': {
            'Meta': {'object_name': 'Encuesta'},
            'administrador': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Administrador']"}),
            'descripcion': ('django.db.models.fields.TextField', [], {}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'default': "'egresado'", 'max_length': '15'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'encuesta.opcion': {
            'Meta': {'object_name': 'Opcion'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'opcion': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'pregunta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['encuesta.Pregunta']"})
        },
        u'encuesta.pregunta': {
            'Meta': {'object_name': 'Pregunta'},
            'encuesta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['encuesta.Encuesta']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'opcional': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pregunta': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'encuesta.respuesta': {
            'Meta': {'object_name': 'Respuesta'},
            'encuesta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['encuesta.Encuesta']"}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'opcion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['encuesta.Opcion']"}),
            'pregunta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['encuesta.Pregunta']"}),
            'usuario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Usuario']"})
        },
        u'encuesta.respuestav': {
            'Meta': {'object_name': 'RespuestaV'},
            'encuesta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['encuesta.Encuesta']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pregunta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['encuesta.Pregunta']"}),
            'respuesta': ('django.db.models.fields.TextField', [], {}),
            'usuario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Usuario']"})
        },
        u'usuarios.administrador': {
            'Meta': {'object_name': 'Administrador'},
            'apellidos': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'telefono': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['usuarios.Usuario']", 'unique': 'True'})
        },
        u'usuarios.usuario': {
            'Meta': {'object_name': 'Usuario'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'codigo_confirmacion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password_2': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ultimo_ingreso': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 2, 7, 0, 0)'})
        }
    }

    complete_apps = ['encuesta']