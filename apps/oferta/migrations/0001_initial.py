# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Oferta'
        db.create_table(u'oferta_oferta', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('desc', self.gf('django.db.models.fields.TextField')(max_length=255)),
            ('salario', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('perfil_profesional', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('horario', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('tipo_contrato', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('contacto', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('telefonofijo_contacto', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('celular_contacto', self.gf('django.db.models.fields.BigIntegerField')()),
            ('correo', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('estado_oferta', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('fecha_creacion', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, auto_now_add=True, blank=True)),
            ('uoferta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Usuario'])),
        ))
        db.send_create_signal(u'oferta', ['Oferta'])

        # Adding model 'Vacante'
        db.create_table(u'oferta_vacante', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('oferta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['oferta.Oferta'])),
            ('egresado_postulado', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usuarios.Egresado'])),
            ('hoja_vida', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('estado_vacante', self.gf('django.db.models.fields.CharField')(default='Sin respuesta', max_length=15)),
            ('fecha_creacion', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'oferta', ['Vacante'])


    def backwards(self, orm):
        # Deleting model 'Oferta'
        db.delete_table(u'oferta_oferta')

        # Deleting model 'Vacante'
        db.delete_table(u'oferta_vacante')


    models = {
        u'oferta.oferta': {
            'Meta': {'object_name': 'Oferta'},
            'celular_contacto': ('django.db.models.fields.BigIntegerField', [], {}),
            'contacto': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '255'}),
            'estado_oferta': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'horario': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'perfil_profesional': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'salario': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'telefonofijo_contacto': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tipo_contrato': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'uoferta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Usuario']"})
        },
        u'oferta.vacante': {
            'Meta': {'object_name': 'Vacante'},
            'egresado_postulado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            'estado_vacante': ('django.db.models.fields.CharField', [], {'default': "'Sin respuesta'", 'max_length': '15'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'hoja_vida': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'oferta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['oferta.Oferta']"})
        },
        u'usuarios.egresado': {
            'Meta': {'object_name': 'Egresado'},
            'cedula': ('django.db.models.fields.BigIntegerField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'estado': ('django.db.models.fields.CharField', [], {'default': "'vivo'", 'max_length': '10'}),
            'estado_laboral': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'fecha_cumple': ('django.db.models.fields.DateField', [], {}),
            'fecha_grado': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'p_apellido': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'p_nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Programa']"}),
            's_apellido': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            's_nombre': ('django.db.models.fields.CharField', [], {'max_length': '45', 'null': 'True', 'blank': 'True'}),
            'ultima_modificacion': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 6, 0, 0)'}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['usuarios.Usuario']", 'unique': 'True'})
        },
        u'usuarios.programa': {
            'Meta': {'object_name': 'Programa'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'usuarios.usuario': {
            'Meta': {'object_name': 'Usuario'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'password_2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ultimo_ingreso': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 6, 0, 0)'})
        }
    }

    complete_apps = ['oferta']