# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Oferta.nombre'
        db.alter_column(u'oferta_oferta', 'nombre', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Changing field 'Oferta.horario'
        db.alter_column(u'oferta_oferta', 'horario', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Changing field 'Oferta.telefonofijo_contacto'
        db.alter_column(u'oferta_oferta', 'telefonofijo_contacto', self.gf('django.db.models.fields.BigIntegerField')(null=True))

        # Changing field 'Oferta.contacto'
        db.alter_column(u'oferta_oferta', 'contacto', self.gf('django.db.models.fields.CharField')(max_length=45, null=True))

        # Changing field 'Oferta.desc'
        db.alter_column(u'oferta_oferta', 'desc', self.gf('django.db.models.fields.TextField')(max_length=500))

    def backwards(self, orm):

        # Changing field 'Oferta.nombre'
        db.alter_column(u'oferta_oferta', 'nombre', self.gf('django.db.models.fields.CharField')(max_length=45))

        # Changing field 'Oferta.horario'
        db.alter_column(u'oferta_oferta', 'horario', self.gf('django.db.models.fields.CharField')(max_length=45))

        # Changing field 'Oferta.telefonofijo_contacto'
        db.alter_column(u'oferta_oferta', 'telefonofijo_contacto', self.gf('django.db.models.fields.IntegerField')(null=True))

        # User chose to not deal with backwards NULL issues for 'Oferta.contacto'
        raise RuntimeError("Cannot reverse this migration. 'Oferta.contacto' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Oferta.contacto'
        db.alter_column(u'oferta_oferta', 'contacto', self.gf('django.db.models.fields.CharField')(max_length=45))

        # Changing field 'Oferta.desc'
        db.alter_column(u'oferta_oferta', 'desc', self.gf('django.db.models.fields.TextField')(max_length=255))

    models = {
        u'oferta.oferta': {
            'Meta': {'object_name': 'Oferta'},
            'celular_contacto': ('django.db.models.fields.BigIntegerField', [], {}),
            'contacto': ('django.db.models.fields.CharField', [], {'max_length': '45', 'null': 'True', 'blank': 'True'}),
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'estado_oferta': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'horario': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'perfil_profesional': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'salario': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'telefonofijo_contacto': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tipo_contrato': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'uoferta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Usuario']"})
        },
        u'oferta.vacante': {
            'Meta': {'object_name': 'Vacante'},
            'egresado_postulado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Egresado']"}),
            'estado_vacante': ('django.db.models.fields.CharField', [], {'default': "'Sin respuesta'", 'max_length': '15'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'hoja_vida': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'oferta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['oferta.Oferta']"})
        },
        u'usuarios.egresado': {
            'Meta': {'object_name': 'Egresado'},
            'carnet': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cedula': ('django.db.models.fields.BigIntegerField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'estado': ('django.db.models.fields.CharField', [], {'default': "'vivo'", 'max_length': '10'}),
            'estado_laboral': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'fecha_cumple': ('django.db.models.fields.DateField', [], {}),
            'fecha_grado': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'p_apellido': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'p_nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Programa']"}),
            's_apellido': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            's_nombre': ('django.db.models.fields.CharField', [], {'max_length': '45', 'null': 'True', 'blank': 'True'}),
            'ultima_modificacion': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 11, 5, 0, 0)'}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['usuarios.Usuario']", 'unique': 'True'})
        },
        u'usuarios.programa': {
            'Meta': {'object_name': 'Programa'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'usuarios.usuario': {
            'Meta': {'object_name': 'Usuario'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'codigo_confirmacion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password_2': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ultimo_ingreso': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 11, 5, 0, 0)'})
        }
    }

    complete_apps = ['oferta']