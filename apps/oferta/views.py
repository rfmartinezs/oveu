from django.shortcuts import render_to_response, render
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.contrib.auth.hashers import *
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
import datetime
from apps.usuarios.models import *
from apps.usuarios.decorators import *
from .forms import *
from .models import *
import json

@loginRequired
def oflaboral(request):
	x = request.session['id_coordinador']
	usuario = Administrador.objects.get(pk=x)	

	if request.method == 'POST':
		form = CrearOfertaForm(request.POST)

		#Validamos
		if form.is_valid():
			nombre = form.cleaned_data['nombre']
			desc = form.cleaned_data['desc']
			salario = form.cleaned_data['salario']
			perfil_profesional= form.cleaned_data['perfil_profesional']
			horario = form.cleaned_data['horario']
			tipo_contrato = form.cleaned_data['tipo_contrato']
			uoferta = usuario.usuario
			contacto = form.cleaned_data['contacto']
			telefonofijo_contacto= form.cleaned_data['telefonofijo_contacto']
			correo = form.cleaned_data['correo']
			celular_contacto = form.cleaned_data['celular_contacto']
			estado_oferta = True

			new_oferta = Oferta.objects.create(nombre=nombre, 
											desc=desc,
				    						salario=salario, 
				    						perfil_profesional=perfil_profesional, 
				    						horario=horario, 
				    						tipo_contrato=tipo_contrato, 
				    						contacto=contacto, 
				    						telefonofijo_contacto=telefonofijo_contacto, 
				    						celular_contacto=celular_contacto, 
				    						correo=correo, 
				    						estado_oferta=estado_oferta, 
				    						fecha_creacion=datetime.datetime.now(), 
				    						uoferta=uoferta)
			new_oferta.save()
			return HttpResponseRedirect(reverse('listaOfertaLaboral'))
	else:
		form = CrearOfertaForm()
	return render(request, 'oferta/addOfertaLab.html',{'form' :form})

@loginRequired
def editOferta(request, ofertas_id):
	e = get_object_or_404(Oferta, pk=ofertas_id)
	if request.method == 'POST':
		form = CrearOfertaForm(request.POST)
		#Validamos
		if form.is_valid():
			e.nombre = form.cleaned_data['nombre']
			e.desc = form.cleaned_data['desc']
			e.salario = form.cleaned_data['salario']
			e.perfil_profesional= form.cleaned_data['perfil_profesional']
			e.horario = form.cleaned_data['horario']
			e.tipo_contrato = form.cleaned_data['tipo_contrato']
			e.uoferta = e.uoferta
			e.contacto = form.cleaned_data['contacto']
			e.telefonofijo_contacto= form.cleaned_data['telefonofijo_contacto']
			e.correo = form.cleaned_data['correo']
			e.celular_contacto = form.cleaned_data['celular_contacto']
			e.estado_oferta = form.cleaned_data['estado_oferta']
			e.save()
			#form.save()
			return 	HttpResponseRedirect(reverse('listaOfertaLaboral'))

	if request.method == 'GET':
		form = CrearOfertaForm(initial={
									'nombre': e.nombre,
									'desc': e.desc,
									'salario': e.salario,
									'perfil_profesional':e.perfil_profesional,
									'horario': e.horario,
									'tipo_contrato': e.tipo_contrato,
									'uoferta': e.uoferta,
									'contacto': e.contacto,
									'telefonofijo_contacto': e.telefonofijo_contacto,
									'correo': e.correo,
									'celular_contacto': e.celular_contacto,
									'estado_oferta': e.estado_oferta,										
		})
	return render(request, 'oferta/addOfertaLab.html', {'form': form})

@loginRequired
def verOferta_dash(request, ofertas_id):
	ofertas = get_object_or_404(Oferta, pk=ofertas_id)
	return render_to_response("oferta/verOferta_dash.html", {"ofertas":ofertas}, RequestContext(request))

@loginRequired
def dash_postulados(request, ofertas_id):
	ofertas = get_object_or_404(Oferta, pk=ofertas_id)
	vacantes = Vacante.objects.all().filter(oferta=ofertas_id)
	x = ofertas.uoferta.id
	emp = get_object_or_404(Empresa, usuario=x)
	autor = emp.nombre
	return render_to_response("oferta/dash_postulados.html", locals(), RequestContext(request))

@loginRequired
def delOferta(request, ofertas_id):
	ofertas = get_object_or_404(Oferta,pk=ofertas_id)
	ofertas.delete()
	return HttpResponseRedirect(reverse('listaOfertaLaboral'))

@loginRequired
def listaOfertaLaboral(request):
	busqueda = request.POST.get('busqueda')
	query = request.POST.get('query')
	if request.method == 'POST':
		if query and busqueda:
			if busqueda == 'nombre':
				ofertas = Oferta.objects.filter(nombre__istartswith=query)
			elif busqueda == 'desc':
				ofertas = Oferta.objects.filter(desc__icontains=query)
			else:
				ofertas = Oferta.objects.all()
		else:
			ofertas = Oferta.objects.all()
	else:
		ofertas = Oferta.objects.all()
	return render(request, 'oferta/Lista_Oferta.html', {'ofertas':ofertas})