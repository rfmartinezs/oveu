#from django.conf.urls.defaults import *
from django.conf.urls import patterns, url
from apps.blog.views import *

urlpatterns = patterns('apps.oferta.views',
    url(r'^/addOfertaLaboral/$', 'oflaboral', name='oflaboral'), 
    url(r'^/listaOfertas/$', 'listaOfertaLaboral', name='listaOfertaLaboral'),
    url(r'^/listaOfertas/ver/(?P<ofertas_id>\d+)/$', 'verOferta_dash', name='verOferta_dash'),
    #rl(r'^dashboard/listaEmpresas/$', 'listaEmpresa', name='listaEmpresa'),
    #url(r'^dashboard/addEmpresa/$', 'addEmpresa', name='addEmpresa'),
    url(r'^/listaOfertas/ver/(?P<ofertas_id>\d+)/postulados$', 'dash_postulados', name='dash_postulados'),  
    url(r'^/editOferta/(?P<ofertas_id>\d+)/$', 'editOferta', name='editOferta'), 
    url(r'^/delOferta/(?P<ofertas_id>\d+)/$', 'delOferta', name='delOferta'), 
    #url(r'^empleo/$', 'empleo', name='empleo'),
)    