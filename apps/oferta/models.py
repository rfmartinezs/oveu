from django.db import models
from apps.usuarios.models import Usuario
from apps.usuarios.models import Egresado

PERFIL_CHOICES = (
    ('Profesional','Profesional'),
    ('Tecnologo','Tecnologo'),
    ('Tecnico','Tecnico')
)

CONTRATO_CHOICES = (
    ('Definido','Definido'),
    ('Indefinido','Indefinido'),
    ('En practicas','En practicas'),
    ('Formacion y aprendizaje','Formacion y aprendizaje')
)

VACANTE_CHOICES = (
    ('Aprobado','Aprobado'),
    ('Rechazado','Rechazado'),
    ('Sin respuesta','Sin respuesta')
)

class Oferta(models.Model):
    nombre = models.CharField(max_length = 255)
    desc = models.TextField(max_length = 500)
    salario = models.CharField(max_length = 45)
    perfil_profesional = models.CharField(max_length = 25, choices=PERFIL_CHOICES)
    horario = models.CharField(max_length = 255)
    tipo_contrato = models.CharField(max_length = 25, choices=CONTRATO_CHOICES)
    contacto = models.CharField(max_length = 45,blank=True, null=True)
    telefonofijo_contacto = models.BigIntegerField(blank=True, null=True)
    celular_contacto = models.BigIntegerField()
    correo = models.EmailField()
    estado_oferta = models.BooleanField(default=True) 
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    uoferta = models.ForeignKey(Usuario)

    def __unicode__(self):
        return self.nombre

class Vacante(models.Model):
    oferta = models.ForeignKey(Oferta)
    egresado_postulado = models.ForeignKey(Egresado)
    hoja_vida = models.FileField(upload_to = 'hoja_de_vida', blank=False, null=False)
    estado_vacante = models.CharField(max_length = 15, choices=VACANTE_CHOICES, default='Sin respuesta') 
    fecha_creacion = models.DateTimeField(auto_now_add=True)
