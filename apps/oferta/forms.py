# -*- encoding: utf-8 -*-
from django.forms import ModelForm
from .models import *
from django import forms
from django.template.defaultfilters import filesizeformat
from django.utils.translation import ugettext_lazy as _
import json
from django.conf import settings

class CrearOfertaForm(forms.Form):
    nombre 					= forms.CharField(max_length=45, label='Título para oferta laboral', widget=forms.TextInput(attrs={'placeholder': 'Nombre de oferta'}))
    desc                    = forms.CharField(widget=forms.Textarea, max_length=1000, label='Descripción')
    salario 				= forms.CharField(max_length=45, label='Salario', widget=forms.TextInput(attrs={'placeholder': 'Escribe el salario'}))
    perfil_profesional		= forms.CharField(max_length=45, label='Tipo de perfil', widget=forms.Select(choices=PERFIL_CHOICES))
    horario 				= forms.CharField(max_length=45, label='Horario', widget=forms.TextInput(attrs={'placeholder': 'Escriba un horario'}))
    tipo_contrato 			= forms.CharField(max_length=45, label='Tipo de contrato', widget=forms.Select(choices=CONTRATO_CHOICES))
    contacto                = forms.CharField(max_length=45, label='Contacto', widget=forms.TextInput(attrs={'placeholder': 'Nombre de contacto'}))
    telefonofijo_contacto   = forms.IntegerField(label='Telefono Fijo', required=False,widget=forms.TextInput(attrs={'placeholder': 'Numero fijo'})) 
    correo                  = forms.EmailField(label='Correo Electronico', widget=forms.TextInput(attrs={'placeholder': 'ejemplo@correo.com'}))
    celular_contacto        = forms.IntegerField(label='Numero de Celular',widget=forms.TextInput(attrs={'placeholder': 'Numero de celular'})) 
    estado_oferta           = forms.BooleanField(label='Publicado?', required=False)

class CrearOfertaFormb(forms.Form):
    nombre                  = forms.CharField(max_length=45, label='Título de oferta laboral', widget=forms.TextInput(attrs={'placeholder': 'Nombre de oferta'}))
    desc                    = forms.CharField(widget=forms.Textarea, max_length=1000, label='Descripción')
    salario                 = forms.CharField(max_length=45, label='Salario', widget=forms.TextInput(attrs={'placeholder': 'Escribe el salario'}))
    perfil_profesional      = forms.CharField(max_length=45, label='Tipo de perfil', widget=forms.Select(choices=PERFIL_CHOICES))
    horario                 = forms.CharField(max_length=45, label='Horario',widget=forms.TextInput(attrs={'placeholder': 'Escriba un horario'}))
    tipo_contrato           = forms.CharField(max_length=45, label='Tipo de contrato', widget=forms.Select(choices=CONTRATO_CHOICES))
    contacto                = forms.CharField(max_length=45, label='Nombre de contacto')
    telefonofijo_contacto   = forms.IntegerField(label='Telefono Fijo', required=False,widget=forms.TextInput(attrs={'placeholder': 'Numero fijo'})) 
    correo                  = forms.EmailField(label='Correo Electronico', widget=forms.TextInput(attrs={'placeholder': 'ejemplo@correo.com'}))
    celular_contacto        = forms.IntegerField(label='Telefono Celular',widget=forms.TextInput(attrs={'placeholder': 'Numero de celular'})) 
 
class VacanteForm(ModelForm):

    class Meta:
        model = Vacante
        fields = ['hoja_vida',]
        labels = {
            'hoja_vida': ('Adjunta tu hoja de vida'),
        }

    def clean_hoja_vida(self):
        hoja_vida = self.cleaned_data['hoja_vida']
        if hoja_vida != None:
            content_type = hoja_vida.content_type.split('/')[1]
            print content_type
            if content_type in settings.CONTENT_TYPES:
                if hoja_vida._size > int(settings.MAX_UPLOAD_SIZE):
                    raise forms.ValidationError(_(u'Por favor, mantenga tamaño de archivo bajo %s. Tamaño de archivo %s') % (filesizeformat(settings.MAX_UPLOAD_SIZE), filesizeformat(hoja_vida._size)))
            else:
                raise forms.ValidationError(_(u'Solo se permiten archivos PDF, DOC, DOCX o ODF. '))
            return hoja_vida
        
        

class Aprobar_rechazarForm(ModelForm):
    class Meta:
        model = Vacante
        fields = ['estado_vacante',]
        labels = {
            'estado_vacante': (''),
        }
