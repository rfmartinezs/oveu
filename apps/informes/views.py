from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic.base import View
from wkhtmltopdf.views import PDFTemplateView
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from cities_light.models import City, Country
from apps.usuarios.models import Egresado, DireccionActual, EstudiosRealizados, Idioma, Asociacion, Premio, Programa
import datetime
from wkhtmltopdf.views import PDFTemplateResponse
from django.db.models import Q
from django.http import Http404
#import requests


class HomePageView(TemplateView):
    template_name = "informes/pdfc.html"

def PrimerInformePdf(request):
	c = DireccionActual.objects.values_list('ciudad', flat=True).distinct()
	data = []
	nombre_pdf = 'PrimerInformePdf-%s.pdf' % (datetime.datetime.now())
	date_generated = datetime.datetime.now()
	for da in c:
		#d contiene la cantidad de inserciones que tienen la ciudad da[]
		
		city = City.objects.get(pk=da)
		
		cantidad = DireccionActual.objects.filter(ciudad=da).count()
		nombre = city.name.encode('utf8')
		fila = [nombre, cantidad]
		data.append(fila)

	return PDFTemplateResponse(request=request,
									template="informes/1pdf.html",
									context={"data": data, "tiempo":date_generated},
									filename=nombre_pdf,
									header_template="informes/header.html",
									footer_template="informes/footer.html",
									show_content_in_browser=True,
									cmd_options = {'margin-top':25,'margin-bottom':20}
									)

def SegundoInformePdf(request):
	nombre_pdf = 'SegundoInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	si = 0
	no = 0
	for e in egresados:
		estudio = EstudiosRealizados.objects.filter(egresado=e).filter(tipo="especializacion").first()
		if estudio:
			si+=1
		else:
			no+=1
	sil = ["Si", si ]
	nol = ["No", no ]
	data.append(sil)
	data.append(nol)

	return PDFTemplateResponse(request=request,
									template="informes/2pdf.html",
									context={"data": data, "tiempo":date_generated},
									filename=nombre_pdf,
									header_template="informes/header.html",
									footer_template="informes/footer.html",
									show_content_in_browser=True,
									cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
									)
def TerceroInformePdf(request):
	nombre_pdf = 'TerceroInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	si = egresados.filter(estado_laboral='laborando en su profesion').count()
	no = egresados.count() - si

	print si
	print no
	sil = ["Si", si ]
	nol = ["No", no ]
	data.append(sil)
	data.append(nol)

	return PDFTemplateResponse(request=request,
									template="informes/3pdf.html",
									context={"data": data, "tiempo":date_generated},
									filename=nombre_pdf,
									header_template="informes/header.html",
									footer_template="informes/footer.html",
									show_content_in_browser=True,
									cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
									)
def CuartoInformePdf(request):
	nombre_pdf = 'CuartoInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	si = egresados.filter(estado_laboral='laborando en su profesion').count()
	no = egresados.count() - si

	print si
	print no
	sil = ["Si", si ]
	nol = ["No", no ]
	data.append(sil)
	data.append(nol)

	return PDFTemplateResponse(request=request,
									template="informes/4pdf.html",
									context={"data": data, "tiempo":date_generated},
									filename=nombre_pdf,
									header_template="informes/header.html",
									footer_template="informes/footer.html",
									show_content_in_browser=True,
									cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
									)


def QuintoInformePdf(request):
	nombre_pdf = 'QuintoInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	si = 0
	no = 0
	for e in egresados:
		if e.estado == "fallecido":
			si+=1
		else:
			no+=1

	sil = ["Fallecidos", si ]
	nol = ["No", no ]
	data.append(sil)
	data.append(nol)
	return PDFTemplateResponse(request=request,
									template="informes/5pdf.html",
									context={"data": data, "tiempo":date_generated},
									filename=nombre_pdf,
									header_template="informes/header.html",
									footer_template="informes/footer.html",
									show_content_in_browser=True,
									cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
									)

def SextoInformePdf(request):
	nombre_pdf = 'SextoInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	si = 0
	no = 0
	for e in egresados:
		estudio = EstudiosRealizados.objects.filter(egresado=e).filter(tipo="magister").first()
		if estudio:
			si+=1
		else:
			no+=1
	sil = ["Si", si ]
	nol = ["No", no ]
	data.append(sil)
	data.append(nol)
	return PDFTemplateResponse(request=request,
									template="informes/6pdf.html",
									context={"data": data, "tiempo":date_generated},
									filename=nombre_pdf,
									header_template="informes/header.html",
									footer_template="informes/footer.html",
									show_content_in_browser=True,
									cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
									)

def SeptimoInformePdf(request):
	nombre_pdf = 'SeptimoInformePdf-%s.pdf' % (datetime.datetime.now())
	escuelas = Programa.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	for e in escuelas:
		egresados = Egresado.objects.filter(programa=e)
		cantidadEgresado = egresados.count()
		desactualizados = egresados.filter(ultima_modificacion__lte=datetime.datetime.now() - datetime.timedelta(days=180)).exclude(estado='fallecido').count()
		actualizados = cantidadEgresado - desactualizados
		lista = [e.nombre,actualizados, desactualizados]
		data.append(lista)
	return PDFTemplateResponse(request=request,
									template="informes/7pdf.html",
									context={"data": data, "tiempo":date_generated},
									filename=nombre_pdf,
									header_template="informes/header.html",
									footer_template="informes/footer.html",
									show_content_in_browser=True,
									cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
									)

def OctavoInformePdf(request):
	nombre_pdf = 'OctavoInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	si = egresados.filter(Q(estado_laboral='laborando en su profesion') | Q(estado_laboral='Laborando fuera de su profesion') | Q(estado_laboral='Laborando y estudiando')).count()
	print si
	no = egresados.count() - si
	sil = ["Si", si ]
	nol = ["No", no ]
	data.append(sil)
	data.append(nol)
	return PDFTemplateResponse(request=request,
									template="informes/8pdf.html",
									context={"data": data, "tiempo":date_generated},
									filename=nombre_pdf,
									header_template="informes/header.html",
									footer_template="informes/footer.html",
									show_content_in_browser=True,
									cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
									)

def NovenoInformePdf(request):
	nombre_pdf = 'NovenoInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	si = egresados.filter(estado_laboral='Desempleado').count()
	no = egresados.count() - si
	sil = ["Si", si ]
	nol = ["No", no ]
	data.append(sil)
	data.append(nol)
	return PDFTemplateResponse(request=request,
									template="informes/9pdf.html",
									context={"data": data, "tiempo":date_generated},
									filename=nombre_pdf,
									header_template="informes/header.html",
									footer_template="informes/footer.html",
									show_content_in_browser=True,
									cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
									)


def DecimoInformePdf(request):
	nombre_pdf = 'DecimoInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	si = Egresado.objects.filter(Q(estado_laboral='Laborando y estudiando') | Q(estado_laboral='Estudiando')).count()
	print si
	no = egresados.count() - si
	sil = ["Si", si ]
	nol = ["No", no ]
	data.append(sil)
	data.append(nol)
	return PDFTemplateResponse(request=request,
								template="informes/10pdf.html",
								context={"data": data, "tiempo":date_generated},
								filename=nombre_pdf,
								show_content_in_browser=True, 
								header_template='informes/header.html',
								footer_template="informes/footer.html",
								cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
								)

def UnDecimoInformePdf(request):
	
	c = DireccionActual.objects.all().exclude(ciudad__country__pk=49)
	data = []
	nombre_pdf = 'UnDecimoInformePdf-%s.pdf' % (datetime.datetime.now())
	date_generated = datetime.datetime.now()	
	data = []
	
	for da in c:
		#d contiene la cantidad de inserciones que tienen la ciudad da[]
		
		country = Country.objects.get(pk=da.ciudad.country.pk)
			
		cantidad = DireccionActual.objects.filter(ciudad__country=country).count()
		nombre = country.name.encode('utf8')
		fila = [nombre, cantidad]
		if not fila in data:
			data.append(fila)

	return PDFTemplateResponse(request=request,
								template="informes/11pdf.html",
								context={"data": data, "tiempo": date_generated},
								filename=nombre_pdf,
								show_content_in_browser=True, 
								header_template='informes/header.html',
								footer_template="informes/footer.html",
								cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
								)

def DoceInformePdf(request):
	nombre_pdf = 'DoceInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	si = 0
	no = 0
	for e in egresados:
		estudio = EstudiosRealizados.objects.filter(egresado=e).filter(tipo="diplomado").first()
		if estudio:
			si+=1
		else:
			no+=1
	sil = ["Si", si ]
	nol = ["No", no ]
	data.append(sil)
	data.append(nol)
	return PDFTemplateResponse(request=request,
								template="informes/12pdf.html",
								context={"data": data, "tiempo":date_generated},
								filename=nombre_pdf,
								show_content_in_browser=True, 
								header_template='informes/header.html',
								footer_template="informes/footer.html",
								cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
								)


def TreceInformePdf(request):
	nombre_pdf = 'TreceInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	si = 0
	no = 0
	for e in egresados:
		estudio = EstudiosRealizados.objects.filter(egresado=e).filter(tipo="doctorado").first()
		if estudio:
			si+=1
		else:
			no+=1
	sil = ["Si", si ]
	nol = ["No", no ]
	data.append(sil)
	data.append(nol)
	return PDFTemplateResponse(request=request,
								template="informes/13pdf.html",
								context={"data": data, "tiempo":date_generated},
								filename=nombre_pdf,
								show_content_in_browser=True, 
								header_template='informes/header.html',
								footer_template="informes/footer.html",
								cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
								)



def CatorceInformePdf(request):
	nombre_pdf = 'CatorceInformePdf-%s.pdf' % (datetime.datetime.now())
	data = []
	programa = Programa.objects.all()
	date_generated = datetime.datetime.now()
	for p in programa:
		cantidad = Egresado.objects.filter(estado="vivo").filter(programa=p).count()
		nombre = p.nombre.encode('utf8')
		fila = [nombre, cantidad]
		data.append(fila)
	return PDFTemplateResponse(request=request,
									template="informes/14pdf.html",
									context={"data": data, "tiempo":date_generated},
									filename=nombre_pdf,
									header_template="informes/header.html",
									footer_template="informes/footer.html",
									show_content_in_browser=True,
									cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
									)

def QuinceInformePdf(request):
	nombre_pdf = 'QuinceInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	si = 0
	no = 0
	for e in egresados:
		premio = Premio.objects.filter(egresado=e).first()
		if premio:
			si+=1
		else:
			no+=1
	sil = ["Si", si ]
	nol = ["No", no ]
	data.append(sil)
	data.append(nol)
	return PDFTemplateResponse(request=request,
									template="informes/15pdf.html",
									context={"data": data, "tiempo":date_generated},
									filename=nombre_pdf,
									header_template="informes/header.html",
									footer_template="informes/footer.html",
									show_content_in_browser=True,
									cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
									)

def DieciseisInformePdf(request):
	nombre_pdf = 'DieciseisInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	si = 0
	no = 0
	for e in egresados:
		asociacion = Asociacion.objects.filter(egresado=e).first()
		if asociacion:
			si+=1
		else:
			no+=1
	sil = ["Si", si ]
	nol = ["No", no ]
	data.append(sil)
	data.append(nol)
	return PDFTemplateResponse(request=request,
									template="informes/16pdf.html",
									context={"data": data, "tiempo":date_generated},
									filename=nombre_pdf,
									header_template="informes/header.html",
									footer_template="informes/footer.html",
									show_content_in_browser=True,
									cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
									)

def DiecisieteInformePdf(request):
	nombre_pdf = 'DiecisieteInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	si = 0
	no = 0
	for e in egresados:
		idioma = Idioma.objects.filter(egresado=e).first()
		if idioma:
			si+=1
		else:
			no+=1
	sil = ["Si", si ]
	nol = ["No", no ]
	data.append(sil)
	data.append(nol)
	return PDFTemplateResponse(request=request,
									template="informes/17pdf.html",
									context={"data": data, "tiempo":date_generated},
									filename=nombre_pdf,
									header_template="informes/header.html",
									footer_template="informes/footer.html",
									show_content_in_browser=True,
									cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
									)
def DieciochoInformePdf(request):
	nombre_pdf = 'DiecisieteInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all().count()
	date_generated = datetime.datetime.now()
	desactualizados = Egresado.objects.filter(ultima_modificacion__lte=datetime.datetime.now() - datetime.timedelta(days=180)).exclude(estado='fallecido').count()
	actualizados = egresados - desactualizados
	data = []
	sil = ["Desactualizados", desactualizados ]
	nol = ["Actualizados", actualizados ]
	data.append(sil)
	data.append(nol)
	return PDFTemplateResponse(request=request,
									template="informes/18pdf.html",
									context={"data": data, "tiempo":date_generated},
									filename=nombre_pdf,
									header_template="informes/header.html",
									footer_template="informes/footer.html",
									show_content_in_browser=True,
									cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
									)
def DiecinueveInformePdf(request):
	nombre_pdf = 'DiecinueveInformePdf-%s.pdf' % (datetime.datetime.now())
	egresados = Egresado.objects.all()
	date_generated = datetime.datetime.now()
	data = []
	si = 0
	no = 0
	for e in egresados:
		estudio = EstudiosRealizados.objects.filter(egresado=e).filter(tipo="subespecializacion").first()
		if estudio:
			si+=1
		else:
			no+=1
	sil = ["Si", si ]
	nol = ["No", no ]
	data.append(sil)
	data.append(nol)
	
	return PDFTemplateResponse(request=request,
								template="informes/19pdf.html",
								context={"data": data, "tiempo":date_generated},
								filename=nombre_pdf,
								show_content_in_browser=True, 
								header_template='informes/header.html',
								footer_template="informes/footer.html",
								cmd_options = {
										'margin-top': 25,
										'margin-bottom':20
									}
								)


class MyPDF(PDFTemplateView):
	filename = 'my_pdf.pdf'
	template_name = "informes/index.html"
	show_content_in_browser=True
	cmd_options = {
	    'margin-top': 3,
	}


class Graficos(View):
	"""docstring for IndexInformeView"""
	#template_name = "informes/index.html"
	def post(self, request, *args, **kwargs):
		#programa = Programa.objects.get(pk=1)
		#Egresados = Egresado.objects.filter(programa=programa).count()
		#return render(request, self.template_name, {'e': Egresados, 'p':programa })
		numero = request.POST['grafico']
		return HttpResponseRedirect(reverse('informes:graficoDetalle', args=(numero,)))

class GraficoDetalle(View):
	"""docstring for IndexInformeView"""
	#template_name = "informes/index.html"
	def get(self, request, *args, **kwargs):
		
		numero=self.kwargs['numero']
		
		if numero == "1":
			c = DireccionActual.objects.values_list('ciudad', flat=True).distinct()
			print c
			data = []
			
			for da in c:
				#d contiene la cantidad de inserciones que tienen la ciudad da[]
				
				city = City.objects.get(pk=da)
				cantidad = DireccionActual.objects.filter(ciudad=da).count()
				nombre = city.name.encode('utf8')
				fila = [nombre, cantidad]
				data.append(fila)
			print data
			
			return render(request, 'informes/1.html', {'data': data })
		elif numero == "2":
			egresados = Egresado.objects.all()
			cant_egresados = egresados.count()
			data = []
			si = 0
			no = 0
			for e in egresados:
				estudio = EstudiosRealizados.objects.filter(egresado=e).filter(tipo="especializacion").first()
				if estudio:
					si+=1
				else:
					no+=1
			sil = ["Si", si ]
			nol = ["No", no ]
			data.append(sil)
			data.append(nol)
			print data
			return render(request, 'informes/2.html', {'data': data, 'cant_egresados':cant_egresados })

		elif numero == "3":
			egresados = Egresado.objects.all()
			data = []
			si = egresados.filter(estado_laboral='laborando en su profesion').count()
			no = egresados.count() - si

			print si
			print no
			sil = ["Si", si ]
			nol = ["No", no ]
			data.append(sil)
			data.append(nol)
			return render(request, 'informes/3.html', {'data': data })

		elif numero == "4":
			egresados = Egresado.objects.all()
			data = []
			si = egresados.filter(estado_laboral='Laborando fuera de su profesion').count()
			no = egresados.count() - si

			print si
			print no
			sil = ["Si", si ]
			nol = ["No", no ]
			data.append(sil)
			data.append(nol)
			return render(request, 'informes/4.html', {'data': data })

		elif numero == "5":
			egresados = Egresado.objects.all()
			data = []
			si = 0
			no = 0
			for e in egresados:
				if e.estado == "fallecido":
					si+=1
				else:
					no+=1

			sil = ["Fallecidos", si ]
			nol = ["No", no ]
			data.append(sil)
			data.append(nol)
			return render(request, 'informes/5.html', {'data': data })

		elif numero == "6":
			egresados = Egresado.objects.all()
			data = []
			si = 0
			no = 0
			for e in egresados:
				estudio = EstudiosRealizados.objects.filter(egresado=e).filter(tipo="magister").first()
				if estudio:
					si+=1
				else:
					no+=1
			sil = ["Si", si ]
			nol = ["No", no ]
			data.append(sil)
			data.append(nol)
			return render(request, 'informes/6.html', {'data': data })

		elif numero == "7":
			#TODO
			escuelas = Programa.objects.all()
			data = []
			for e in escuelas:
				egresados = Egresado.objects.filter(programa=e)
				cantidadEgresado = egresados.count()
				desactualizados = egresados.filter(ultima_modificacion__lte=datetime.datetime.now() - datetime.timedelta(days=180)).exclude(estado='fallecido').count()
				actualizados = cantidadEgresado - desactualizados
				lista = [e.nombre,actualizados, desactualizados]
				data.append(lista)
			return render(request, 'informes/7.html', {'data': data })

		elif numero == "8":
			#TODO
			egresados = Egresado.objects.all()
			data = []
			si = egresados.filter(Q(estado_laboral='laborando en su profesion') | Q(estado_laboral='Laborando fuera de su profesion') | Q(estado_laboral='Laborando y estudiando')).count()
			print si
			no = egresados.count() - si
			sil = ["Si", si ]
			nol = ["No", no ]
			data.append(sil)
			data.append(nol)
			return render(request, 'informes/8.html', {'data': data })

		elif numero == "9":
			egresados = Egresado.objects.all()
			data = []
			si = egresados.filter(estado_laboral='Desempleado').count()
			no = egresados.count() - si

			print si
			print no
			sil = ["Si", si ]
			nol = ["No", no ]
			data.append(sil)
			data.append(nol)
			return render(request, 'informes/9.html', {'data': data })

		elif numero == "10":
			egresados = Egresado.objects.all()
			data = []
			si = egresados.filter(Q(estado_laboral='Laborando y estudiando') | Q(estado_laboral='Estudiando')).count()
			no = egresados.count() - si
			sil = ["Si", si ]
			nol = ["No", no ]
			data.append(sil)
			data.append(nol)
			return render(request, 'informes/10.html', {'data': data })

		if numero == "11":
			c = DireccionActual.objects.all().exclude(ciudad__country__pk=49)
			print c
			data = []
			
			for da in c:
				#d contiene la cantidad de inserciones que tienen la ciudad da[]
				
				country = Country.objects.get(pk=da.ciudad.country.pk)
				print country
				
				cantidad = DireccionActual.objects.filter(ciudad__country=country).count()
				nombre = country.name.encode('utf8')
				fila = [nombre, cantidad]
				if not fila in data:
					data.append(fila)
			print data
			
			return render(request, 'informes/11.html', {'data': data })

		elif numero == "12":
			egresados = Egresado.objects.all()
			data = []
			si = 0
			no = 0
			for e in egresados:
				estudio = EstudiosRealizados.objects.filter(egresado=e).filter(tipo="diplomado").first()
				if estudio:
					si+=1
				else:
					no+=1
			sil = ["Si", si ]
			nol = ["No", no ]
			data.append(sil)
			data.append(nol)
			return render(request, 'informes/12.html', {'data': data })

		elif numero == "13":
			egresados = Egresado.objects.all()
			data = []
			si = 0
			no = 0
			for e in egresados:
				estudio = EstudiosRealizados.objects.filter(egresado=e).filter(tipo="doctorado").first()
				if estudio:
					si+=1
				else:
					no+=1
			sil = ["Si", si ]
			nol = ["No", no ]
			data.append(sil)
			data.append(nol)
			return render(request, 'informes/13.html', {'data': data })



		elif numero == "14":
			data = []
			programa = Programa.objects.all()
			for p in programa:
				cantidad = Egresado.objects.filter(estado="vivo").filter(programa=p).count()
				nombre = p.nombre.encode('utf8')
				fila = [nombre, cantidad]
				data.append(fila)

			return render(request, 'informes/14.html', {'data': data })

		elif numero == "15":
			egresados = Egresado.objects.all()
			data = []
			si = 0
			no = 0
			for e in egresados:
				premio = Premio.objects.filter(egresado=e).first()
				if premio:
					si+=1
				else:
					no+=1
			sil = ["Si", si ]
			nol = ["No", no ]
			data.append(sil)
			data.append(nol)
			return render(request, 'informes/15.html', {'data': data })

		elif numero == "16":
			egresados = Egresado.objects.all()
			data = []
			si = 0
			no = 0
			for e in egresados:
				asociacion = Asociacion.objects.filter(egresado=e).first()
				if asociacion:
					si+=1
				else:
					no+=1
			sil = ["Si", si ]
			nol = ["No", no ]
			data.append(sil)
			data.append(nol)
			return render(request, 'informes/16.html', {'data': data })

		elif numero == "17":
			egresados = Egresado.objects.all()
			data = []
			si = 0
			no = 0
			for e in egresados:
				idioma = Idioma.objects.filter(egresado=e).first()
				if idioma:
					si+=1
				else:
					no+=1
			sil = ["Si", si ]
			nol = ["No", no ]
			data.append(sil)
			data.append(nol)
			return render(request, 'informes/17.html', {'data': data })

		elif numero == "18":
			egresados = Egresado.objects.all().count()
			desactualizados = Egresado.objects.filter(ultima_modificacion__lte=datetime.datetime.now() - datetime.timedelta(days=180)).exclude(estado='fallecido').count()
			actualizados = egresados - desactualizados
			data = []
			sil = ["Desactualizados", desactualizados ]
			nol = ["Actualizados", actualizados ]
			data.append(sil)
			data.append(nol)
			return render(request, 'informes/18.html', {'data': data })

		elif numero == "19":
			egresados = Egresado.objects.all()
			data = []
			si = 0
			no = 0
			for e in egresados:
				estudio = EstudiosRealizados.objects.filter(egresado=e).filter(tipo="supespecializacion").first()
				if estudio:
					si+=1
				else:
					no+=1
			sil = ["Si", si ]
			nol = ["No", no ]
			data.append(sil)
			data.append(nol)
			return render(request, 'informes/19.html', {'data': data })

		else:
			raise Http404("No existe!")

class IndexInformeView(View):
	"""docstring for IndexInformeView"""
	template_name = "informes/index.html"
	def get(self, request, *args, **kwargs):
		programa = Programa.objects.get(pk=1)
		Egresados = Egresado.objects.filter(programa=programa).count()
		return render(request, self.template_name, {'e': Egresados, 'p':programa })
