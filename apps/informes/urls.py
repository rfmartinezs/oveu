from django.conf.urls import patterns, url
from apps.informes.views import *
from wkhtmltopdf.views import PDFTemplateView



urlpatterns = patterns('apps.informes.views',
    # Examples:
    url(r'^/$', IndexInformeView.as_view(), name='indexInformes'),
    #url(r'^/pdf/$', MyPDF.as_view(), name='pdf'),
    url(r'^/graficos/$', Graficos.as_view(), name='graficos'),
    url(r'^/chrome/$', HomePageView.as_view(), name='cr'),
    url(r'^/grafico/(?P<numero>\d+)/$', GraficoDetalle.as_view(), name='graficoDetalle'),
    url(r'^/graficos/primergraficopdf/$', 'PrimerInformePdf', name='primerGrafico'),
    url(r'^/graficos/segundograficopdf/$', 'SegundoInformePdf', name='segundoGrafico'),
    url(r'^/graficos/tercerograficopdf/$', 'TerceroInformePdf', name='terceroGrafico'),
    url(r'^/graficos/cuartograficopdf/$', 'CuartoInformePdf', name='cuartoGrafico'),
    url(r'^/graficos/quintograficopdf/$', 'QuintoInformePdf', name='quintoGrafico'),
    url(r'^/graficos/sextograficopdf/$', 'SextoInformePdf', name='sextoGrafico'),
    url(r'^/graficos/septimograficopdf/$', 'SeptimoInformePdf', name='septimoGrafico'),
    url(r'^/graficos/octavograficopdf/$', 'OctavoInformePdf', name='octavoGrafico'),
    url(r'^/graficos/novenograficopdf/$', 'NovenoInformePdf', name='novenoGrafico'),
    url(r'^/graficos/decimograficopdf/$', 'DecimoInformePdf', name='decimoGrafico'),
    url(r'^/graficos/Undecimograficopdf/$', 'UnDecimoInformePdf', name='undecimoGrafico'),
    url(r'^/graficos/docegraficopdf/$', 'DoceInformePdf', name='doceGrafico'),
    url(r'^/graficos/trecegraficopdf/$', 'TreceInformePdf', name='treceGrafico'),
    url(r'^/graficos/catorcegraficopdf/$', 'CatorceInformePdf', name='catorceGrafico'),
    url(r'^/graficos/quincegraficopdf/$', 'QuinceInformePdf', name='quinceGrafico'),
    url(r'^/graficos/dieciseisgraficopdf/$', 'DieciseisInformePdf', name='dieciseisGrafico'),
    url(r'^/graficos/diecisietegraficopdf/$', 'DiecisieteInformePdf', name='diecisieteGrafico'),
    url(r'^/graficos/dieciochograficopdf/$', 'DieciochoInformePdf', name='dieciochoGrafico'),
    url(r'^/graficos/diecinuevegraficopdf/$', 'DiecinueveInformePdf', name='diecinueveGrafico'),
    #url(r'^encuesta/crear$', EncuestaCreateView.as_view(), name='crearEncuesta'),
    # url(r'^blog/', include('blog.urls')),


)
