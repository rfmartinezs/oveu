# -*- encoding: utf-8 -*-
from django.shortcuts import render_to_response, render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect, HttpResponse, HttpResponsePermanentRedirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic.edit import UpdateView
from django.views.generic import ListView, CreateView
from django.core.exceptions import ObjectDoesNotExist
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import *
from django.core.mail import send_mail
from .models import *
import hashlib, datetime, random
from apps.blog.decorators import *
from .tasks import RecoveryUserTask, CreateEmpresaTask
from apps.oferta.models import *
from apps.oferta.forms import *
from apps.encuesta.models import *
from apps.eventos.models import *
from apps.usuarios.models import *
from apps.usuarios.forms import *
from apps.usuarios.decorators import *
from .forms import *
from apps.blog.forms import *
import json
from django.template import RequestContext
from django.shortcuts import get_object_or_404

def casa(request):
	cat = Categoria.objects.all()
	return render_to_response('baseblog.html', {"cat":cat}, RequestContext(request))

@loginRequired_empresa
def elogout(request):
	try:
		if 'id_user' in request.session:
			del request.session['id_user']
		if 'id_empresa' in request.session:
			del request.session['id_empresa']
		if 'tipo_usuario' in request.session:
			del request.session['tipo_usuario']
		if 'nombre_u' in request.session:
			del request.session['nombre_u']
	except KeyError:
		pass
	return HttpResponseRedirect(reverse('index'))

@loginRequired_egresado
def blogout(request):
	try:
		if 'id_user' in request.session:
			del request.session['id_user']
		if 'id_egresado' in request.session:
			del request.session['id_egresado']
		if 'tipo_usuario' in request.session:
			del request.session['tipo_usuario']
		if 'urlimagen_egresado' in request.session:
			del request.session['	_egresado']
		if 'nombre_u' in request.session:
			del request.session['nombre_u']
	except KeyError:
		pass
	return HttpResponseRedirect(reverse('index'))

def index(request):
	if request.method == 'POST':
		form = InicioSesion2Form(request.POST)
		if form.is_valid():
			codigo = request.POST['codigo']
			clave = request.POST['clave']
			tipo_u = request.POST['tipo_u']
			try:
				u = Usuario.objects.get(codigo=codigo)
				
				if tipo_u == 'Egresado':
					egresado = Egresado.objects.get(usuario=u)
					if check_password(clave, u.password):
						#print u.estado
						if u.estado == True:
							request.session['id_egresado']=egresado.id
							request.session['id_user']=u.id	
							request.session['tipo_usuario']="egresado"
							request.session['nombre_u']=egresado.p_nombre
							if egresado.imagen:
								request.session['urlimagen_egresado']=egresado.imagen.url
							else:
								request.session['urlimagen_egresado']='http://placehold.it/50x50'
							return HttpResponseRedirect(reverse('egresados'))
						else:
							form = InicioSesion2Form()
							info = "Tu cuenta no ha sido verificada, revisa tu correo electronico"
							return render_to_response('index.html',locals(), context_instance=RequestContext(request))
					else:
						#return HttpResponse('clave incorrecta')
						form = InicioSesion2Form()
						info = "Contraseña no válida"	
						return render_to_response('index.html',locals(), context_instance=RequestContext(request))
				elif tipo_u == 'Empresa':
					empresa = Empresa.objects.get(usuario=u)

					if check_password(clave, u.password):
						if u.estado == True:
							request.session['id_empresa']=empresa.id
							request.session['id_user']=u.id
							request.session['tipo_usuario']="empresa"	
							request.session['nombre_u']=empresa.nombre
							return HttpResponseRedirect(reverse('empresa'))
						else:
							form = InicioSesion2Form()
							info = "Tu cuenta no ha sido verificada, resiva tu correo electronico"	
							return render_to_response('index.html',locals(), context_instance=RequestContext(request))

					else:
						#return HttpResponse('clave incorrecta')
						form = InicioSesion2Form()
						info = "Contraseña no válida"	
						return render_to_response('index.html',locals(), context_instance=RequestContext(request))
				
				elif tipo_u == 'Coordinador':
					try:
						c = Administrador.objects.get(usuario=u)
						if check_password(clave, u.password):
							request.session['id_usuario']=u.id
							request.session['nombre_usuario']=c.nombre
							request.session['id_coordinador']=c.id
							if c.foto:
								request.session['urlimagen']=c.get_absolute_image_url()
							else:
								request.session['urlimagen']='http://placehold.it/50x50'
							if 'url_redirect'in request.GET:
								url = '%s' % request.GET['url_redirect']
								print 'No tiene url_redirect'
							else:
								url = reverse('listaEgresados')
							
							return HttpResponseRedirect(url)
						else:
							info = "Contraseña no válida"	
							form = InicioSesion2Form()
							return render_to_response('index.html',locals(), context_instance=RequestContext(request))

					except ObjectDoesNotExist:
						info = "No tiene permisos para acceder al Sistema"
						form = InicioSesion2Form()
						return render_to_response('index.html',locals(), context_instance=RequestContext(request))

			except ObjectDoesNotExist:
				info = "No tiene permiso para acceder al Sistema"
				form = InicioSesion2Form()
				return render_to_response('index.html',locals(), context_instance=RequestContext(request))
	else:
		form = InicioSesion2Form()
	return render(request, 'index.html',locals())

@loginRequired
def listaCategoria(request):
	busqueda = request.POST.get('busqueda')
	query = request.POST.get('query')
	if request.method == 'POST':
		if query and busqueda:
			if busqueda == 'name':
				cat = Categoria.objects.filter(name__istartswith=query)
			else:
				cat = Categoria.objects.all()
		else:
			cat = Categoria.objects.all()
	else:
		cat = Categoria.objects.all()
	return render_to_response("blog/Lista_Categorias.html", {"cat":cat}, RequestContext(request))

@loginRequired
def addCategoria(request):
	if request.method == 'POST':
		form = CategoriaForm(request.POST)
		#verificamos si es valido
		if form.is_valid():
			form.save()
			if form:
				mensaje = 'Se ha guardado correctamente!'
				clase = 'success'
			else:
				mensaje = 'No se ha guardado correctamente, intentelo de nuevo'
				clase = 'danger'
			return render(request, "blog/addCategoria.html", {'form': form, 'clase':clase, 'mensaje':mensaje })
	else:
		form = CategoriaForm()
	return render(request, 'blog/addCategoria.html', {'form':form})

@loginRequired
def addEntrada(request):
	entradas = Entrada.objects.all()
	x = request.session['id_coordinador']
	a = Administrador.objects.get(pk=x)		

	if request.method == 'POST':
		form = CrearEntradaForm(request.POST, request.FILES)
		if form.is_valid():
			#obtenemos las variables del arreglo request.POST[] que contiene los atributos del formulario
			entradas = form.save(commit=False)
			entradas.autor=a
			entradas.slug = entradas.titulo
			entradas.ultima_modificacion=datetime.datetime.now()
			entradas.save()
			form.save()
			return HttpResponseRedirect(reverse('listaEntrada'))
	else:
		form = CrearEntradaForm()
		return render(request, 'blog/addEntrada.html', {'form' :form})
	
def verEntradas(request, slug):
	try:
		cat_entrada = Categoria.objects.all()
		entradas = Entrada.objects.get(slug=slug)
		entra = Categoria.objects.all()
		return render_to_response("blog/verEntradas.html", {"entradas":entradas,"entra":entra,"cat_entrada":cat_entrada}, RequestContext(request))
	except ObjectDoesNotExist:
		return HttpResponseRedirect(reverse('listaEntradas'))

@loginRequired
def editEntrada(request, entradas_id):
	e = get_object_or_404(Entrada,pk=entradas_id)
	if request.method == 'POST':
		form = CrearEntradaForm(request.POST, request.FILES, instance=e)
		if form.is_valid():
			guardar = form.save(commit=False)
			guardar.ultima_modificacion=datetime.datetime.now()
			guardar.save()
			form.save_m2m()
			return 	HttpResponseRedirect(reverse('listaEntrada'))
	else:
		form = CrearEntradaForm(instance=e)
	return render_to_response('blog/addEntrada.html', {'form': form}, context_instance=RequestContext(request))

class EgresadosUpdateView(UpdateView):
	model = Egresado
	form_class = EgresadosForm
	template_name = 'blog/actualiza_datos.html'
	success_url = reverse_lazy('egresado_perfil')

	#solo podra acceder si ha iniciado session
	@method_decorator(loginRequired)
	def dispatch(self, request, *args, **kwargs):
	    return super(EgresadosUpdateView, self).dispatch(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		self.object = get_object_or_404(Egresado, pk=request.session['id_egresado'])
		direccion_actual_object = DireccionActual.objects.filter(egresado=self.object).first()
		#usuario = self.object.usuario
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		print self.object
		
		telefono_form = TelefonoFormSet(instance=self.object)
		#usuario_form = UsuarioForm(instance=usuario)
		experiencia_form = ExperienciaLaboralFormSet(instance=self.object)
		direccion_form = DireccionFormSet(instance=self.object)
		estudios_form = EstudiosFormSet(instance=self.object)
		idioma_form = IdiomaFormSet(instance=self.object)
		asociacion_form = AsociacionFormSet(instance=self.object)
		premio_form = PremioFormSet(instance=self.object)
		direccion_actual_form = DireccionActualForm(instance=direccion_actual_object)
		return self.render_to_response(
            self.get_context_data(form = form,
                                  telefono_form = telefono_form,
                                  experiencia_form = experiencia_form,
                                  direccion_form = direccion_form,
                                  estudios_form= estudios_form,
                                  idioma_form = idioma_form,
                                  premio_form = premio_form,
                                  asociacion_form = asociacion_form,
                                  direccion_actual_form = direccion_actual_form
                                   ))

	

	def post(self, request, *args, **kwargs):
		self.object = Egresado.objects.get(pk=request.session['id_egresado'])
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		direccion_actual_object = DireccionActual.objects.filter(egresado=self.object).first()
		#form = EgresadoForm(self.request.POST, self.request.FILES)
		#usuario_form = UsuarioForm(self.request.POST, instance=self.object.usuario)
		experiencia_form = ExperienciaLaboralFormSet(self.request.POST,instance =self.object)
		direccion_form = DireccionFormSet(self.request.POST, instance=self.object)
		telefono_form = TelefonoFormSet(self.request.POST, instance= self.object)
		estudios_form = EstudiosFormSet(self.request.POST, instance=self.object)
		idioma_form = IdiomaFormSet(self.request.POST, instance=self.object)
		asociacion_form = AsociacionFormSet(self.request.POST, instance=self.object)
		premio_form = PremioFormSet(self.request.POST, instance=self.object)
		direccion_actual_form = DireccionActualForm(self.request.POST, instance=direccion_actual_object)
		if (form.is_valid() and experiencia_form.is_valid() and telefono_form.is_valid() and direccion_form.is_valid() and estudios_form.is_valid() and idioma_form.is_valid() and asociacion_form.is_valid() and premio_form.is_valid() and direccion_actual_form.is_valid() ):
			return self.form_valid(form, experiencia_form, telefono_form, direccion_form, estudios_form, asociacion_form, premio_form, idioma_form, direccion_actual_form)
		else:
			return self.form_invalid(form, experiencia_form, telefono_form, direccion_form, estudios_form, asociacion_form, premio_form, idioma_form, direccion_actual_form)

	def form_valid(self, form, experiencia_form, telefono_form, direccion_form, estudios_form, asociacion_form, premio_form, idioma_form, direccion_actual_form):
		"""
		Called if all forms are valid. Creates a Recipe instance along with
		associated Ingredients and Instructions and then redirects to a
		success page.
		"""
		#instanciamos el modelForm de usuario y lo guardamos pero no le damos commit
		
		#como exclui el campo ultimo ingreso necesito agregarlo antes de guardar
		
		#guardamos porque ya tenemos el usuario con todos sus datos
		
		
		#como un objeto egresado necesita una instancia de usuario entonces le pasamos la instancia que acabamos de crear de usuario
		egresado = form.save()
		#egresado.usuario = user
		#le paso la instancia de usuario a egresado para poder guardar correctamente
		egresado.ultima_modificacion = datetime.datetime.now()
		egresado.save()
		
		
		#ahora guardamos el form de egresado pero lo guardamos en el self.object
		self.object = egresado
		#como la direccion necesita una instancia de usuario se la asignamos al form de direccion
		#direccion_form.instance = user
		direccion_form.save()
		d_actual_object = direccion_actual_form.save(commit=False)
		d_actual_object.egresado = self.object
		d_actual_object.save()

		#print self.object
		idioma_form.save()
		asociacion_form.save()
		premio_form.save()
		#telefono_form.instance = self.object
		telefono_form.save()

		#experiencia_form.instance = self.object
		experiencia_form.save()
		estudios_form.save()

		
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form, experiencia_form, telefono_form, direccion_form, estudios_form, asociacion_form, premio_form, idioma_form, direccion_actual_form):
		"""
		Called if a form is invalid. Re-renders the context data with the
		data-filled forms and errors.
		"""
		return self.render_to_response(
			self.get_context_data(form=form,
                                  telefono_form=telefono_form,
                                  experiencia_form = experiencia_form,
                                  direccion_form = direccion_form,
                                  estudios_form = estudios_form,
                                  asociacion_form=asociacion_form,
                                  premio_form=premio_form,
                                  idioma_form=idioma_form,
                                  direccion_actual_form=direccion_actual_form))


@loginRequired_egresado
def egresado_perfil(request):
	tels = Telefono.objects.all().filter(usuario=request.session['id_egresado'])
	idiomas = Idioma.objects.all().filter(egresado=request.session['id_egresado'])
	estrs = EstudiosRealizados.objects.all().filter(egresado=request.session['id_egresado'])
	egresados = get_object_or_404(Egresado, pk=request.session['id_egresado'])
	dirs = DireccionActual.objects.all().filter(egresado=request.session['id_egresado'])
	dirs_otros = Direccion.objects.all().filter(egresado=request.session['id_egresado'])
	xplab = ExperienciaLaboral.objects.all().filter(egresado=request.session['id_egresado'])
	asociacion = Asociacion.objects.all().filter(egresado=request.session['id_egresado'])
	premio = Premio.objects.all().filter(egresado=request.session['id_egresado'])
	return render_to_response("blog/egresado_perfil.html", locals(), RequestContext(request))

@loginRequired_egresado
def egresados(request):
	egresados = get_object_or_404(Egresado, pk=request.session['id_egresado'])
	return render_to_response("blog/egresados.html", {'egresados':egresados}, RequestContext(request))

def eventos(request):
	eventos = Evento.objects.filter(estado=True).order_by("-fecha_creacion")
	# numero de paginas por portada
	paginator = Paginator(eventos, 6)

	page = request.GET.get('page')

	try:
		eventos = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		eventos = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		eventos = paginator.page(paginator.num_pages)

	return render_to_response("blog/eventos.html",locals(), RequestContext(request))

def verEvento(request, evento_id):
	e = get_object_or_404(Evento,pk=evento_id)
	asistente = Asistente.objects.filter(evento=evento_id)

	if request.method == 'POST':
		egresados = get_object_or_404(Egresado, pk=request.POST['id-egresado'])
	
		asistente = Asistente()
		asistente.egresado = egresados  
		asistente.evento = e
		asistente.save()

		info = "Se agrego a la lista de asistentes"
		e = get_object_or_404(Evento, pk=evento_id)

		asistente = Asistente.objects.filter(evento=evento_id)
		return render(request, 'blog/evento.html', {'evento':e,'info':info,'asistente':asistente})

	return render(request, 'blog/evento.html', {'evento':e,'asistente':asistente})

def encuestas(request):
	if 'id_user' in request.session:
		if request.session['tipo_usuario'] == 'egresado':
			encuesta = Encuesta.objects.filter(estado=True).filter(tipo='egresado')
		else:
			encuesta = Encuesta.objects.filter(estado=True).filter(tipo='empresa')
		usuario = get_object_or_404(Usuario, pk=request.session['id_user'])
		respuesta = Respuesta.objects.filter(usuario=usuario)
		respuestav = RespuestaV.objects.filter(usuario=usuario)
		p1 = []
		for res in respuesta:
			x = res.encuesta.id
			if not x in p1:
				p1.append(x)
	else:
		return HttpResponseRedirect(reverse('index'))
	return render_to_response("blog/encuestas_list.html",locals(), RequestContext(request))


			
class VisualizarEncuestasCreateView(CreateView):
	#model = Pregunta
	#form_class = PreguntaForm
	template_name = "blog/encuesta.html"
	success_url = reverse_lazy('encuestas')

	@method_decorator(loginRequired_users)
	def dispatch(self, request, *args, **kwargs):
		return super(VisualizarEncuestasCreateView, self).dispatch(request, *args, **kwargs)


	def get(self, request, *args, **kwargs):
		self.object = None
		encuesta = get_object_or_404(Encuesta, pk=self.kwargs['encuesta_id'])
		usuario = get_object_or_404(Usuario, pk=request.session['id_user'])
		#para verificar si ya el usuario relizo la encuesta
		respuesta = Respuesta.objects.filter(usuario=usuario).filter(encuesta=encuesta)
		respuestav = RespuestaV.objects.filter(usuario=usuario).filter(encuesta=encuesta)
		if not respuesta and not respuestav:
			p = Pregunta.objects.filter(encuesta=encuesta)
			op = Opcion.objects.filter(pregunta__encuesta=encuesta)
			return self.render_to_response(self.get_context_data(encuesta=encuesta, preguntas=p, opcion = op))
		else:
			return self.render_to_response(self.get_context_data(info="Usted ya realizo la encuesta"))

			
		"""try:
			respuesta = Respuesta.objects.filter(usuario=usuario).filter(encuesta=encuesta)
			respuestav = RespuestaV.objects.filter(usuario=usuario).filter(encuesta=encuesta)
			print respuesta
			print respuestav
			return self.render_to_response(self.get_context_data(info="Usted ya realizo la encuesta"))
		except Exception, e:
			p = Pregunta.objects.filter(encuesta=encuesta)
			op = Opcion.objects.filter(pregunta__encuesta=encuesta)
			return self.render_to_response(self.get_context_data(encuesta=encuesta, preguntas=p, opcion = op))
		"""
			
		
	def post(self, request, *args, **kwargs):
		self.object = None
		encuesta = get_object_or_404(Encuesta, pk=self.kwargs['encuesta_id'])
		pre = Pregunta.objects.filter(encuesta=encuesta)
		usuario = get_object_or_404(Usuario, pk=request.session['id_user'])
		for p in pre:
			indice = "respuesta[%s]" % p.pk
			res = request.POST.getlist(indice)
			w = len(res)
			if p.tipo == 'AB' and w == 1:
				rv = RespuestaV()
				rv.pregunta = p
				rv.usuario = usuario
				for r in res:
					rv.respuesta = r
					rv.encuesta = encuesta
				rv.save()

			elif p.tipo == 'UN' and w == 1:
				for r in res:
					opcion = Opcion.objects.get(pk=r)
					respuesta = Respuesta()
					respuesta.usuario = usuario
					respuesta.opcion = opcion
					respuesta.pregunta = p
					respuesta.encuesta = encuesta
					respuesta.save()

			elif p.tipo == 'MU' and w >= 1:
				for r in res:
					opcion = Opcion.objects.get(pk=r)
					respuesta = Respuesta()
					respuesta.usuario = usuario
					respuesta.opcion = opcion
					respuesta.pregunta = p
					respuesta.encuesta = encuesta
					respuesta.save()
		return HttpResponseRedirect(reverse('encuestas'))

@loginRequired_egresado
def empleos(request):
	ofertas = Oferta.objects.all()
	busqueda = request.POST.get('busqueda')
	query = request.POST.get('query')
	if request.method == 'POST':
		if query and busqueda:
			if busqueda == 'nombre':
				ofertas = Oferta.objects.filter(nombre__istartswith=query)
			elif busqueda == 'desc':
				ofertas = Oferta.objects.filter(desc__icontains=query)
			elif busqueda == 'salario':
				ofertas = Oferta.objects.filter(salario__icontains=query)
			elif busqueda == 'perfil_profesional':
				ofertas = Oferta.objects.filter(perfil_profesional__icontains=query)
			elif busqueda == 'tipo_contrato':
				ofertas = Oferta.objects.filter(tipo_contrato__icontains=query)	
			else:
				ofertas = Oferta.objects.all()
		else:
			ofertas = Oferta.objects.all()
	else:
		ofertas = Oferta.objects.all()
	return render_to_response('blog/empleos_egresado.html',{'ofertas':ofertas}, context_instance=RequestContext(request))

def addEmpresaOferta(request):
	if "id_empresa" in request.session:
		return HttpResponseRedirect(reverse('empresa'))
	elif "id_egresado" in request.session:
		return HttpResponseRedirect(reverse('empleos'))
	else:
		if request.method=='POST' and 'form2' in request.POST:
			form2 = AddEmpresaForm(request.POST)
			if request.POST['password'] == request.POST['password2']:
				#verificamos si es valido
				if form2.is_valid():
					#obtenemos las variables del arreglo request.POST[] que contiene los atributos del formulario
					codigo = form2.cleaned_data['codigo']
					password  = form2.cleaned_data['password']
					passencryp = make_password(password)
					nombre = form2.cleaned_data['nombre']
					tipo = form2.cleaned_data['tipo']
					email = form2.cleaned_data['email']
					#se establece el codigo de confirmacion para el usuario empresa
					salt = hashlib.sha1(str(random.random())).hexdigest()[:7]
					activation_key = hashlib.sha1(salt).hexdigest()
					#creacion de objeto usuario para obtener una instancia y poder relacionarla con una empresa
					nuevo_usuario = Usuario.objects.create(codigo=codigo,password=passencryp, password_2=passencryp, codigo_confirmacion=activation_key, ultimo_ingreso=datetime.datetime.now(), email=email)
					#creacion de objeto empresa con relacion al objeto usuario creado anteriormente
					nuevo_empresa = Empresa.objects.create(nombre=nombre, tipo=tipo, usuario=nuevo_usuario)
					#si todo esta correcto redirigimos hacia la vista para logear empresa
					mensaje = 'Tu empresa se ha registrado exitosamente. Se ha enviado un codigo de confirmación a tu correo %s' % (nuevo_usuario.email) 
					link = "http://127.0.0.1:8000/confirm/%s" % (nuevo_usuario.codigo_confirmacion)
					CreateEmpresaTask.delay(email, nombre, link)
					form2 = AddEmpresaForm()	
			else:
				form2 = AddEmpresaForm()	
		else:
			form2 = AddEmpresaForm()

		if request.method=='POST' and 'form1' in request.POST:
			#dd
			form = InicioSesion3Form(request.POST)
			if form.is_valid():
				codigo = request.POST['codigo']
				clave = request.POST['clave']
				tipo_u = request.POST['tipo_u']
				try:
					u = Usuario.objects.get(codigo=codigo)
					
					try:
						if tipo_u == 'Egresado':
							egresado = Egresado.objects.get(usuario=u)
							if check_password(clave, u.password):
								if u.estado == True:
									request.session['id_egresado']=egresado.id
									request.session['id_user']=u.id	
									request.session['tipo_usuario']="egresado"
									request.session['nombre_u']=egresado.p_nombre
									if egresado.imagen:
										request.session['urlimagen_egresado']=egresado.imagen.url
									else:
										request.session['urlimagen_egresado']='http://placehold.it/50x50'
									return HttpResponseRedirect(reverse('egresados'))
								else:
									form = InicioSesion3Form()
									info = "Tu cuenta no ha sido verificada, revisa tu correo de confirmación."
									check = "danger"
							else:
								#return HttpResponse('clave incorrecta')
								form = InicioSesion3Form()
								info = "Contraseña no válida"	
								return render_to_response('blog/addEmpresaOferta.html',locals(), context_instance=RequestContext(request))
						elif tipo_u == 'Empresa':
							empresa = Empresa.objects.get(usuario=u)

							if check_password(clave, u.password):
								if u.estado == True:
									request.session['id_empresa']=empresa.id
									request.session['id_user']=u.id
									request.session['tipo_usuario']="empresa"	
									request.session['nombre_u']=empresa.nombre
									return HttpResponseRedirect(reverse('empresa'))
								else:
									form = InicioSesion3Form()
									info = "Tu cuenta no ha sido verificada, revisa tu correo de confirmación."
							else:
								#return HttpResponse('clave incorrecta')
								form = InicioSesion3Form()
								info = "Contraseña no válida"	
								return render_to_response('blog/addEmpresaOferta.html',locals(), context_instance=RequestContext(request))

						elif tipo_u == 'Coordinador':
							try:
								c = Administrador.objects.get(usuario=u)
								if check_password(clave, u.password):
									#if u.estado == True:
									request.session['id_usuario']=u.id
									request.session['nombre_usuario']=c.nombre
									request.session['id_coordinador']=c.id
									if c.foto:
										request.session['urlimagen']=c.get_absolute_image_url()
									else:
										request.session['urlimagen']='http://placehold.it/50x50'
									if 'url_redirect'in request.GET:
										url = '%s' % request.GET['url_redirect']
										print 'No tiene url_redirect'
									else:
										url = reverse('listaEgresados')
									
									return HttpResponseRedirect(url)
								else:
									#return HttpResponse('clave incorrecta')
									info = "Contraseña no válida"	
									form = InicioSesion2Form()
									return render_to_response('blog/addEmpresaOferta.html',locals(), context_instance=RequestContext(request))

							except ObjectDoesNotExist:
								info = "No tiene permisos para Acceder al Sistema"
								form = InicioSesion2Form()
								return render_to_response('blog/addEmpresaOferta.html',locals(), context_instance=RequestContext(request))
					except ObjectDoesNotExist:
						info = "Codigo o contraseña no válida"
						form = InicioSesion3Form()
						return render_to_response('blog/addEmpresaOferta.html',locals(), context_instance=RequestContext(request))
				except ObjectDoesNotExist:
					info = "No tiene permisos para Acceder al Sistema"
					form = InicioSesion3Form()
					return render_to_response('blog/addEmpresaOferta.html',locals(), context_instance=RequestContext(request))
			
		else:
			form = InicioSesion3Form()
	return render(request, 'blog/addEmpresaOferta.html',locals())

@loginRequired_empresa	
def publicar(request):
	emp = get_object_or_404(Empresa, pk=request.session['id_empresa'])	
	if request.method == 'POST':
		form = CrearOfertaFormb(request.POST)

		#Validamos
		if form.is_valid():
			nombre = form.cleaned_data['nombre']
			desc = form.cleaned_data['desc']
			salario = form.cleaned_data['salario']
			perfil_profesional= form.cleaned_data['perfil_profesional']
			horario = form.cleaned_data['horario']
			tipo_contrato = form.cleaned_data['tipo_contrato']
			uoferta = emp.usuario
			contacto = form.cleaned_data['contacto']
			telefonofijo_contacto= form.cleaned_data['telefonofijo_contacto']
			correo = form.cleaned_data['correo']
			celular_contacto = form.cleaned_data['celular_contacto']
			#estado_oferta = form.cleaned_data['estado_oferta']
			estado_oferta = True

			new_oferta = Oferta.objects.create(nombre=nombre, 
											desc=desc,
				    						salario=salario, 
				    						perfil_profesional=perfil_profesional, 
				    						horario=horario, 
				    						tipo_contrato=tipo_contrato, 
				    						contacto=contacto, 
				    						telefonofijo_contacto=telefonofijo_contacto, 
				    						celular_contacto=celular_contacto, 
				    						correo=correo, 
				    						estado_oferta=estado_oferta, 
				    						fecha_creacion=datetime.datetime.now(), 
				    						uoferta=uoferta)
			new_oferta.save()
			return HttpResponseRedirect(reverse('empresa'))
		#else:
		#	mensaje = "Formulario invalido, por favor ingrese correctamente los datos"	
	else:
		form = CrearOfertaFormb()
	return render(request, 'blog/publicar.html',locals())

@loginRequired_empresa
def empresa(request):
	empresas = get_object_or_404(Empresa, pk=request.session['id_empresa'])
	ofertas = Oferta.objects.all().filter(uoferta=empresas.usuario)
	return render_to_response('blog/empresa.html', {"empresas":empresas, "ofertas":ofertas}, context_instance=RequestContext(request))

@loginRequired_empresa
def verOfertae(request, ofertas_id):
	emp = get_object_or_404(Empresa, pk=request.session['id_empresa'])	
	u = get_object_or_404(Oferta, pk=ofertas_id)

	if emp.usuario == u.uoferta:
		ofertas = get_object_or_404(Oferta, pk=ofertas_id)
		return render_to_response("blog/verOferta_emp.html", {"ofertas":ofertas}, RequestContext(request))
	else:
		return HttpResponseRedirect(reverse('addEmpresaOferta'))

def listaEntradas(request):
	entradas = Entrada.objects.order_by('-fecha_creacion')
	cat_entrada = Categoria.objects.all()
	# numero de paginas por portada
	paginator = Paginator(entradas, 6)

	page = request.GET.get('page')

	try:
		entrada = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		entrada = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		entrada = paginator.page(paginator.num_pages)
	return render_to_response('blog/Noticias.html', locals(), context_instance=RequestContext(request))

@loginRequired_egresado
def cambiarcontra(request, template_name='blog/cpassword.html'):
	egresados = get_object_or_404(Egresado, pk=request.session['id_egresado'])
	usuarios = get_object_or_404(Usuario, pk=egresados.usuario.pk)
	if request.method == 'POST':
		form = CpasswordForm(request.POST)
		if form.is_valid():
			password  = form.cleaned_data['password']
			password_2  = form.cleaned_data['password_2']	
			password_a = form.cleaned_data['password_a']		

			if check_password(password_a, usuarios.password):
				if password == password_2:
					passencryp = make_password(password)
					usuarios.password = passencryp
					usuarios.password_2 = usuarios.password
					usuarios.save()
					return HttpResponseRedirect(reverse('egresados'))
				else:
					info = "Contraseñas nuevas son distintas"		
					form = CpasswordForm()
			else:
				info = "Contraseña antigua no concuerda con la nueva"	
				form = CpasswordForm()
		else:
			form = CpasswordForm()
	if request.method == 'GET':
		form = CpasswordForm(initial={'password':usuarios.password,
    									'password_2':usuarios.password_2,
		})
	return render(request, template_name, locals())

@loginRequired_empresa
def cambiarcontra_empresa(request, template_name='blog/cpassword_empresa.html'):
	empresas = get_object_or_404(Empresa, pk=request.session['id_empresa'])
	usuarios = get_object_or_404(Usuario, pk=empresas.usuario.pk)
	if request.method == 'POST':
		form = CpasswordForm(request.POST)
		if form.is_valid():
			password  = form.cleaned_data['password']
			password_2  = form.cleaned_data['password_2']	
			password_a = form.cleaned_data['password_a']		

			if check_password(password_a, usuarios.password):
				if password == password_2:
					passencryp = make_password(password)
					usuarios.password = passencryp
					usuarios.password_2 = usuarios.password
					usuarios.save()
					return HttpResponseRedirect(reverse('empresa'))
				else:
					form = CpasswordForm()
					info = "Contraseñas nuevas no son iguales"	
			else:
				print usuarios.password
				form = CpasswordForm()
				info = "Contraseña actual no válida"	
		else:
			form = CpasswordForm()
			info = "Credenciales Invalidas"	

	if request.method == 'GET':
		form = CpasswordForm(initial={'password':usuarios.password,
    									'password_2':usuarios.password_2,
		})
	return render(request, template_name, locals())


@loginRequired_egresado
def verOfertas(request, ofertas_id):
	ofertas = get_object_or_404(Oferta, pk=ofertas_id)
	vacantes = Vacante.objects.all().filter(oferta=ofertas_id)
	egresados = get_object_or_404(Egresado, pk=request.session['id_egresado'])

	if request.method == 'POST':
		form = VacanteForm(request.POST, request.FILES)
		#verificamos si es valido
		if form.is_valid():
			vacantes = form.save(commit=False)
			vacantes.oferta = ofertas
			vacantes.fecha_creacion=datetime.datetime.now()
			vacantes.egresado_postulado = egresados
			vacantes.estado_vacante = 'Sin respuesta'
			vacantes.save()
			form.save()
			return HttpResponseRedirect(reverse('empleos'))
	else:
		form = VacanteForm()

	return render_to_response("oferta/verOferta.html", locals(), RequestContext(request))

@loginRequired_empresa
def emp_postulados(request, ofertas_id):
	ofertas = get_object_or_404(Oferta, pk=ofertas_id)
	vacantes = Vacante.objects.all().filter(oferta=ofertas_id)
	try:
		instance = get_object_or_404(Vacante, oferta=ofertas_id)
		form = Aprobar_rechazarForm(request.POST or None, instance=instance)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse('emp_postulados',ofertas_id))
	except:
		pass
	return render_to_response("blog/emp_postulados.html", locals(), RequestContext(request))

@loginRequired_empresa
def emp_editOferta(request, ofertas_id):
	e = get_object_or_404(Oferta, pk=ofertas_id)
	if request.method == 'POST':
		form = EditOferta_empForm(request.POST)
		#Validamos
		if form.is_valid():
			e.nombre = form.cleaned_data['nombre']
			e.desc = form.cleaned_data['desc']
			e.salario = form.cleaned_data['salario']
			e.perfil_profesional= form.cleaned_data['perfil_profesional']
			e.horario = form.cleaned_data['horario']
			e.tipo_contrato = form.cleaned_data['tipo_contrato']
			e.uoferta = e.uoferta
			e.contacto = form.cleaned_data['contacto']
			e.telefonofijo_contacto= form.cleaned_data['telefonofijo_contacto']
			e.correo = form.cleaned_data['correo']
			e.celular_contacto = form.cleaned_data['celular_contacto']
			e.estado_oferta = e.estado_oferta
			e.save()
			return 	HttpResponseRedirect(reverse('empresa'))

	if request.method == 'GET':
		form = EditOferta_empForm(initial={
									'nombre': e.nombre,
									'desc': e.desc,
									'salario': e.salario,
									'perfil_profesional':e.perfil_profesional,
									'horario': e.horario,
									'tipo_contrato': e.tipo_contrato,
									'uoferta': e.uoferta,
									'contacto': e.contacto,
									'telefonofijo_contacto': e.telefonofijo_contacto,
									'correo': e.correo,
									'celular_contacto': e.celular_contacto,
									'estado_oferta': e.estado_oferta,										
		})
	
	accion = "Editar"
	return render(request, 'blog/emp_editOferta.html', {'form': form, 'accion':accion})

@loginRequired
def delEntrada(request, entradas_id):
	entradas = get_object_or_404(Entrada,pk=entradas_id)
	entradas.delete()
	return HttpResponseRedirect(reverse('listaEntrada'))

@loginRequired
def delCategoria(request, categorias_id):
	categorias = get_object_or_404(Categoria,pk=categorias_id)
	categorias.delete()
	return HttpResponseRedirect(reverse('listaCategoria'))

@loginRequired_empresa
def emp_delOferta(request, ofertas_id):
	ofertas = get_object_or_404(Oferta,pk=ofertas_id)
	ofertas.delete()
	return HttpResponseRedirect(reverse('empresa'))

@loginRequired
def listaEntrada(request):
	busqueda = request.POST.get('busqueda')
	query = request.POST.get('query')
	categorias = request.POST.get('categorias')
	retorno = Entrada.objects.order_by("-fecha_creacion")

	cat = Categoria.objects.all()
	if request.method == 'POST':
		if query and busqueda:
			if busqueda == 'titulo':
				entradas = Entrada.objects.filter(titulo__istartswith=query)
			elif busqueda == 'contenido':
				entradas = Entrada.objects.filter(contenido__icontains=query)
		elif categorias and busqueda:
			if busqueda == 'categoria':
				cat = Categoria.objects.filter(name=categorias)
				entradas = Entrada.objects.filter(categorias__istartswith=cat)
			else:
				entradas = retorno
		else:
			entradas = retorno
	else:
		entradas = retorno
	return render(request, 'blog/Lista_entrada.html', {'entradas':entradas, 'cat':cat})

def verCat_entrada(request, categorias_id):
	entradas = Entrada.objects.filter(categorias=categorias_id).order_by("-fecha_creacion")
	cat_entrada = Categoria.objects.all()
	paginator = Paginator(entradas, 6)

	page = request.GET.get('page')

	try:
		entrada = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		entrada = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		entrada = paginator.page(paginator.num_pages)
	return render_to_response('blog/verCat_entrada.html', locals(), context_instance=RequestContext(request))

def recuperar_pass(request):
	if request.method == 'POST':
		form = recoverPassForm(request.POST)
		#Validamos
		if form.is_valid():
			correo = form.cleaned_data['correo']
			try:
				user = Usuario.objects.get(email=correo)
				salt = hashlib.sha1(str(random.random())).hexdigest()[:8]
				
				if user.estado:
					try:
						e = Egresado.objects.get(usuario=user)
						password = salt  
						nombre = "%s %s" % (e.p_nombre, e.p_apellido)
						passencryp = make_password(password)
						user.password = passencryp
						user.password_2 = user.password
						user.save()
						RecoveryUserTask.delay(user.email, nombre, password)
						mensaje = "Hemos enviado un email a tu correo electronico"
						return render(request, 'blog/recuperar_password.html', locals())
					except Exception, e:
						try:
							emp = Empresa.objects.get(usuario=user)
							password = salt  
							nombre = emp.nombre
							passencryp = make_password(password)
							user.password = passencryp
							user.password_2 = user.password
							user.save()
							RecoveryUserTask.delay(user.email, nombre, password)
							mensaje = "Hemos enviado un email a tu correo electronico"
							return render(request, 'blog/recuperar_password.html', locals())
						except Exception, e:
							info = "No se encontro coincidencia con tu email"
				elif Administrador.objects.get(usuario=user):
					password = salt
					passencryp = make_password(password)
					user.password = passencryp
					user.password_2 = user.password
					user.save()
					RecoveryUserTask.delay(user.email, user.codigo, password)	
					mensaje = "Hemos enviado un email a tu correo electronico"	
					return render(request, 'blog/recuperar_password.html', locals())
				else:	
					info = "Tu email pertenece a una cuenta de usuario no confirmada"
					clase = "danger"
					return render(request, 'blog/recuperar_password.html', locals())
			except Exception, e:
				info = "No se encontro ninguna coincidencia del email"
				clase = "danger"
				return render(request, 'blog/recuperar_password.html', locals())
	else:
		form = recoverPassForm()
	return render(request, 'blog/recuperar_password.html', locals())

def confimar_pass(request, key):
	usuarios = get_object_or_404(Usuario,codigo_confirmacion=key)
	if request.method == 'POST':
		form = ResetpassForm(request.POST)
		if form.is_valid():
			password  = form.cleaned_data['password']
			password_2  = form.cleaned_data['password_2']			

			if password == password_2:
				passencryp = make_password(password)
				usuarios.password = passencryp
				usuarios.password_2 = usuarios.password
				usuarios.codigo_confirmacion = 0
				usuarios.save()
				mensaje = "Tu contraseña fue cambiada con exitosamente!"
			else:
				info = "Contraseñas nuevas son distintas"		
				form = ResetpassForm()

		else:
			form = ResetpassForm()
	if request.method == 'GET':
		form = ResetpassForm(initial={'password':usuarios.password,
    									'password_2':usuarios.password_2,
		})
	return render(request, 'blog/nueva_passConfirm.html', locals())

@loginRequired_empresa
def editEmpresa(request):
	e = get_object_or_404(Empresa,pk=request.session['id_empresa'])

	if request.method == 'POST':
		form = EditEmpresaForm(request.POST)
		if form.is_valid():
			e.nombre = form.cleaned_data['nombre']
			e.tipo = form.cleaned_data['tipo']
			e.usuario.email = form.cleaned_data['email']
			usuario = Usuario.objects.get(pk=e.usuario.pk)
			e.save()
			usuario.email = form.cleaned_data['email']
			usuario.save()
			mensaje = "Se actualizo la informacion exitosamente!"
		else:
			mensaje = "Formulario invalido, por favor ingrese correctamente los datos"

	if request.method == 'GET':
		form = EditEmpresaForm(initial={
									'nombre': e.nombre,
									'tipo': e.tipo,
									'email': e.usuario.email,
			})
	
	accion = "Editar"
	return render(request, 'blog/editEmpresa.html',locals())

@loginRequired
def editCategoria(request, categorias_id):
	c = get_object_or_404(Categoria,pk=categorias_id)

	if request.method == 'POST':
		form = CategoriaForm(request.POST)
		if form.is_valid():
			c.name = form.cleaned_data['name']
			c.save()
			mensaje = "Se edito correctamente la categoria"	
			return render(request, 'blog/editCategoria.html',{'mensaje':mensaje,'form':form})

	if request.method == 'GET':
		form = CategoriaForm(initial={
									'name': c.name,
			})
	
	accion = "Editar"
	return render(request, 'blog/editCategoria.html',locals())

def mision_vision(request):
	cat_entrada = Categoria.objects.all()
	return render_to_response('blog/mision_vision.html', {'cat_entrada':cat_entrada}, context_instance=RequestContext(request))

def objetivos(request):
	cat_entrada = Categoria.objects.all()
	return render_to_response('blog/objetivos.html', {'cat_entrada':cat_entrada}, context_instance=RequestContext(request))

def politicas(request):
	cat_entrada = Categoria.objects.all()
	return render_to_response('blog/politicas.html', {'cat_entrada':cat_entrada}, context_instance=RequestContext(request))

		
@loginRequired_egresado
def asisEvento(request, evento_id):
	if request.method == 'GET':
		evento = get_object_or_404(Evento, pk=evento_id)
		egresados = get_object_or_404(Egresado, pk=request.session['id_egresado'])
		
		asistente = Asistente()
		asistente.egresado = egresados  
		asistente.evento = evento
		asistente.save()

		#print asistente
		return render(request, "blog/evento.html", {'evento':e, 'info':info})