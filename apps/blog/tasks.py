# -*- encoding: utf-8 -*-
from oveu.celery import app
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import EmailMultiAlternatives

@app.task(bind=True)
def RecoveryUserTask(self, email, nombre, password):
	subject = 'Instrucciones para recuperar su contraseña'
	from_email = 'rafaelmartinezs@live.com'
	to = email
	html_content = render_to_string('blog/recuperar_pass.html', {'nombre':nombre, 'password':password})
	text_content = strip_tags(html_content)
	msg = EmailMultiAlternatives(subject,text_content, from_email, [to])
	msg.attach_alternative(html_content, "text/html")
	msg.send()

@app.task(bind=True)
def CreateEmpresaTask(self, email, nombre, link):
	subject = 'Bienvenido %s, confirme su cuenta.' % (nombre)
	from_email = 'rafaelmartinezs@live.com'
	to = email
	html_content = render_to_string('blog/confirm_empresa.html', {'nombre':nombre, 'link':link})
	text_content = strip_tags(html_content)
	msg = EmailMultiAlternatives(subject,text_content, from_email, [to])
	msg.attach_alternative(html_content, "text/html")
	msg.send()