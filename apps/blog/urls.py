from django.conf.urls import patterns, url
from apps.blog.views import *

#from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()

urlpatterns = patterns('apps.blog.views',
    url(r'^$', 'index', name='index'), 
    url(r'^Noticias/$', 'listaEntradas', name='listaEntradas'),
    url(r'^mision-y-vision/$', 'mision_vision', name='mision_vision'),
    url(r'^objetivos/$', 'objetivos', name='objetivos'),
    url(r'^politicas/$', 'politicas', name='politicas'),
    url(r'^casa/$', 'casa', name='casa'),
    url(r'^Noticias/ver/(?P<slug>[-\w]+)/$', 'verEntradas', name='verEntradas'),
    url(r'^Noticias/lista/(?P<categorias_id>\d+)/$', 'verCat_entrada', name='verCat_entrada'),
    #evento
    url(r'^evento/ver/(?P<evento_id>\d+)$', 'verEvento', name='verEvento'),
    #url(r'^evento/asistencia/(?P<evento_id>\d+)$', 'asisEvento',name='asisEvento'),
    url(r'^eventos/$', 'eventos', name='eventos'),
    url(r'^cerrar-sesion/$', 'blogout', name='blogout'),
    url(r'^cerrar-session/$', 'elogout', name='elogout'),
    url(r'^recuperar/$', 'recuperar_pass', name='recuperar_pass'),
    url(r'^recuperar/confirm/(?P<key>\w+)/','confimar_pass', name='confimar_pass'),
    #encuesta
    url(r'^encuestas/$', 'encuestas', name='encuestas'),
    url(r'^encuesta/ver/(?P<encuesta_id>\d+)/$', VisualizarEncuestasCreateView.as_view(), name='encuesta'),
    #egresado
    url(r'^egresado/$', 'egresados', name='egresados'),
    url(r'^egresado/update/$', EgresadosUpdateView.as_view(), name='actualizaDatos'),
    url(r'^egresado/perfil/$', 'egresado_perfil', name='egresado_perfil'),
    url(r'^egresado/password/$', 'cambiarcontra', name='cambiarcontra'),
    #emplao
    url(r'^empleo/$', 'addEmpresaOferta', name='addEmpresaOferta'),
    #egresado empleo
    url(r'^empleo/egresado/$', 'empleos', name='empleos'),
    url(r'^empleo/egresado/ver/(?P<ofertas_id>\d+)/$', 'verOfertas', name='verOfertas'), 
    #empresa empleo
    url(r'^empleo/empresa/publicar/$', 'publicar', name='publicar'),
    url(r'^empleo/empresa/$', 'empresa', name='empresa'),
    url(r'^empleo/empresa/update/$', 'editEmpresa', name='editEmpresa'),
    url(r'^empleo/empresa/ver/(?P<ofertas_id>\d+)/$', 'verOfertae', name='verOfertae'),
    url(r'^empleo/empresa/ver/(?P<ofertas_id>\d+)/postulados/$', 'emp_postulados', name='emp_postulados'),  
    url(r'^empleo/empresa/edit/(?P<ofertas_id>\d+)/$', 'emp_editOferta', name='emp_editOferta'),
    url(r'^empleo/empresa/del/(?P<ofertas_id>\d+)/$', 'emp_delOferta', name='emp_delOferta'),
    url(r'^empleo/empresa/password/$', 'cambiarcontra_empresa', name='cambiarcontra_empresa'),

    
)