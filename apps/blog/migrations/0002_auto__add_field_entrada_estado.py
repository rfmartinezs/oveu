# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Entrada.estado'
        db.add_column(u'blog_entrada', 'estado',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Entrada.estado'
        db.delete_column(u'blog_entrada', 'estado')


    models = {
        u'blog.categoria': {
            'Meta': {'object_name': 'Categoria'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '140'})
        },
        u'blog.entrada': {
            'Meta': {'object_name': 'Entrada'},
            'autor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usuarios.Administrador']"}),
            'categorias': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['blog.Categoria']", 'symmetrical': 'False'}),
            'contenido': ('tinymce.models.HTMLField', [], {}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '140'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'ultima_modificacion': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'usuarios.administrador': {
            'Meta': {'object_name': 'Administrador'},
            'apellidos': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'default': "u'dirs'", 'max_length': '120', 'null': 'True', 'blank': 'True'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'telefono': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['usuarios.Usuario']", 'unique': 'True'})
        },
        u'usuarios.usuario': {
            'Meta': {'object_name': 'Usuario'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'fecha_creacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'password_2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ultimo_ingreso': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 3, 0, 0)'})
        }
    }

    complete_apps = ['blog']