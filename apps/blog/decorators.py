from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from functools import wraps

def loginRequired_egresado(a):
	@wraps(a)
	def wrapper(request, *args, **kwargs):
		if 'id_egresado' in request.session:
			return a(request, *args, **kwargs)
		else:
			url_redirect =  request.get_full_path()
			url_callback= "%s?url_redirect=%s" % (reverse('index'), url_redirect)
			return HttpResponseRedirect(url_callback)
	return wrapper

def loginRequired_empresa(a):
	@wraps(a)
	def wrapper(request, *args, **kwargs):
		if 'id_empresa' in request.session:
			return a(request, *args, **kwargs)
		else:
			url_redirect =  request.get_full_path()
			url_callback= "%s?url_redirect=%s" % (reverse('addEmpresaOferta'), url_redirect)
			return HttpResponseRedirect(url_callback)
	return wrapper
		
def loginRequired_users(a):
	@wraps(a)
	def wrapper(request, *args, **kwargs):
		if 'id_egresado' in request.session:
			return a(request, *args, **kwargs)
		elif 'id_empresa' in request.session:
			return a(request, *args, **kwargs)
		else:
			url_redirect =  request.get_full_path()
			url_callback= "%s?url_redirect=%s" % (reverse('index'), url_redirect)
			return HttpResponseRedirect(url_callback)
	return wrapper
		