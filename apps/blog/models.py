from django.db import models 
from apps.usuarios.models import Administrador
from tinymce import models as tinymce_models
from django.template.defaultfilters import slugify
from django.template import defaultfilters
from django.db.models.signals import pre_delete, pre_save
from django.dispatch import receiver
from PIL import Image 

class Categoria(models.Model):
	name = models.CharField(max_length=140)

	def __unicode__(self):
		return self.name

class Entrada(models.Model):
	titulo = models.CharField(max_length = 140)
	imagen = models.ImageField(upload_to='imagen_entrada',blank=True, null=True)
	slug = models.SlugField(max_length=140)
	categorias = models.ManyToManyField(Categoria)
	autor =  models.ForeignKey(Administrador)
	contenido = tinymce_models.HTMLField()
	estado = models.BooleanField(default=True)
	fecha_creacion = models.DateTimeField(auto_now=True, auto_now_add=True)
	ultima_modificacion = models.DateTimeField(auto_now=False, auto_now_add=False)

	def __unicode__(self):
		return self.titulo

	def get_absolute_image_url(self):
	    return '%s' % (self.imagen.url)

	def save(self, *args, **kwargs):
		self.slug = defaultfilters.slugify(self.titulo)
		super(Entrada, self).save(*args, **kwargs)	    

	# borrar imagen del disco duro
@receiver(pre_delete, sender=Entrada)
def post_pre_delete_handler(sender, instance, **kwargs):
	instance.imagen.delete(False)
