# -*- encoding: utf-8 -*-
from django.forms import ModelForm
from .models import *
from django import forms
from apps.usuarios.models import *
from apps.oferta.models import *
import json
from tinymce.widgets import TinyMCE

class CategoriaForm(ModelForm):
    class Meta:
        model 	= Categoria
        labels = {'name': ('Nueva categoria')}
        widgets={'name': forms.TextInput(attrs={'placeholder':'Nombre de la categoria'})}

class CrearEntradaForm(ModelForm):

	class Meta:
		model = Entrada
		exclude = ['autor','fecha_creacion','ultima_modificacion','slug']
		labels = {
			'estado': ('Publicar?'),
		}

class InicioSesion2Form(forms.Form):
	OPTIONS = (
                ("Egresado", "Egresado"),
                ("Empresa", "Empresa"),
                ("Coordinador", "Coordinador")
                )
	tipo_u = forms.ChoiceField(choices=OPTIONS, label='')
	codigo 	= forms.CharField(max_length=50 , required=True, label='Usuario')
	clave 	= forms.CharField(max_length=50, widget=forms.PasswordInput, label='Contraseña')

class EmpresaForm(ModelForm):
	class Meta:
		model = Empresa


class InicioSesion3Form(forms.Form):
	OPTION_S = (
				("Empresa", "Empresa"),
                ("Egresado", "Egresado"),
                ("Coordinador", "Coordinador"),
                
    )
	tipo_u = forms.ChoiceField(choices=OPTION_S, label='')
	codigo 	= forms.CharField(max_length=50 , required=True, label='Usuario')
	clave 	= forms.CharField(max_length=50, widget=forms.PasswordInput, label='Contraseña')

class CpasswordForm(ModelForm):
	
	password_a 	= forms.CharField(max_length=12, widget=forms.PasswordInput, label='Contraseña antigua:')
	class Meta:
		model = Usuario 
		fields = ['password_a','password','password_2',]
		labels = {
			'password': ('Contraseña nueva:'),
			'password_2': ('Contraseña nueva (Confirmación):'),
		}
		widgets = {
			'password': forms.PasswordInput,
			'password_2': forms.PasswordInput,	
		}

class EditOferta_empForm(forms.Form):
    nombre 					= forms.CharField(max_length=45, label='Titulo para oferta laboral')
    desc                    = forms.CharField(widget=forms.Textarea, label='Descripcion')
    salario 				= forms.CharField(max_length=45, label='Salario', widget=forms.TextInput(attrs={'placeholder': 'Escribe el salario'}))
    perfil_profesional		= forms.CharField(max_length=45, label='Tipo de perfil', widget=forms.Select(choices=PERFIL_CHOICES))
    horario 				= forms.CharField(max_length=45, label='Horario')
    tipo_contrato 			= forms.CharField(max_length=45, label='Tipo de contrato', widget=forms.Select(choices=CONTRATO_CHOICES))
    contacto                = forms.CharField(max_length=45, label='Nombre de contacto')
    telefonofijo_contacto   = forms.IntegerField(label='Telefono Fijo', required=False) 
    correo                  = forms.EmailField(label='Correo Electronico', widget=forms.TextInput(attrs={'placeholder': 'ejemplo@correo.com'}))
    celular_contacto        = forms.IntegerField(label='Numero de Celular') 

class EgresadosForm(ModelForm):
	class Meta:
		model = Egresado
		exclude = ['ultima_modificacion','usuario','programa','estado','fecha_grado',]
		labels = {
			'p_nombre': ('Primer Nombre'),
			's_nombre': ('Segundo Nombre'),
			'p_apellido': ('Primer Apellido'),
			's_apellido': ('Segundo Apellido'),
			#'estado': ('Estado'),
		}

class recoverPassForm(forms.Form):
	correo = forms.EmailField(max_length=50 , required=True, label='Correo electronico:', widget=forms.TextInput(attrs={'placeholder': 'Email'}))

class ResetpassForm(ModelForm):
	
	class Meta:
		model = Usuario 
		fields = ['password','password_2',]
		labels = {
			'password': ('Contraseña nueva:'),
			'password_2': ('Contraseña nueva (Confirmación):'),
		}
		widgets = {
			'password': forms.PasswordInput,
			'password_2': forms.PasswordInput,	
		}

class EditEmpresaForm(ModelForm):

	email = forms.EmailField(max_length=50 , required=True, label='Correo electronico:', widget=forms.TextInput(attrs={'placeholder': 'Email'}))

	class Meta:
		model = Empresa
		fields = ['nombre','tipo','email']
		labels = {
			'nombre': ('Nombre de empresa:'),
			'tipo': ('Tipo de empresa:'),
		}
		